﻿using Identity.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Identity.Repository.Seed
{
    public static class UsuarioidentitySeed
    {
        public static void PopularUsuarioIdentity(this ModelBuilder modelBuilder)
        {
            var hasher = new PasswordHasher<UsuarioIdentity>();
            var adminId = "a18be9c0-aa65-4af8-bd17-00bd9344e575";
            var proprietarioId = "84ad2c82-23f4-4b52-b379-ebd4e07bc7ed";
            var corretorId = "874a2abd-4d56-4af5-b361-c2b24234d519";
            var imobiliariaId = "472f72a7-2e9f-41d1-b756-a662ac2a1d98";

            modelBuilder.Entity<UsuarioIdentity>().HasData(new UsuarioIdentity
            {
                Id = adminId,
                UserName = "admin@admin",
                NormalizedUserName = "ADMIN@ADMIN",
                Email = "admin@admin",
                NormalizedEmail = "ADMIN@ADMIN",
                EmailConfirmed = true,
                PasswordHash = hasher.HashPassword(null, "admin@admin"),
                SecurityStamp = string.Empty
            });

            modelBuilder.Entity<UsuarioIdentity>().HasData(new UsuarioIdentity
            {
                Id = proprietarioId,
                UserName = "proprietario@proprietario",
                NormalizedUserName = "PROPRIETARIO@PROPRIETARIO",
                Email = "proprietario@proprietario",
                NormalizedEmail = "PROPRIETARIO@PROPRIETARIO",
                EmailConfirmed = true,
                PasswordHash = hasher.HashPassword(null, "proprietario@proprietario"),
                SecurityStamp = string.Empty
            });

            modelBuilder.Entity<UsuarioIdentity>().HasData(new UsuarioIdentity
            {
                Id = corretorId,
                UserName = "corretor@corretor",
                NormalizedUserName = "CORRETOR@CORRETOR",
                Email = "corretor@corretor",
                NormalizedEmail = "CORRETOR@CORRETOR",
                EmailConfirmed = true,
                PasswordHash = hasher.HashPassword(null, "corretor@corretor"),
                SecurityStamp = string.Empty
            });

            modelBuilder.Entity<UsuarioIdentity>().HasData(new UsuarioIdentity
            {
                Id = imobiliariaId,
                UserName = "imobiliaria@imobiliaria",
                NormalizedUserName = "IMOBILIARIA@IMOBILIARIA",
                Email = "imobiliaria@imobiliaria",
                NormalizedEmail = "IMOBILIARIA@IMOBILIARIA",
                EmailConfirmed = true,
                PasswordHash = hasher.HashPassword(null, "imobiliaria@imobiliaria"),
                SecurityStamp = string.Empty
            });

            modelBuilder.Entity<IdentityUserClaim<string>>().HasData(new IdentityUserClaim<string>
            {
                Id = 1,
                UserId = adminId,
                ClaimType = "Administrador",
                ClaimValue = "True"
            });

            modelBuilder.Entity<IdentityUserClaim<string>>().HasData(new IdentityUserClaim<string>
            {
                Id = 2,
                UserId = adminId,
                ClaimType = "Anunciante",
                ClaimValue = "True"
            });

            modelBuilder.Entity<IdentityUserClaim<string>>().HasData(new IdentityUserClaim<string>
            {
                Id = 3,
                UserId = proprietarioId,
                ClaimType = "Proprietario",
                ClaimValue = "True"
            });

            modelBuilder.Entity<IdentityUserClaim<string>>().HasData(new IdentityUserClaim<string>
            {
                Id = 4,
                UserId = proprietarioId,
                ClaimType = "Anunciante",
                ClaimValue = "True"
            });

            modelBuilder.Entity<IdentityUserClaim<string>>().HasData(new IdentityUserClaim<string>
            {
                Id = 5,
                UserId = corretorId,
                ClaimType = "Corretor",
                ClaimValue = "True"
            });

            modelBuilder.Entity<IdentityUserClaim<string>>().HasData(new IdentityUserClaim<string>
            {
                Id = 6,
                UserId = corretorId,
                ClaimType = "Anunciante",
                ClaimValue = "True"
            });

            modelBuilder.Entity<IdentityUserClaim<string>>().HasData(new IdentityUserClaim<string>
            {
                Id = 7,
                UserId = imobiliariaId,
                ClaimType = "Imobiliaria",
                ClaimValue = "True"
            });

            modelBuilder.Entity<IdentityUserClaim<string>>().HasData(new IdentityUserClaim<string>
            {
                Id = 8,
                UserId = imobiliariaId,
                ClaimType = "Anunciante",
                ClaimValue = "True"
            });
        }
    }
}
