﻿using Identity.Entities;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Identity.Repository.Interface
{
    public interface IUsuarioIdentityRepository
    {
        Task<IdentityResult> CriarUsuarioIdentity(UsuarioIdentity usuarioIdentity, string senha);
        Task<IdentityResult> CriarClaimsDoUsuario(UsuarioIdentity usuarioIdentity, Claim claim);
        Task<SignInResult> RealizarLogin(UsuarioIdentity usuarioIdentity, string senha);
        Task Sair();
        Task<UsuarioIdentity> BuscarPorEmail(string email);
        Task<UsuarioIdentity> UsuarioLogado();
        string UsuarioLogadoId();
        Task<ICollection<Claim>> BuscarClaims(string id);
        Task<IdentityResult> Deletar(UsuarioIdentity usuarioIdentity);
        Task DeletarClaim(string id, Claim claim);
    }
}