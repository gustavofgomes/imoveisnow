﻿using Identity.Entities;
using Identity.Repository.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Identity.Repository.Class
{
    public class UsuarioIdentityRepository : IUsuarioIdentityRepository
    {
        private readonly UserManager<UsuarioIdentity> UsuarioIdentityContexto;
        private readonly SignInManager<UsuarioIdentity> AcessoIdentityContexto;
        private readonly IHttpContextAccessor HttpContextAccessor;

        public UsuarioIdentityRepository(UserManager<UsuarioIdentity> usuarioIdentityContexto, SignInManager<UsuarioIdentity> acessoIdentityContexto, IHttpContextAccessor httpContextAccessor)
        {
            UsuarioIdentityContexto = usuarioIdentityContexto;
            AcessoIdentityContexto = acessoIdentityContexto;
            HttpContextAccessor = httpContextAccessor;
        }

        public async Task<IdentityResult> CriarUsuarioIdentity(UsuarioIdentity usuarioIdentity, string senha)
        {
            return await UsuarioIdentityContexto.CreateAsync(usuarioIdentity, senha);
        }

        public async Task<IdentityResult> CriarClaimsDoUsuario(UsuarioIdentity usuarioIdentity, Claim claim)
        {
            return await UsuarioIdentityContexto.AddClaimAsync(usuarioIdentity, claim);
        }

        public async Task<SignInResult> RealizarLogin(UsuarioIdentity usuarioIdentity, string senha)
        {
            if (usuarioIdentity != null)
                return await AcessoIdentityContexto.PasswordSignInAsync(usuarioIdentity, senha, false, lockoutOnFailure: true);

            return SignInResult.Failed;
        }

        public async Task Sair()
        {
            await AcessoIdentityContexto.SignOutAsync();
        }

        public async Task<UsuarioIdentity> BuscarPorEmail(string email)
        {
            return await UsuarioIdentityContexto.FindByEmailAsync(email);
        }

        public async Task<UsuarioIdentity> BuscarPorId(string id)
        {
            return await UsuarioIdentityContexto.FindByIdAsync(id);
        }

        public async Task<UsuarioIdentity> UsuarioLogado()
        {
            return await UsuarioIdentityContexto.GetUserAsync(HttpContextAccessor.HttpContext.User);
        }

        public string UsuarioLogadoId()
        {
            return UsuarioIdentityContexto.GetUserId(HttpContextAccessor.HttpContext.User);
        }

        public async Task<ICollection<Claim>> BuscarClaims(string id)
        {
            var usuarioIdentity = await BuscarPorId(id);
            return await UsuarioIdentityContexto.GetClaimsAsync(usuarioIdentity);
        }

        public async Task<IdentityResult> Deletar(UsuarioIdentity usuarioIdentity)
        {
            return await UsuarioIdentityContexto.DeleteAsync(usuarioIdentity);
        }

        public async Task DeletarClaim(string id, Claim claim)
        {
            var usuarioIdentity = await BuscarPorId(id);
            await UsuarioIdentityContexto.RemoveClaimAsync(usuarioIdentity, claim);
        }
    }
}