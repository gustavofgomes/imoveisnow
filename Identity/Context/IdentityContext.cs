﻿using Identity.Entities;
using Identity.Repository.Seed;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Identity.Context
{
    public class IdentityContext : IdentityDbContext<UsuarioIdentity>
    {
        public IConfiguration Configuration { get; }

        public IdentityContext(IConfiguration configuration, DbContextOptions<IdentityContext> options) : base(options)
        {
            Configuration = configuration;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.PopularUsuarioIdentity();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(Configuration.GetConnectionString("INContext"));
        }
    }
}
