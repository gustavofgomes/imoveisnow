﻿$('input').each(function () {
    let req = $(this).attr('data-val-required');
    if (undefined != req) {
        let label = $('label[for="' + $(this).attr('id') + '"]');
        let text = label.text();
        if (text.length > 0) {
            label.append('<span style="color:red"> *</span>');
        }
    }
});

$('select').each(function () {
    let req = $(this).attr('data-val-required');
    if (undefined != req) {
        let label = $('label[for="' + $(this).attr('id') + '"]');
        let text = label.text();
        if (text.length > 0) {
            label.append('<span style="color:red"> *</span>');
        }
    }
});

$('textarea').each(function () {
    let req = $(this).attr('data-val-required');
    if (undefined != req) {
        let label = $('label[for="' + $(this).attr('id') + '"]');
        let text = label.text();
        if (text.length > 0) {
            label.append('<span style="color:red"> *</span>');
        }
    }
});