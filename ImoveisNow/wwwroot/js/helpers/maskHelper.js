﻿$(document).ready(function () {
    if (jQuery().mask) {
        var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? "(00) 00000-0000" : "(00) 0000-00009";
        },
            spOptions = {
                onKeyPress: function (val, e, field, options) {
                    field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };
        $(".mask-telefone").mask(SPMaskBehavior, spOptions);
    }
});

$(document).ready(function () {
    $(".mask-inteiro").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
});

$(document).ready(function () {
    $(".mask-valor").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
});

$(".mask-valor").mask("0.000.000.000", { reverse: true });
$(".mask-cep").mask("00000-000", { reverse: true });