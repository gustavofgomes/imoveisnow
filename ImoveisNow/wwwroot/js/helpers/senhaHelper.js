﻿$(document).on('click', '.icone-senha', function () {
    if ($(".senha").attr("type") === "password") {
        $(".senha").attr("type", "text");
        $("#iconeSenha").removeClass("far fa-eye");
        $("#iconeSenha").addClass("fas fa-eye");
    } else {
        $(".senha").attr("type", "password");
        $("#iconeSenha").removeClass("fas fa-eye");
        $("#iconeSenha").addClass("far fa-eye");
    }
});

$(document).on('click', '.icone-confirmacao-senha', function () {
    if ($(".confirmacao-senha").attr("type") === "password") {
        $(".confirmacao-senha").attr("type", "text");
        $("#iconeConfirmacaoSenha").removeClass("far fa-eye");
        $("#iconeConfirmacaoSenha").addClass("fas fa-eye");
    } else {
        $(".confirmacao-senha").attr("type", "password");
        $("#iconeConfirmacaoSenha").removeClass("fas fa-eye");
        $("#iconeConfirmacaoSenha").addClass("far fa-eye");
    }
});