﻿var $CepConsultado = false;
var $ValorDeCepConsultado = "";

if ($('#Cep').val() !== "") {
    buscarCep($('#Cep'));
}
$('#Cep').keyup(function () {
    if ($(this).val().length > 8) {
        buscarCep($(this));
    }
});
function buscarCep(el) {

    let cep = el.val().replace(/\D/g, '');
    let vm = this;

    //Verifica se o elemento esta vazio
    if (cep !== "") {
        var validacep = /^[0-9]{8}$/;

        //Valida o formato do CEP.
        if (validacep.test(cep)) {

            //Consulta o webservice viacep.com.br/
            $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {

                if (!("erro" in dados)) {

                    //Atualiza os campos com os valores da consulta.
                    $(".Logradouro").val(dados.logradouro);
                    $(".Bairro").val(dados.bairro);
                    $(".Cidade").val(dados.localidade);
                    $(".Uf").val(dados.uf);
                    habilitarForm();
                    //Exibi o resto do Formulário
                    $CepConsultado = true;
                    $ValorDeCepConsultado = cep;
                }
                else {
                    //CEP pesquisado não foi encontrado.
                    vm.limpaFormCep();
                    Swal.fire({
                        type: 'warning',
                        title: 'Atenção',
                        text: 'CEP não encontrado!'
                    })
                    desabilitarForm();
                }
            });
        } else {
            //CEP é inválido.
            vm.limpaFormCep();
            Swal.fire({
                type: 'warning',
                title: 'Atenção',
                text: 'Formato de CEP inválido!'
            })
            desabilitarForm();
        }
    } else {
        //CEP sem valor, limpa formulário.
        vm.limpaFormCep();
        desabilitarForm();
        return false;
    }
}

function limpaFormCep() {
    //Limpa valores do formulário de CEP.
    $(".Logradouro").val('');
    $(".Bairro").val('');
    $(".Municipio").val('');
    $(".Uf").val('');
    $("#Numero").val('');
    $("#Cidade").show();
}

function habilitarForm() {
    $('.Endereco').show();
    $('.CepInfo').hide();
    $('.submit').attr("disabled", false);
}

function desabilitarForm() {
    $('.Endereco').hide();
    $('.CepInfo').show();
    $('.submit').attr("disabled", true);
}

//validações antes de efetuar o submit
$(document).on('submit', function () {
    var el = $(".Cep");
    let cep = el.val().replace(/\D/g, '');
    var validacep = /^[0-9]{8}$/;

    if (cep === "") {
        return false;
    }

    if (!validacep.test(cep)) {
        Swal.fire({
            title: "Erro!",
            text: "Formato de CEP inválido.",
            type: "error"
        });
        return false;
    }

    if (!$CepConsultado) {
        Swal.fire({
            title: "Erro!",
            text: "Consulta de CEP ainda não realizada.",
            type: "error"
        });
        return false;
    }

    if (cep !== $ValorDeCepConsultado) {
        Swal.fire({
            title: "Erro!",
            text: "O CEP atual difere do consultado anteriormente.",
            type: "error"
        });
        return false;
    }
});

$("#btnBuscarCep").on("click", function () {
    var el = $("#Cep");
    let cep = el.val().replace(/\D/g, '');
    if (cep !== "") {
        buscarCep(el);
    }
});

$("#Cep").on("keyup keypress", function (e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        var el = $("#Cep");
        let cep = el.val().replace(/\D/g, '');
        if (cep !== "") {
            e.preventDefault();
            buscarCep(el);
        }
        else {
            e.preventDefault();
        }
    }
});