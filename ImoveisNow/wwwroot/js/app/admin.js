﻿$(document).ready(function () {
    var dados = $Visualizacao;
    new Chart(document.getElementById("chart"), {
        "type": "bar",
        "data": {
            "labels": ["Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro"],
            "datasets": [{
                "label": "Visualizações",
                "data": [0, 0, 0, dados, 0, 0],
                "backgroundColor": ["rgb(0, 92, 191)", "rgb(0, 62, 128)", "rgb(0, 123, 255)", "rgb(0, 31, 64)", "rgb(230, 0, 0)", "rgb(236, 239, 241)"]
            }]
        },
        "options": {
            "responsive": true,
            "legend": {
                "display": false
            }
        }
    });
})

