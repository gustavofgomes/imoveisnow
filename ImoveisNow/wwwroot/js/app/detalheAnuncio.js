﻿$(".carousel").carousel({
    interval: false
});

$(document).on("click", ".caracteristicas", function () {
    $("#modalCaracteristicasDoAnuncio").modal("show");
});

$(document).on("click", ".contatar", function () {
    $("#modalContatarAnunciante").modal("show");
});

$(document).on("click", ".compartilhar", function () {
    $("#modalCompartilhar").modal("show");
});

$(document).on("click", ".denuncia", function () {
    $("#modalDenuncia").modal("show");
});

$("#url-xs").keyup(function () {
    $("#xl-lg-md").val($(this).val());
});

$(document).ready(function () {
    let url = window.location.href;
    $("#url").val(url);
});

$('#copy').click(function () {
    let url = window.location.href;
    copyToClipboard(url);
});

function copyToClipboard(url) {
    let $temp = $("<input>");
    $("body").append($temp);
    $temp.val(url).select();
    document.execCommand("copy");
    $temp.remove();
}