﻿$("#Descricao").on("focusin", function () {
    $(".descricao-info").show();
});

$("#Descricao").on("focusout", function () {
    $(".descricao-info").hide();
    titulo();
});

$("#AreaUtil").on("focusin", function () {
    $(".area-info").show();
});

$("#AreaUtil").on("focusout", function () {
    $(".area-info").hide();
    titulo();
});

$("#Condominio1").on("click", function () {
    $(".DetalhesCondominio").show();
});

$("#Condominio2").on("click", function () {
    $(".DetalhesCondominio").hide();
    titulo();
});

function titulo() {
    var tipoDeImovel = $("#TipoDeImovelId option:selected").html();
    var tipoDeOferta = $("#TipoDeOfertaId").data("id")
    var area = $("#AreaUtil").val();
    $('#preTitulo').text(tipoDeImovel + " " + tipoDeOferta + ", " + area + "m²");
};