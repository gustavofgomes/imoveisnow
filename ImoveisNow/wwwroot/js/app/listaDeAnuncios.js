﻿$("#mosaicoDeAnuncios").hide();

$("#mosaico").click(function () {
    $("#mosaico").removeClass("btn-filtro");
    $("#mosaico").addClass("btn-filtro-ativo");
    $("#listagem").removeClass("btn-filtro-ativo")
    $("#listagem").addClass("btn-filtro");
    $("#listagemDeAnuncios").hide();
    $("#mosaicoDeAnuncios").show();
    $("#mosaicoDeAnuncios").removeClass("hide");
});

$("#listagem").click(function () {
    $("#listagem").removeClass("btn-filtro");
    $("#listagem").addClass("btn-filtro-ativo");
    $("#mosaico").removeClass("btn-filtro-ativo")
    $("#mosaico").addClass("btn-filtro");
    $("#mosaicoDeAnuncios").hide();
    $("#listagemDeAnuncios").show();
});

$(".carousel").carousel({
    interval: false
});