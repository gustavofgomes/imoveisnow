﻿var imgUpload = document.getElementById("Imagens")
    , imgPreview = document.getElementById("img_preview")
    , totalFiles
    , previewTitle
    , previewTitleText
    , img;

imgUpload.addEventListener("change", previewImgs, false);

function previewImgs(event) {
    totalFiles = imgUpload.files.length;

    if (!!totalFiles) {
        imgPreview.classList.remove("imgs-thumbs-hidden");
        previewTitle = document.createElement("p");
        previewTitle.style.fontWeight = "bold";
        previewTitleText = document.createTextNode(totalFiles + " Foto Selecionada");
        previewTitle.appendChild(previewTitleText);
        imgPreview.appendChild(previewTitle);
    }

    for (var i = 0; i < totalFiles; i++) {
        img = document.createElement("img");
        img.src = URL.createObjectURL(event.target.files[i]);
        img.classList.add("img-preview-thumb");
        imgPreview.appendChild(img);
    }
}