﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ImoveisNow.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Buscas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Parametro = table.Column<string>(nullable: false),
                    DataEHora = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Buscas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Estado",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nome = table.Column<string>(type: "varchar(36)", maxLength: 36, nullable: false),
                    Sigla = table.Column<string>(type: "varchar(4)", maxLength: 4, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Estado", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Feedback",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Email = table.Column<string>(type: "varchar(80)", maxLength: 80, nullable: false),
                    Texto = table.Column<string>(type: "varchar(2000)", maxLength: 2000, nullable: false),
                    ReceberResposta = table.Column<bool>(nullable: false),
                    Arquivado = table.Column<bool>(nullable: false),
                    DataEHora = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Feedback", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TipoDeOferta",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nome = table.Column<string>(type: "varchar(50)", maxLength: 36, nullable: false),
                    Descricao = table.Column<string>(type: "varchar(50)", maxLength: 36, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoDeOferta", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TipoDePlano",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nome = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    DiasDeDuracao = table.Column<int>(nullable: false),
                    LimiteDeAnuncios = table.Column<int>(nullable: false),
                    LimiteDeDestaques = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoDePlano", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TipoDoImovel",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nome = table.Column<string>(type: "varchar(36)", maxLength: 36, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoDoImovel", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Cidade",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nome = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    EstadoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cidade", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cidade_Estado_EstadoId",
                        column: x => x.EstadoId,
                        principalTable: "Estado",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Anunciante",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Nome = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    Email = table.Column<string>(type: "varchar(80)", maxLength: 80, nullable: false),
                    Telefone = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    CidadeId = table.Column<int>(nullable: false),
                    CRECI = table.Column<string>(nullable: true),
                    DataDeCadastro = table.Column<DateTime>(nullable: false),
                    UltimoAcesso = table.Column<DateTime>(nullable: false),
                    TipoDeAnunciante = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: false),
                    Ativo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Anunciante", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Anunciante_Cidade_CidadeId",
                        column: x => x.CidadeId,
                        principalTable: "Cidade",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Anuncio",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Valor = table.Column<decimal>(nullable: false),
                    Titulo = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    Descricao = table.Column<string>(type: "varchar(2000)", maxLength: 2000, nullable: false),
                    DataDeCriacao = table.Column<DateTime>(nullable: false),
                    AnuncianteId = table.Column<Guid>(nullable: false),
                    CidadeId = table.Column<int>(nullable: false),
                    OcultarTelefone = table.Column<bool>(nullable: false),
                    Status = table.Column<string>(nullable: false),
                    Peso = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Anuncio", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Anuncio_Anunciante_AnuncianteId",
                        column: x => x.AnuncianteId,
                        principalTable: "Anunciante",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Anuncio_Cidade_CidadeId",
                        column: x => x.CidadeId,
                        principalTable: "Cidade",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Compra",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AnuncianteId = table.Column<Guid>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    FormaDePagamento = table.Column<string>(nullable: true),
                    DateEHora = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Compra", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Compra_Anunciante_AnuncianteId",
                        column: x => x.AnuncianteId,
                        principalTable: "Anunciante",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlanoDoAnunciante",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TipoDePlanoId = table.Column<int>(nullable: false),
                    AnuncianteId = table.Column<Guid>(nullable: false),
                    DataDeInicio = table.Column<DateTime>(nullable: false),
                    DataDeTermino = table.Column<DateTime>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanoDoAnunciante", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PlanoDoAnunciante_Anunciante_AnuncianteId",
                        column: x => x.AnuncianteId,
                        principalTable: "Anunciante",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PlanoDoAnunciante_TipoDePlano_TipoDePlanoId",
                        column: x => x.TipoDePlanoId,
                        principalTable: "TipoDePlano",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AnunciosFavoritos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AnuncioId = table.Column<Guid>(nullable: false),
                    AnuncianteId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnunciosFavoritos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AnunciosFavoritos_Anunciante_AnuncianteId",
                        column: x => x.AnuncianteId,
                        principalTable: "Anunciante",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AnunciosFavoritos_Anuncio_AnuncioId",
                        column: x => x.AnuncioId,
                        principalTable: "Anuncio",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContatosRecebidos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AnuncioId = table.Column<Guid>(nullable: false),
                    DataEHora = table.Column<DateTime>(nullable: false),
                    Nome = table.Column<string>(type: "varchar(36)", maxLength: 36, nullable: false),
                    Telefone = table.Column<string>(nullable: true),
                    ContatoViaWhatsApp = table.Column<bool>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    Mensagem = table.Column<string>(nullable: false),
                    Arquivado = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContatosRecebidos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContatosRecebidos_Anuncio_AnuncioId",
                        column: x => x.AnuncioId,
                        principalTable: "Anuncio",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Denuncia",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AnuncioId = table.Column<Guid>(nullable: false),
                    Texto = table.Column<string>(type: "varchar(1000)", maxLength: 1000, nullable: false),
                    Motivo = table.Column<string>(nullable: false),
                    DataEHoraDeCriacao = table.Column<DateTime>(nullable: false),
                    AvalidaEm = table.Column<DateTime>(nullable: true),
                    Avaliada = table.Column<bool>(nullable: false),
                    Veredito = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Denuncia", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Denuncia_Anuncio_AnuncioId",
                        column: x => x.AnuncioId,
                        principalTable: "Anuncio",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Imagens",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AnuncioId = table.Column<Guid>(nullable: false),
                    Nome = table.Column<string>(type: "varchar(36)", maxLength: 36, nullable: false),
                    Url = table.Column<string>(type: "varchar(200)", maxLength: 24, nullable: false),
                    Formato = table.Column<string>(type: "varchar(8)", maxLength: 8, nullable: false),
                    Tamanho = table.Column<string>(type: "varchar(24)", maxLength: 24, nullable: false),
                    Largura = table.Column<int>(nullable: false),
                    Altura = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Imagens", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Imagens_Anuncio_AnuncioId",
                        column: x => x.AnuncioId,
                        principalTable: "Anuncio",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Imovel",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TipoDeOfertaId = table.Column<int>(nullable: false),
                    TipoDeImovelId = table.Column<int>(nullable: false),
                    AreaUtil = table.Column<decimal>(nullable: false),
                    Condominio = table.Column<bool>(nullable: false),
                    Escriturado = table.Column<bool>(nullable: false),
                    Financiavel = table.Column<bool>(nullable: false),
                    AreaMurada = table.Column<bool>(nullable: false),
                    AcessoAsfaltado = table.Column<bool>(nullable: false),
                    Piscina = table.Column<bool>(nullable: false),
                    Churrasqueira = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Imovel", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Imovel_Anuncio_Id",
                        column: x => x.Id,
                        principalTable: "Anuncio",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Imovel_TipoDoImovel_TipoDeImovelId",
                        column: x => x.TipoDeImovelId,
                        principalTable: "TipoDoImovel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Imovel_TipoDeOferta_TipoDeOfertaId",
                        column: x => x.TipoDeOfertaId,
                        principalTable: "TipoDeOferta",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Visualizacoes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AnuncioId = table.Column<Guid>(nullable: false),
                    DataEHora = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Visualizacoes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Visualizacoes_Anuncio_AnuncioId",
                        column: x => x.AnuncioId,
                        principalTable: "Anuncio",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DetalhesDoImovel",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    NumeroDeQuartos = table.Column<int>(nullable: false),
                    NumeroDeBanheiros = table.Column<int>(nullable: false),
                    NumeroDeSuites = table.Column<int>(nullable: false),
                    VagasDeGaragem = table.Column<int>(nullable: false),
                    GaragemCoberta = table.Column<bool>(nullable: false),
                    Mobiliado = table.Column<bool>(nullable: false),
                    Quintal = table.Column<bool>(nullable: false),
                    Jardim = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DetalhesDoImovel", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DetalhesDoImovel_Imovel_Id",
                        column: x => x.Id,
                        principalTable: "Imovel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DetalhesDoTerreno",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Planado = table.Column<bool>(nullable: false),
                    Limpo = table.Column<bool>(nullable: false),
                    AguaEncanada = table.Column<bool>(nullable: false),
                    PocoArtesiano = table.Column<bool>(nullable: false),
                    EnergiaEletrica = table.Column<bool>(nullable: false),
                    AreaVerde = table.Column<bool>(nullable: false),
                    CasaSede = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DetalhesDoTerreno", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DetalhesDoTerreno_Imovel_Id",
                        column: x => x.Id,
                        principalTable: "Imovel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Endereco",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Cep = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    Bairro = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Logradouro = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Numero = table.Column<string>(type: "varchar(18)", maxLength: 18, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Endereco", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Endereco_Imovel_Id",
                        column: x => x.Id,
                        principalTable: "Imovel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InformacoesDoCondominio",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Nome = table.Column<string>(nullable: true),
                    ValorDoCondominio = table.Column<decimal>(nullable: false),
                    Fechado = table.Column<bool>(nullable: false),
                    SistemaDeVigilancia = table.Column<bool>(nullable: false),
                    PortaoEletronico = table.Column<bool>(nullable: false),
                    PermiteAnimais = table.Column<bool>(nullable: false),
                    Elevador = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InformacoesDoCondominio", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InformacoesDoCondominio_Imovel_Id",
                        column: x => x.Id,
                        principalTable: "Imovel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Buscas",
                columns: new[] { "Id", "DataEHora", "Parametro" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 7, 9, 12, 5, 17, 410, DateTimeKind.Local).AddTicks(9331), "Cohab" },
                    { 2, new DateTime(2019, 7, 9, 12, 5, 17, 410, DateTimeKind.Local).AddTicks(9331), "Castanheira" },
                    { 3, new DateTime(2019, 7, 9, 12, 5, 17, 410, DateTimeKind.Local).AddTicks(9331), "Porto Velho" }
                });

            migrationBuilder.InsertData(
                table: "Estado",
                columns: new[] { "Id", "Nome", "Sigla" },
                values: new object[] { 1, "Rondônia", "RO" });

            migrationBuilder.InsertData(
                table: "Feedback",
                columns: new[] { "Id", "Arquivado", "DataEHora", "Email", "ReceberResposta", "Texto" },
                values: new object[] { 1, false, new DateTime(2019, 7, 9, 12, 5, 17, 410, DateTimeKind.Local).AddTicks(3754), "feedback@feedback.com", true, "Mensagem enviada pelo Feedback" });

            migrationBuilder.InsertData(
                table: "TipoDeOferta",
                columns: new[] { "Id", "Descricao", "Nome" },
                values: new object[,]
                {
                    { 1, "à Venda", "Venda" },
                    { 2, "para Alugar", "Aluguel" },
                    { 3, "Vendo agio", "Agio" }
                });

            migrationBuilder.InsertData(
                table: "TipoDePlano",
                columns: new[] { "Id", "DiasDeDuracao", "LimiteDeAnuncios", "LimiteDeDestaques", "Nome" },
                values: new object[,]
                {
                    { 4, 30, 50, 8, "Premium3" },
                    { 3, 30, 20, 4, "Premium2" },
                    { 1, 365, 3, 0, "Gratuito" },
                    { 2, 30, 8, 2, "Premium1" }
                });

            migrationBuilder.InsertData(
                table: "TipoDoImovel",
                columns: new[] { "Id", "Nome" },
                values: new object[,]
                {
                    { 8, "Loja" },
                    { 11, "Lote/Terreno" },
                    { 10, "Imóvel comercial" },
                    { 9, "Box" },
                    { 7, "Ponto comercial" },
                    { 1, "Casa" },
                    { 5, "Kitnet/Conjugado" },
                    { 4, "Flat" },
                    { 3, "Cobertuda" },
                    { 2, "Apartamento" },
                    { 12, "Fazenda/Sítio" },
                    { 6, "Sobrado" },
                    { 13, "Chácara" }
                });

            migrationBuilder.InsertData(
                table: "Cidade",
                columns: new[] { "Id", "EstadoId", "Nome" },
                values: new object[] { 1, 1, "Porto Velho" });

            migrationBuilder.InsertData(
                table: "Anunciante",
                columns: new[] { "Id", "Ativo", "CRECI", "CidadeId", "DataDeCadastro", "Email", "Nome", "Telefone", "TipoDeAnunciante", "UltimoAcesso" },
                values: new object[,]
                {
                    { new Guid("a18be9c0-aa65-4af8-bd17-00bd9344e575"), true, null, 1, new DateTime(2019, 7, 9, 0, 0, 0, 0, DateTimeKind.Local), "admin@admin", "Admin", "(69) 91234-5678", "Proprietário", new DateTime(2019, 7, 9, 0, 0, 0, 0, DateTimeKind.Local) },
                    { new Guid("84ad2c82-23f4-4b52-b379-ebd4e07bc7ed"), true, null, 1, new DateTime(2019, 7, 9, 0, 0, 0, 0, DateTimeKind.Local), "proprietario@proprietario", "Proprietário", "(69) 91234-5678", "Proprietário", new DateTime(2019, 7, 9, 0, 0, 0, 0, DateTimeKind.Local) },
                    { new Guid("472f72a7-2e9f-41d1-b756-a662ac2a1d98"), true, null, 1, new DateTime(2019, 7, 9, 0, 0, 0, 0, DateTimeKind.Local), "imobiliaria@imobiliaria", "Imobiliária", "(69) 91234-5678", "Imobiliária", new DateTime(2019, 7, 9, 0, 0, 0, 0, DateTimeKind.Local) },
                    { new Guid("874a2abd-4d56-4af5-b361-c2b24234d519"), true, null, 1, new DateTime(2019, 7, 9, 0, 0, 0, 0, DateTimeKind.Local), "corretor@corretor", "Corretor", "(69) 91234-5678", "Corretor", new DateTime(2019, 7, 9, 0, 0, 0, 0, DateTimeKind.Local) }
                });

            migrationBuilder.InsertData(
                table: "Anuncio",
                columns: new[] { "Id", "AnuncianteId", "CidadeId", "DataDeCriacao", "Descricao", "OcultarTelefone", "Peso", "Status", "Titulo", "Valor" },
                values: new object[,]
                {
                    { new Guid("37f0b25d-a6b3-4337-bf8b-8a6a289c3ebc"), new Guid("84ad2c82-23f4-4b52-b379-ebd4e07bc7ed"), 1, new DateTime(2019, 7, 9, 12, 5, 17, 398, DateTimeKind.Local).AddTicks(5734), "Casa de 96 metros quadrados está localizado no bairro Castanheira com 2 Quartos e 1 Banheiro, Sala de estar e jantar, cozinha, churrasqueira, área de serviço e um amplo terreno.", false, 100, "Em Destaque", "Casa á venda, 96m² com 2 Quartos", 110000m },
                    { new Guid("987e10f8-5042-4b54-aca6-f7bfb01e5301"), new Guid("84ad2c82-23f4-4b52-b379-ebd4e07bc7ed"), 1, new DateTime(2019, 7, 9, 12, 5, 17, 398, DateTimeKind.Local).AddTicks(5734), "Casa de 148 metros quadrados está localizado no bairro Areal com 3 Quartos, 2 Banheiro e 1 Suite, Sala de estar e jantar, cozinha, churrasqueira, área de serviço e um amplo terreno.", false, 90, "Ativo", "Casa á venda, 148m² com 3 Quartos", 160000m },
                    { new Guid("796e2890-519a-40e4-b8d5-631e67cbc37a"), new Guid("84ad2c82-23f4-4b52-b379-ebd4e07bc7ed"), 1, new DateTime(2019, 7, 9, 12, 5, 17, 398, DateTimeKind.Local).AddTicks(5734), "Casa de 76 metros quadrados está localizado no bairro Cohab com 2 Quartos e 1 Banheiro, cozinha, área de serviço e um amplo terreno.", true, 80, "Ativo", "Casa á venda, 76m², com 2 Quartos", 80000m },
                    { new Guid("7dac6fff-eeff-47e9-92df-a8341f0c44fc"), new Guid("84ad2c82-23f4-4b52-b379-ebd4e07bc7ed"), 1, new DateTime(2019, 7, 9, 12, 5, 17, 398, DateTimeKind.Local).AddTicks(5734), "Terreno de 1000 metros quadrados localizado Estrada do Japonês, Bairro Aeroclube.", true, 60, "Em Destaque", "Terreno/Lote á venda, 1000m²", 2000000m },
                    { new Guid("3a6b2dca-a817-4fc5-be1d-37af2d62ea6f"), new Guid("84ad2c82-23f4-4b52-b379-ebd4e07bc7ed"), 1, new DateTime(2019, 7, 9, 12, 5, 17, 398, DateTimeKind.Local).AddTicks(5734), "Terreno de 500 metros quadrados localizado no Bairro Eldorado.", false, 50, "Em Destaque", "Terreno/Lote á venda, 500m²", 1000000m },
                    { new Guid("5015e685-eda4-4591-b63b-44764c8c41c1"), new Guid("84ad2c82-23f4-4b52-b379-ebd4e07bc7ed"), 1, new DateTime(2019, 7, 9, 12, 5, 17, 398, DateTimeKind.Local).AddTicks(5734), "Apartamento de 58 metros quadrados localizado no Bairro Embratel.", false, 70, "Em Destaque", "Apartamento para alugar, 58m²", 90000m }
                });

            migrationBuilder.InsertData(
                table: "PlanoDoAnunciante",
                columns: new[] { "Id", "AnuncianteId", "Ativo", "DataDeInicio", "DataDeTermino", "TipoDePlanoId" },
                values: new object[,]
                {
                    { 2, new Guid("84ad2c82-23f4-4b52-b379-ebd4e07bc7ed"), true, new DateTime(2019, 7, 9, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2019, 8, 8, 12, 5, 17, 393, DateTimeKind.Local).AddTicks(271), 2 },
                    { 1, new Guid("874a2abd-4d56-4af5-b361-c2b24234d519"), true, new DateTime(2019, 7, 9, 0, 0, 0, 0, DateTimeKind.Local), new DateTime(2020, 7, 9, 12, 5, 17, 393, DateTimeKind.Local).AddTicks(275), 1 }
                });

            migrationBuilder.InsertData(
                table: "AnunciosFavoritos",
                columns: new[] { "Id", "AnuncianteId", "AnuncioId" },
                values: new object[] { 1, new Guid("84ad2c82-23f4-4b52-b379-ebd4e07bc7ed"), new Guid("987e10f8-5042-4b54-aca6-f7bfb01e5301") });

            migrationBuilder.InsertData(
                table: "ContatosRecebidos",
                columns: new[] { "Id", "AnuncioId", "Arquivado", "ContatoViaWhatsApp", "DataEHora", "Email", "Mensagem", "Nome", "Telefone" },
                values: new object[] { 1, new Guid("37f0b25d-a6b3-4337-bf8b-8a6a289c3ebc"), false, true, new DateTime(2019, 7, 9, 12, 5, 17, 400, DateTimeKind.Local).AddTicks(3197), "joao@gmail.com", "Tenho interesse em comprar este imóvel.", "João Lucas", "(69) 99325-0874" });

            migrationBuilder.InsertData(
                table: "Denuncia",
                columns: new[] { "Id", "AnuncioId", "Avaliada", "AvalidaEm", "DataEHoraDeCriacao", "Motivo", "Texto", "Veredito" },
                values: new object[] { 1, new Guid("37f0b25d-a6b3-4337-bf8b-8a6a289c3ebc"), false, null, new DateTime(2019, 7, 9, 12, 5, 17, 404, DateTimeKind.Local).AddTicks(710), "Imóvel já foi vendido", "Entrei em contato com o Anunciante e o mesmo informou que o Imóvel já se encontra vendido mas o anúncio estar aberto", "Não Avaliada" });

            migrationBuilder.InsertData(
                table: "Imagens",
                columns: new[] { "Id", "Altura", "AnuncioId", "Formato", "Largura", "Nome", "Tamanho", "Url" },
                values: new object[,]
                {
                    { 8, 522, new Guid("3a6b2dca-a817-4fc5-be1d-37af2d62ea6f"), ".jpg", 870, "3577cb3a-35d6-43a4-b6a8-03523b246736", "645", "/upload/" },
                    { 7, 539, new Guid("7dac6fff-eeff-47e9-92df-a8341f0c44fc"), ".jpg", 849, "fd79d498-793b-46f5-a813-7c8bfd5f710f", "875", "/upload/" },
                    { 6, 539, new Guid("796e2890-519a-40e4-b8d5-631e67cbc37a"), ".jpg", 849, "81c2bbee-fcb9-49ae-ae16-8aab1c8cf948", "875", "/upload/" },
                    { 9, 580, new Guid("5015e685-eda4-4591-b63b-44764c8c41c1"), ".jpg", 870, "f5b2efce-dd8d-4c6f-8a64-76db0b502206", "232", "/upload/" },
                    { 5, 397, new Guid("987e10f8-5042-4b54-aca6-f7bfb01e5301"), ".jpg", 812, "85870e80-36f3-422d-9afa-bfa2e7bb1fd5", "495", "/upload/" },
                    { 3, 240, new Guid("37f0b25d-a6b3-4337-bf8b-8a6a289c3ebc"), ".jpg", 360, "9d16061d-779c-42b3-ad34-9508244898eb", "1040", "/upload/" },
                    { 2, 240, new Guid("37f0b25d-a6b3-4337-bf8b-8a6a289c3ebc"), ".jpg", 360, "f88fa5dd-d329-4a73-99d1-21efa9cffc79", "798", "/upload/" },
                    { 1, 240, new Guid("37f0b25d-a6b3-4337-bf8b-8a6a289c3ebc"), ".jpg", 360, "7683571e-d99b-4a9a-9059-6b1a2c7675a8", "875", "/upload/" },
                    { 4, 489, new Guid("987e10f8-5042-4b54-aca6-f7bfb01e5301"), ".jpg", 870, "455a1961-41b8-4ff5-9ab2-d6ff53f5770f", "342", "/upload/" }
                });

            migrationBuilder.InsertData(
                table: "Imovel",
                columns: new[] { "Id", "AcessoAsfaltado", "AreaMurada", "AreaUtil", "Churrasqueira", "Condominio", "Escriturado", "Financiavel", "Piscina", "TipoDeImovelId", "TipoDeOfertaId" },
                values: new object[,]
                {
                    { new Guid("37f0b25d-a6b3-4337-bf8b-8a6a289c3ebc"), true, true, 96m, true, true, true, true, false, 1, 1 },
                    { new Guid("987e10f8-5042-4b54-aca6-f7bfb01e5301"), true, true, 148m, true, false, true, true, true, 1, 1 },
                    { new Guid("796e2890-519a-40e4-b8d5-631e67cbc37a"), true, true, 76m, false, false, true, true, false, 1, 1 },
                    { new Guid("7dac6fff-eeff-47e9-92df-a8341f0c44fc"), false, true, 1000m, false, false, true, true, false, 11, 1 },
                    { new Guid("3a6b2dca-a817-4fc5-be1d-37af2d62ea6f"), true, true, 500m, false, false, true, true, false, 11, 1 },
                    { new Guid("5015e685-eda4-4591-b63b-44764c8c41c1"), true, true, 58m, false, false, true, true, false, 2, 2 }
                });

            migrationBuilder.InsertData(
                table: "DetalhesDoImovel",
                columns: new[] { "Id", "GaragemCoberta", "Jardim", "Mobiliado", "NumeroDeBanheiros", "NumeroDeQuartos", "NumeroDeSuites", "Quintal", "VagasDeGaragem" },
                values: new object[,]
                {
                    { new Guid("37f0b25d-a6b3-4337-bf8b-8a6a289c3ebc"), false, false, false, 1, 2, 0, false, 1 },
                    { new Guid("987e10f8-5042-4b54-aca6-f7bfb01e5301"), false, false, false, 2, 3, 1, false, 1 },
                    { new Guid("796e2890-519a-40e4-b8d5-631e67cbc37a"), false, false, false, 1, 2, 0, false, 2 },
                    { new Guid("5015e685-eda4-4591-b63b-44764c8c41c1"), true, false, true, 1, 1, 0, false, 2 }
                });

            migrationBuilder.InsertData(
                table: "DetalhesDoTerreno",
                columns: new[] { "Id", "AguaEncanada", "AreaVerde", "CasaSede", "EnergiaEletrica", "Limpo", "Planado", "PocoArtesiano" },
                values: new object[,]
                {
                    { new Guid("7dac6fff-eeff-47e9-92df-a8341f0c44fc"), false, true, false, true, false, false, true },
                    { new Guid("3a6b2dca-a817-4fc5-be1d-37af2d62ea6f"), true, false, true, true, false, false, true }
                });

            migrationBuilder.InsertData(
                table: "Endereco",
                columns: new[] { "Id", "Bairro", "Cep", "Logradouro", "Numero" },
                values: new object[,]
                {
                    { new Guid("37f0b25d-a6b3-4337-bf8b-8a6a289c3ebc"), "Castanheira", "76811-200", "Rua São Borja", "6169" },
                    { new Guid("987e10f8-5042-4b54-aca6-f7bfb01e5301"), "Areal", "76811-200", "Rua Três e meio", "1341" },
                    { new Guid("796e2890-519a-40e4-b8d5-631e67cbc37a"), "Cohab", "76811-200", "Rua Aroeira", "5796" },
                    { new Guid("7dac6fff-eeff-47e9-92df-a8341f0c44fc"), "Aeroclube", "76811-200", "Estrada do Japônes", "438" },
                    { new Guid("3a6b2dca-a817-4fc5-be1d-37af2d62ea6f"), "Eldorado", "76811-200", "Rua Açai", "7243" },
                    { new Guid("5015e685-eda4-4591-b63b-44764c8c41c1"), "Caladinho", "76811-200", "Rua Tancredo Neves", "3567" }
                });

            migrationBuilder.InsertData(
                table: "InformacoesDoCondominio",
                columns: new[] { "Id", "Elevador", "Fechado", "Nome", "PermiteAnimais", "PortaoEletronico", "SistemaDeVigilancia", "ValorDoCondominio" },
                values: new object[] { new Guid("37f0b25d-a6b3-4337-bf8b-8a6a289c3ebc"), true, true, "Condominio San Marcos", true, true, true, 300m });

            migrationBuilder.CreateIndex(
                name: "IX_Anunciante_CidadeId",
                table: "Anunciante",
                column: "CidadeId");

            migrationBuilder.CreateIndex(
                name: "IX_Anuncio_AnuncianteId",
                table: "Anuncio",
                column: "AnuncianteId");

            migrationBuilder.CreateIndex(
                name: "IX_Anuncio_CidadeId",
                table: "Anuncio",
                column: "CidadeId");

            migrationBuilder.CreateIndex(
                name: "IX_AnunciosFavoritos_AnuncianteId",
                table: "AnunciosFavoritos",
                column: "AnuncianteId");

            migrationBuilder.CreateIndex(
                name: "IX_AnunciosFavoritos_AnuncioId",
                table: "AnunciosFavoritos",
                column: "AnuncioId");

            migrationBuilder.CreateIndex(
                name: "IX_Cidade_EstadoId",
                table: "Cidade",
                column: "EstadoId");

            migrationBuilder.CreateIndex(
                name: "IX_Compra_AnuncianteId",
                table: "Compra",
                column: "AnuncianteId");

            migrationBuilder.CreateIndex(
                name: "IX_ContatosRecebidos_AnuncioId",
                table: "ContatosRecebidos",
                column: "AnuncioId");

            migrationBuilder.CreateIndex(
                name: "IX_Denuncia_AnuncioId",
                table: "Denuncia",
                column: "AnuncioId");

            migrationBuilder.CreateIndex(
                name: "IX_Imagens_AnuncioId",
                table: "Imagens",
                column: "AnuncioId");

            migrationBuilder.CreateIndex(
                name: "IX_Imovel_TipoDeImovelId",
                table: "Imovel",
                column: "TipoDeImovelId");

            migrationBuilder.CreateIndex(
                name: "IX_Imovel_TipoDeOfertaId",
                table: "Imovel",
                column: "TipoDeOfertaId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanoDoAnunciante_AnuncianteId",
                table: "PlanoDoAnunciante",
                column: "AnuncianteId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanoDoAnunciante_TipoDePlanoId",
                table: "PlanoDoAnunciante",
                column: "TipoDePlanoId");

            migrationBuilder.CreateIndex(
                name: "IX_Visualizacoes_AnuncioId",
                table: "Visualizacoes",
                column: "AnuncioId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AnunciosFavoritos");

            migrationBuilder.DropTable(
                name: "Buscas");

            migrationBuilder.DropTable(
                name: "Compra");

            migrationBuilder.DropTable(
                name: "ContatosRecebidos");

            migrationBuilder.DropTable(
                name: "Denuncia");

            migrationBuilder.DropTable(
                name: "DetalhesDoImovel");

            migrationBuilder.DropTable(
                name: "DetalhesDoTerreno");

            migrationBuilder.DropTable(
                name: "Endereco");

            migrationBuilder.DropTable(
                name: "Feedback");

            migrationBuilder.DropTable(
                name: "Imagens");

            migrationBuilder.DropTable(
                name: "InformacoesDoCondominio");

            migrationBuilder.DropTable(
                name: "PlanoDoAnunciante");

            migrationBuilder.DropTable(
                name: "Visualizacoes");

            migrationBuilder.DropTable(
                name: "Imovel");

            migrationBuilder.DropTable(
                name: "TipoDePlano");

            migrationBuilder.DropTable(
                name: "Anuncio");

            migrationBuilder.DropTable(
                name: "TipoDoImovel");

            migrationBuilder.DropTable(
                name: "TipoDeOferta");

            migrationBuilder.DropTable(
                name: "Anunciante");

            migrationBuilder.DropTable(
                name: "Cidade");

            migrationBuilder.DropTable(
                name: "Estado");
        }
    }
}
