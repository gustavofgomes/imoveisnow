﻿namespace ImoveisNow.Models.Helpers
{
    public static class StringHelper
    {
        public static string GetFirstName(this string value)
        {
            string[] splitValue = value.Split(' ');
            string firstName = splitValue[0];

            return firstName;
        }
    }
}