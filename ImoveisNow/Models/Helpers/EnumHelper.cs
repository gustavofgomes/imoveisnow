﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace ImoveisNow.Models.Helpers
{
    public static class EnumHelper
    {
        public static string GetDescription<TEnum>(this TEnum value)
        {
            FieldInfo fieldInfo = value.GetType().GetField(value.ToString());
            if (fieldInfo == null) return null;
            var attribute = (DescriptionAttribute)fieldInfo.GetCustomAttribute(typeof(DescriptionAttribute));
            return attribute.Description;
        }

        public static List<string> CriarListaDeStringEnum<TEnum>()
        {
            var valuesList = new List<string>();

            var values = from TEnum e in Enum.GetValues(typeof(TEnum)) select new { Id = e, Name = e.GetDescription() };

            foreach (var item in values)
            {
                valuesList.Add(item.Name);

            }
            return valuesList;
        }
    }
}