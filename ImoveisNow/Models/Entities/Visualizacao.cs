﻿using System;

namespace ImoveisNow.Models.Entities
{
    public class Visualizacao
    {
        public Visualizacao( Guid anuncioId)
        {
            AnuncioId = anuncioId;
            DataEHora = DateTime.Now;
        }

        protected Visualizacao()
        {

        }

        public int Id { get; private set; }
        public Guid AnuncioId { get; private set; }
        public DateTime DataEHora { get; private set; }

        public Anuncio Anuncio { get; private set; }
    }
}