﻿using System;

namespace ImoveisNow.Models.Entities
{
    public class Feedback
    {
        public Feedback(int id, string email, string texto, bool receberResposta)
        {
            Id = id;
            Email = email;
            Texto = texto;
            ReceberResposta = receberResposta;
            DataEHora = DateTime.Now;
        }

        protected Feedback()
        {

        }

        public int Id { get; private set; }
        public string Email { get; private set; }
        public string Texto { get; private set; }
        public bool ReceberResposta { get; private set; }
        public bool Arquivado { get; private set; }
        public DateTime DataEHora { get; private set; }

        public void Arquivar()
        {
            Arquivado = true;
        }
    }
}