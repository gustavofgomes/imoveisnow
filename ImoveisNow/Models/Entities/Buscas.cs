﻿using System;
using System.Globalization;

namespace ImoveisNow.Models.Entities
{
    public class Buscas
    {
        public Buscas(string parametro)
        {
            Parametro = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(parametro);
            DataEHora = DateTime.Now;
        }

        protected Buscas()
        {

        }
        public int Id { get; private set; }
        public string Parametro { get; private set; }
        public DateTime DataEHora { get; private set; }
    }
}