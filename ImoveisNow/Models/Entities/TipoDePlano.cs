﻿using System.Collections.Generic;

namespace ImoveisNow.Models.Entities
{
    public class TipoDePlano
    {
        public TipoDePlano(int id, string nome, int diasDeDuracao, int limiteDeAnuncios, int limiteDeDestaques)
        {
            Id = id;
            Nome = nome;
            DiasDeDuracao = diasDeDuracao;
            LimiteDeAnuncios = limiteDeAnuncios;
            LimiteDeDestaques = limiteDeDestaques;
        }

        protected TipoDePlano()
        {

        }

        public int Id { get; private set; }
        public string Nome { get; private set; }
        public int DiasDeDuracao { get; private set; }
        public int LimiteDeAnuncios { get; private set; }
        public int LimiteDeDestaques { get; private set; }

        public ICollection<PlanoDoAnunciante> ListaDePlanosDoAnunciante { get; private set; }
    }
}