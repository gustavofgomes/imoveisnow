﻿using System;

namespace ImoveisNow.Models.Entities
{
    public class Imovel
    {
        public Imovel(Guid id,
            int tipoDeOfertaId,
            int tipoDeImovelId,
            decimal? areaUtil,
            bool condominio,
            bool escriturado,
            bool financiavel,
            bool areaMurada,
            bool acessoAsfaltado,
            bool piscina,
            bool churrasqueira,
            Endereco endereco,
            InformacoesDoCondominio informacoesDoCondominio,
            DetalhesDoTerreno detalhesDoTerreno)
        {
            Id = id;
            TipoDeOfertaId = tipoDeOfertaId;
            TipoDeImovelId = tipoDeImovelId;
            AreaUtil = areaUtil;
            Condominio = condominio;
            Escriturado = escriturado;
            Financiavel = financiavel;
            AreaMurada = areaMurada;
            AcessoAsfaltado = acessoAsfaltado;
            Piscina = piscina;
            Churrasqueira = churrasqueira;
            Endereco = endereco;
            InformacoesDoCondominio = informacoesDoCondominio;
            DetalhesDoTerreno = detalhesDoTerreno;
        }

        public Imovel(Guid id,
            int tipoDeOfertaId,
            int tipoDeImovelId,
            decimal? areaUtil,
            bool condominio,
            bool escriturado,
            bool financiavel,
            bool areaMurada,
            bool acessoAsfaltado,
            bool piscina,
            bool churrasqueira,
            Endereco endereco,
            InformacoesDoCondominio informacoesDoCondominio,
            DetalhesDoImovel detalhesDoImovel)
        {
            Id = id;
            TipoDeOfertaId = tipoDeOfertaId;
            TipoDeImovelId = tipoDeImovelId;
            AreaUtil = areaUtil;
            Condominio = condominio;
            Escriturado = escriturado;
            Financiavel = financiavel;
            AreaMurada = areaMurada;
            AcessoAsfaltado = acessoAsfaltado;
            Piscina = piscina;
            Churrasqueira = churrasqueira;
            Endereco = endereco;
            InformacoesDoCondominio = informacoesDoCondominio;
            DetalhesDoImovel = detalhesDoImovel;
        }

        protected Imovel()
        {

        }

        public Guid Id { get; private set; }
        public int TipoDeOfertaId { get; private set; }
        public int TipoDeImovelId { get; private set; }
        public decimal? AreaUtil { get; private set; }
        public bool Condominio { get; private set; }
        public bool Escriturado { get; private set; }
        public bool Financiavel { get; private set; }
        public bool AreaMurada { get; private set; }
        public bool AcessoAsfaltado { get; private set; }
        public bool Piscina { get; private set; }
        public bool Churrasqueira { get; private set; }

        public Anuncio Anuncio { get; private set; }
        public Endereco Endereco { get; private set; }
        public TipoDeOferta TipoDeOferta { get; private set; }
        public TipoDoImovel TipoDoImovel { get; private set; }
        public InformacoesDoCondominio InformacoesDoCondominio { get; private set; }
        public DetalhesDoImovel DetalhesDoImovel { get; private set; }
        public DetalhesDoTerreno DetalhesDoTerreno { get; private set; }

        public static decimal CalcularPrimeiraParcela(decimal valorASerFinanciado)
        {
            var taxa = new decimal(0.0075);
            var amortizacao = valorASerFinanciado / 360;
            var juros = taxa * valorASerFinanciado;
            var primeiraParcela = amortizacao + juros;
            return primeiraParcela;
        }

        public static decimal CalcularUltimaParcela(decimal valorASerFinanciado)
        {
            var taxa = new decimal(0.0075);
            var amortizacao = valorASerFinanciado / 360;
            var juros = taxa * (valorASerFinanciado - 359 * amortizacao);
            var ultimaParcela = juros + amortizacao;
            return ultimaParcela;
        }
    }
}