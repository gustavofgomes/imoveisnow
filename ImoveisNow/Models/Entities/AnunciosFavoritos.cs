﻿using System;

namespace ImoveisNow.Models.Entities
{
    public class AnunciosFavoritos
    {
        public AnunciosFavoritos(Guid anuncioId, Guid anuncianteId)
        {
            AnuncioId = anuncioId;
            AnuncianteId = anuncianteId;
        }

        protected AnunciosFavoritos()
        {

        }

        public int Id { get; private set; }
        public Guid AnuncioId { get; private set; }
        public Guid AnuncianteId { get; private set; }

        public Anuncio Anuncio { get; private set; }
        public Anunciante Anunciante { get; private set; }
    }
}