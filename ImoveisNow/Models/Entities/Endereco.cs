﻿using System;

namespace ImoveisNow.Models.Entities
{
    public class Endereco
    {
        public Endereco(string cep, string bairro, string logradouro, string numero)
        {
            Cep = cep;
            Bairro = bairro;
            Logradouro = logradouro;
            Numero = numero;
        }

        protected Endereco()
        {

        }

        public Guid Id { get; private set; }
        public string Cep { get; private set; }
        public string Bairro { get; private set; }
        public string Logradouro { get; private set; }
        public string Numero { get; private set; }

        public Imovel Imovel { get; private set; }
    }
}