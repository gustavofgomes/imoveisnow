﻿using System.Collections.Generic;

namespace ImoveisNow.Models.Entities
{
    public class TipoDoImovel
    {
        public TipoDoImovel(int id, string nome)
        {
            Id = id;
            Nome = nome;
        }

        protected TipoDoImovel()
        {

        }

        public int Id { get; private set; }
        public string Nome { get; private set; }

        public ICollection<Imovel> ListaDeImoveis { get; private set; }
    }
}