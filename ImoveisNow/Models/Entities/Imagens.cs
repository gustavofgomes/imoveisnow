﻿using System;

namespace ImoveisNow.Models.Entities
{
    public class Imagens
    {
        public Imagens(string nome, string url, string formato, string tamanho)
        {
            Nome = nome;
            Url = url;
            Formato = formato;
            Tamanho = tamanho;
        }

        protected Imagens()
        {

        }

        public int Id { get; private set; }
        public Guid AnuncioId { get; private set; }
        public string Nome { get; private set; }
        public string Url { get; private set; }
        public string Formato { get; private set; }
        public string Tamanho { get; private set; }
        public int Largura { get; private set; }
        public int Altura { get; private set; }

        public Anuncio Anuncio { get; private set; }
    }
}
