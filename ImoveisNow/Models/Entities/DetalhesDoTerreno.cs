﻿using System;

namespace ImoveisNow.Models.Entities
{
    public class DetalhesDoTerreno
    {
        public DetalhesDoTerreno(Guid id, bool planado, bool limpo, bool aguaEncanada, bool pocoArtesiano, bool energiaEletrica, bool areaVerde, bool casaSede)
        {
            Id = id;
            Planado = planado;
            Limpo = limpo;
            AguaEncanada = aguaEncanada;
            PocoArtesiano = pocoArtesiano;
            EnergiaEletrica = energiaEletrica;
            AreaVerde = areaVerde;
            CasaSede = casaSede;
        }

        protected DetalhesDoTerreno()
        {

        }

        public Guid Id { get; private set; }
        public bool Planado { get; private set; }
        public bool Limpo { get; private set; }
        public bool AguaEncanada { get; private set; }
        public bool PocoArtesiano { get; private set; }
        public bool EnergiaEletrica { get; private set; }
        public bool AreaVerde { get; private set; }
        public bool CasaSede { get; private set; }

        public Imovel Imovel { get; private set; }
    }
}