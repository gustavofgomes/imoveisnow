﻿using System.Collections.Generic;

namespace ImoveisNow.Models.Entities
{
    public class TipoDeOferta
    {
        public TipoDeOferta(int id, string nome, string descricao)
        {
            Id = id;
            Nome = nome;
            Descricao = descricao;
        }

        protected TipoDeOferta()
        {

        }

        public int Id { get; private set; }
        public string Nome { get; private set; }
        public string Descricao { get; private set; }

        public ICollection<Imovel> ListaDeImoveis { get; private set; }
    }
}