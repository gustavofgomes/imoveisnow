﻿using System;

namespace ImoveisNow.Models.Entities
{
    public class DetalhesDoImovel
    {
        public DetalhesDoImovel(Guid id, int numeroDeQuartos, int numeroDeBanheiros, int numeroDeSuites, int vagasDeGaragem, bool garagemCoberta, bool mobiliado, bool quintal, bool jardim)
        {
            Id = id;
            NumeroDeQuartos = numeroDeQuartos;
            NumeroDeBanheiros = numeroDeBanheiros;
            NumeroDeSuites = numeroDeSuites;
            VagasDeGaragem = vagasDeGaragem;
            GaragemCoberta = garagemCoberta;
            Mobiliado = mobiliado;
            Quintal = quintal;
            Jardim = jardim;
        }

        protected DetalhesDoImovel()
        {

        }

        public Guid Id { get; private set; }
        public int NumeroDeQuartos { get; private set; }
        public int NumeroDeBanheiros { get; private set; }
        public int NumeroDeSuites { get; private set; }
        public int VagasDeGaragem { get; private set; }
        public bool GaragemCoberta { get; private set; }
        public bool Mobiliado { get; private set; }
        public bool Quintal { get; private set; }
        public bool Jardim { get; private set; }

        public Imovel Imovel { get; private set; }
    }
}