﻿using System.Collections.Generic;

namespace ImoveisNow.Models.Entities
{
    public class Estado
    {
        protected Estado()
        {

        }
        public Estado(string nome, string sigla)
        {
            Nome = nome;
            Sigla = sigla;
        }

        public int Id { get; private set; }
        public string Nome { get; private set; }
        public string Sigla { get; private set; }

        public ICollection<Cidade> ListaDeCidades { get; private set; }
    }
}
