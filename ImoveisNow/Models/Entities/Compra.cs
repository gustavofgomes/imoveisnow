﻿using System;

namespace ImoveisNow.Models.Entities
{
    public class Compra
    {
        public Compra( Guid anuncianteId, string status, string formaDePagamento, DateTime dateEHora)
        {
            AnuncianteId = anuncianteId;
            Status = status;
            FormaDePagamento = formaDePagamento;
            DateEHora = dateEHora;
        }

        protected Compra()
        {

        }

        public int Id { get; private set; }
        public Guid AnuncianteId { get; private set; }
        public string Status { get; private set; }
        public string FormaDePagamento { get; private set; }
        public DateTime DateEHora { get; private set; }

        public Anunciante Anunciante { get; private set; }
    }
}