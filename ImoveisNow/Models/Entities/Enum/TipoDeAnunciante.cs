﻿using System.ComponentModel;

namespace ImoveisNow.Models.Entities.Enum
{
    public enum TipoDeAnunciante
    {
        [Description("Proprietário")]
        Proprietario,
        [Description("Imobiliária")]
        Imobiliaria,
        [Description("Corretor")]
        Corretor
    }
}