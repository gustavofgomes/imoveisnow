﻿using System.ComponentModel;

namespace ImoveisNow.Models.Entities.Enum
{
    public enum MotivoDaDenuncia
    {
        [Description("Suspeita de golpe")]
        SuspeitaDeGolpe,
        [Description("Imóvel já foi vendido")]
        ImovelJaVendido,
        [Description("Categoria incorreta")]
        CategoriaIncorreta,
        [Description("Anúncio duplicado")]
        AnuncioDuplicado,
        [Description("Preço errado ou enganoso")]
        PrecoErradoOuEnganoso,
        [Description("Imóvel não existe")]
        ImovelNaoExiste,
        [Description("Outros")]
        Outros
    }
}