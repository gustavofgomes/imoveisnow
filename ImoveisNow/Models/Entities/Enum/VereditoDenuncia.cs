﻿using System.ComponentModel;

namespace ImoveisNow.Models.Entities.Enum
{
    public enum VereditoDenuncia
    {
        [Description("Não Avaliada")]
        NaoAvaliada,
        [Description("Aprovada")]
        Aprovada,
        [Description("Negada")]
        Negada,
    }
}