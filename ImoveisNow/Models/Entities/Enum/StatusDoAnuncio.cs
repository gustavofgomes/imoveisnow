﻿using System.ComponentModel;

namespace ImoveisNow.Models.Entities.Enum
{
    public enum StatusDoAnuncio
    {
        [Description("Ativo")]
        Ativo,
        [Description("Em Destaque")]
        EmDestaque,
        [Description("Fechado Por Denúncias")]
        FechadoPorDenuncia,
        [Description("Expirado")]
        Expirado,
        [Description("Finalizado")]
        Finalizado
    }
}