﻿using System;
using System.Collections.Generic;

namespace ImoveisNow.Models.Entities
{
    public class Anunciante
    {
        public Anunciante(Guid id, string nome, string email, string telefone, int cidadeId, string creci, DateTime ultimoAcesso, string tipoDeAnunciante, bool ativo)
        {
            Id = id;
            Nome = nome;
            Email = email;
            Telefone = telefone;
            CidadeId = cidadeId;
            CRECI = creci;
            DataDeCadastro = DateTime.Now;
            UltimoAcesso = ultimoAcesso;
            TipoDeAnunciante = tipoDeAnunciante;
            Ativo = ativo;
        }

        protected Anunciante()
        {

        }

        public Guid Id { get; private set; }
        public string Nome { get; private set; }
        public string Email { get; private set; }
        public string Telefone { get; private set; }
        public int CidadeId { get; private set; }
        public string CRECI { get; private set; }
        public DateTime DataDeCadastro { get; private set; }
        public DateTime UltimoAcesso { get; private set; }
        public string TipoDeAnunciante { get; private set; }
        public bool Ativo { get; private set; }

        public Cidade Cidade { get; private set; }
        public ICollection<PlanoDoAnunciante> ListaDePlanoDoAnunciante { get; private set; }
        public ICollection<Anuncio> ListaDeAnuncios { get; private set; }
        public ICollection<AnunciosFavoritos> ListaDeFavoritos { get; private set; }
        public ICollection<Compra> ListaDeCompras { get; private set; }
    }
}