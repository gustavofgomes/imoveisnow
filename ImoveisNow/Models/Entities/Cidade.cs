﻿using System.Collections.Generic;

namespace ImoveisNow.Models.Entities
{
    public class Cidade
    {
        public Cidade(string nome, int estado)
        {
            Nome = nome;
            EstadoId = estado;
        }

        protected Cidade()
        {

        }

        public int Id { get; private set; }
        public string Nome { get; private set; }
        public int EstadoId { get; private set; }

        public Estado Estado { get; private set; }
        public ICollection<Anunciante> ListaDeAnunciantes { get; private set; }
        public ICollection<Anuncio> ListaDeAnuncios { get; private set; }
    }
}