﻿using System;

namespace ImoveisNow.Models.Entities
{
    public class InformacoesDoCondominio
    {
        public InformacoesDoCondominio(string nome, decimal valorDoCondominio, bool fechado, bool sistemaDeVigilancia, bool portaoEletronico, bool permiteAnimais, bool elevador)
        {
            Nome = nome;
            ValorDoCondominio = valorDoCondominio;
            Fechado = fechado;
            SistemaDeVigilancia = sistemaDeVigilancia;
            PortaoEletronico = portaoEletronico;
            PermiteAnimais = permiteAnimais;
            Elevador = elevador;
        }

        protected InformacoesDoCondominio()
        {

        }

        public Guid Id { get; private set; }
        public string Nome { get; private set; }
        public decimal ValorDoCondominio { get; private set; }
        public bool Fechado { get; private set; }
        public bool SistemaDeVigilancia { get; private set; }
        public bool PortaoEletronico { get; private set; }
        public bool PermiteAnimais { get; private set; }
        public bool Elevador { get; private set; }

        public Imovel Imovel { get; private set; }
    }
}