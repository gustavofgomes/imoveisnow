﻿using System;

namespace ImoveisNow.Models.Entities
{
    public class PlanoDoAnunciante
    {
        public PlanoDoAnunciante(int id, int tipoDePlanoId, Guid anuncianteId, DateTime dataDeTermino, bool ativo)
        {
            Id = id;
            TipoDePlanoId = tipoDePlanoId;
            AnuncianteId = anuncianteId;
            DataDeInicio = DateTime.Now;
            DataDeTermino = dataDeTermino;
            Ativo = Ativo;
        }

        protected PlanoDoAnunciante()
        {

        }

        public int Id { get; private set; }
        public int TipoDePlanoId { get; private set; }
        public Guid AnuncianteId { get; private set; }
        public DateTime DataDeInicio { get; private set; }
        public DateTime DataDeTermino { get; private set; }
        public bool Ativo { get; private set; }

        public TipoDePlano TipoDePlano { get; private set; }
        public Anunciante Anunciante { get; private set; }

        public PlanoDoAnunciante GerarPlano(Guid anuncianteId, TipoDePlano tipoDePlano)
        {
            return new PlanoDoAnunciante
            {
                Id = 0,
                TipoDePlanoId = tipoDePlano.Id,
                AnuncianteId = anuncianteId,
                DataDeInicio = DateTime.Now,
                DataDeTermino = DateTime.Now.AddDays(tipoDePlano.DiasDeDuracao),
                Ativo = true
            };
        }
    }
}