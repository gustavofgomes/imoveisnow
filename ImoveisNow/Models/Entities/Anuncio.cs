﻿using System;
using System.Collections.Generic;

namespace ImoveisNow.Models.Entities
{
    public class Anuncio
    {
        public Anuncio(Guid id,
            Imovel imovel,
            decimal valor,
            string descricao,
            Guid anuncianteId,
            int cidadeId,
            bool ocultarTelefone,
            string status,
            ICollection<Imagens>imagens,
            TipoDeOferta tipoDeOferta,
            TipoDoImovel tipoDoImovel)
        {
            Id = id;
            Imovel = imovel;
            Valor = valor;
            Titulo = GerarTitulo(tipoDeOferta, tipoDoImovel);
            Descricao = descricao;
            DataDeCriacao = DateTime.Now;
            AnuncianteId = anuncianteId;
            CidadeId = cidadeId;
            OcultarTelefone = ocultarTelefone;
            Status = status;
            ListaDeImagens = imagens;
        }

        protected Anuncio()
        {

        }

        public Guid Id { get; private set; }
        public decimal Valor { get; private set; }
        public string Titulo { get; private set; }
        public string Descricao { get; private set; }
        public DateTime DataDeCriacao { get; private set; }
        public Guid AnuncianteId { get; private set; }
        public int CidadeId { get; private set; }
        public bool OcultarTelefone { get; private set; }
        public string Status { get; private set; }
        public int Peso { get; private set; }

        public Anunciante Anunciante { get; private set; }
        public Cidade Cidade { get; private set; }
        public Imovel Imovel { get; private set; }
        public ICollection<ContatosRecebidos> ListaDeContatosRecebidos { get; private set; }
        public ICollection<Visualizacao> ListaDeVisualizacoes { get; private set; }
        public ICollection<Denuncia> ListaDeDenuncias { get; private set; }
        public ICollection<Imagens> ListaDeImagens { get; private set; }
        public ICollection<AnunciosFavoritos> ListaDeFavoritos { get; private set; }

        public string GerarTitulo(TipoDeOferta tipoDeOferta, TipoDoImovel tipoDoImovel)
        {
            //Tipo Do Imóvel, //Categoria, Area, Quartos
            //Verificar se é 1 Quarto ou mais de 1.
            //Casa á venda, 200m² com 2 Quartos
            if (Imovel.DetalhesDoImovel != null)
                return tipoDoImovel.Nome + " " + tipoDeOferta.Descricao + ", " + Imovel.AreaUtil.ToString() + "m² com " + Imovel.DetalhesDoImovel.NumeroDeQuartos + " Quartos";

            else
                return tipoDoImovel.Nome + " " + tipoDeOferta.Descricao + ", " + Imovel.AreaUtil.ToString() + "m²";
        }

        public int CalcularPeso()
        {
            var peso = 40;

            switch (ListaDeImagens.Count)
            {
                case 0:
                    peso -= 5;
                    break;
                case 1:
                    peso += 5;
                    break;
                case int n when (n >= 5 && n <= 10):
                    peso += 10;
                    break;
                case int n when (n >= 10):
                    peso += 15;
                    break;
                default:
                    peso += 0;
                    break;
            }
            //55

            if (Descricao.Length > 200)
                peso += 10;
            //65

            if (Imovel.AreaUtil != null)
                peso += 10;
            //75

            if (Imovel.Endereco != null)
                peso += 10;
            //85

            if (this.Status == "Em Destaque")
                peso += 25;
            //110

            if (peso > 100)
                peso = 100;

            return peso;
        }
    }
}