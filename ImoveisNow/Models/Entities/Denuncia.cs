﻿using ImoveisNow.Models.Entities.Enum;
using ImoveisNow.Models.Helpers;
using System;

namespace ImoveisNow.Models.Entities
{
    public class Denuncia
    {
        public Denuncia(Guid anuncioId, string texto, string motivo, bool avaliada)
        {
            AnuncioId = anuncioId;
            Texto = texto;
            Motivo = motivo;
            DataEHoraDeCriacao = DateTime.Now;
            Avaliada = avaliada;
            Veredito = VereditoDenuncia.NaoAvaliada.GetDescription();
        }

        protected Denuncia()
        {

        }

        public int Id { get; private set; }
        public Guid AnuncioId { get; private set; }
        public string Texto { get; private set; }
        public string Motivo { get; private set; }
        public DateTime DataEHoraDeCriacao { get; private set; }
        public DateTime? AvalidaEm { get; private set; }
        public bool Avaliada { get; private set; }
        public string Veredito { get; private set; }

        public Anuncio Anuncio { get; private set; }

        public void DenunciaAprovada()
        {
            AvalidaEm = DateTime.Now;
            Avaliada = true;
            Veredito = VereditoDenuncia.Aprovada.GetDescription();
        }
        public void DenunciaNegada()
        {
            AvalidaEm = DateTime.Now;
            Avaliada = true;
            Veredito = VereditoDenuncia.Negada.GetDescription();
        }
    }
}