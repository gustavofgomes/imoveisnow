﻿using System;

namespace ImoveisNow.Models.Entities
{
    public class ContatosRecebidos
    {
        public ContatosRecebidos(int id, Guid anuncioId, string nome, string telefone, bool contatoViaWhatsApp, string email, string mensagem, bool arquivado)
        {
            Id = id;
            AnuncioId = anuncioId;
            DataEHora = DateTime.Now;
            Nome = nome;
            Telefone = telefone;
            ContatoViaWhatsApp = contatoViaWhatsApp;
            Email = email;
            Mensagem = mensagem;
            Arquivado = arquivado;
        }

        protected ContatosRecebidos()
        {

        }

        public int Id { get; private set; }
        public Guid AnuncioId { get; private set; }
        public DateTime DataEHora { get; private set; }
        public string Nome { get; private set; }
        public string Telefone { get; private set; }
        public bool ContatoViaWhatsApp { get; private set; }
        public string Email { get; private set; }
        public string Mensagem { get; private set; }
        public bool Arquivado { get; private set; }

        public Anuncio Anuncio { get; private set; }

        public void Arquivar()
        {
            Arquivado = true;
        }
    }
}