﻿namespace ImoveisNow.Models.DTO
{
    public class FotosDTO
    {
        public string Nome { get; set; }
        public string URL { get; set; }
        public string Formato { get; set; }
        public string Tamanho { get; set; }
    }
}