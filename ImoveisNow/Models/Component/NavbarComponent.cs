﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ImoveisNow.Models.Component
{
    public class NavbarComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            if (User.Identity.IsAuthenticated)
            {
                return View("~/Views/Shared/_NavbarSair.cshtml");
            }
            else
            {
                return View("~/Views/Shared/_NavbarEntrar.cshtml");
            }
        }
    }
}
