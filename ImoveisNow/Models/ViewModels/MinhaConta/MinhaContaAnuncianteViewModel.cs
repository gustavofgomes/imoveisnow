﻿using ImoveisNow.Models.ViewModels.Anuncio;
using ImoveisNow.Models.ViewModels.Feedback;
using System;
using System.Collections.Generic;

namespace ImoveisNow.Models.ViewModels.MinhaConta
{
    public class MinhaContaAnuncianteViewModel
    {
        public string Plano { get; set; }
        public int LimiteDeAnuncios { get; set; }
        public int LimiteDeDestaques { get; set; }
        public DateTime Expira { get; set; }

        public CriarFeedBackViewModel CriarFeedBack { get; set; }
        public ICollection<AnunciosFavoritosViewModel> AnunciosFavoritos { get; set; }
        public ICollection<ContatosRecebidosViewModel> ContatosRecebidos { get; set; }
    }
}