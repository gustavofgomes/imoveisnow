﻿using ImoveisNow.Models.Entities;
using ImoveisNow.Models.ViewModels.Anuncio;
using System;
using System.Collections.Generic;

namespace ImoveisNow.Models.ViewModels.MinhaConta
{
    public static class MinhaContaFactory
    {
        public static MinhaContaAdminViewModel CriarMinhaContaAdminViewModel(int numeroDeVisualizacoes, int numeroDeAnuncios, int numeroDeAnunciantes, int numeroDePlanosPremiums, ICollection<Entities.Denuncia> denuncias, ICollection<Entities.Feedback> feedback)
        {
            return new MinhaContaAdminViewModel
            {
                NumeroDeVisualizacoes = numeroDeVisualizacoes,
                NumeroDeAnunciosAtivos = numeroDeAnuncios,
                NumeroDeAnunciantesAtivos = numeroDeAnunciantes,
                NumeroDePlanosPremiumAtivos = numeroDePlanosPremiums,
                Denuncias = CriarListaDeDenunciasViewModel(denuncias),
                Feedbacks = CriarListaDeFeedbacks(feedback)
            };
        }

        public static ICollection<ListaDeFeedBacks> CriarListaDeFeedbacks(ICollection<Entities.Feedback> feedbacks)
        {
            var listaDeDenunciasViewModel = new List<ListaDeFeedBacks>();

            foreach (var feedback in feedbacks)
            {
                listaDeDenunciasViewModel.Add(new ListaDeFeedBacks
                {
                    Id = feedback.Id,
                    DataEHora = feedback.DataEHora.Date,
                });
            }
            return listaDeDenunciasViewModel;
        }

        public static ICollection<ListaDeDenunciaViewModel> CriarListaDeDenunciasViewModel(ICollection<Entities.Denuncia> denuncias)
        {
            var listaDeDenunciasViewModel = new List<ListaDeDenunciaViewModel>();

            foreach (var denuncia in denuncias)
            {
                listaDeDenunciasViewModel.Add(new ListaDeDenunciaViewModel
                {
                    Id = denuncia.Id,
                    DataEHora = denuncia.DataEHoraDeCriacao.Date,
                    Motivo = denuncia.Motivo
                });
            }
            return listaDeDenunciasViewModel;
        }

        public static MinhaContaAnuncianteViewModel CriarMinhaContaAnuncianteViewModel(ICollection<Entities.AnunciosFavoritos> anunciosFavoritos, ICollection<Entities.ContatosRecebidos> contatosRecebidos)
        {
            return new MinhaContaAnuncianteViewModel
            {
                CriarFeedBack = new Feedback.CriarFeedBackViewModel(),
                AnunciosFavoritos = CriarAnunciosFavoritosViewModel(anunciosFavoritos),
                ContatosRecebidos = CriarContatosRecebidosViewModel(contatosRecebidos)
            };
        }

        private static ICollection<ContatosRecebidosViewModel> CriarContatosRecebidosViewModel(ICollection<ContatosRecebidos> contatosRecebidos)
        {
            var listaDeContatosRecebidos = new List<ContatosRecebidosViewModel>();

            foreach (var contato in contatosRecebidos)
            {
                listaDeContatosRecebidos.Add(new ContatosRecebidosViewModel
                {
                    Id = contato.Id,
                    Titulo = contato.Anuncio.Titulo,
                });
            }
            return listaDeContatosRecebidos;
        }

        public static ICollection<AnunciosFavoritosViewModel> CriarAnunciosFavoritosViewModel(ICollection<Entities.AnunciosFavoritos> anunciosFavoritos)
        {
            var listaDeAnunciosFavoritos = new List<AnunciosFavoritosViewModel>();

            foreach (var favorito in anunciosFavoritos)
            {
                listaDeAnunciosFavoritos.Add(new AnunciosFavoritosViewModel
                {
                    Id = favorito.AnuncioId,
                    Titulo = favorito.Anuncio.Titulo,
                });
            }
            return listaDeAnunciosFavoritos;
        }

        public static ContatoRecebidoViewModel CriarContato(ContatosRecebidos contato)
        {
            return new ContatoRecebidoViewModel
            {
                Id = contato.Id,
                Titulo = contato.Anuncio.Titulo,
                Nome = contato.Nome,
                ContatoViaWhats = contato.ContatoViaWhatsApp ? "Sim" : null,
                Telefone = contato.Telefone,
                Texto = contato.Mensagem,
                DataEHora = contato.DataEHora.Date,
            };
        }
    }
}