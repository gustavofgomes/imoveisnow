﻿using System;

namespace ImoveisNow.Models.ViewModels.MinhaConta
{
    public class ContatoRecebidoViewModel
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public DateTime DataEHora { get; set; }
        public string Nome { get; set; }
        public string Telefone { get; set; }
        public string ContatoViaWhats { get; set; }
        public string Texto { get; set; }
    }
}