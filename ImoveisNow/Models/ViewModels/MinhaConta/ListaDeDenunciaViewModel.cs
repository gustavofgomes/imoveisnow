﻿using System;

namespace ImoveisNow.Models.ViewModels.MinhaConta
{
    public class ListaDeDenunciaViewModel
    {
        public int Id { get; set; }
        public DateTime DataEHora { get; set; }
        public string Motivo { get; set; }
    }
}