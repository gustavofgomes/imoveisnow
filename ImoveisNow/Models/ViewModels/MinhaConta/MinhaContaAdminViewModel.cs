﻿using System.Collections.Generic;

namespace ImoveisNow.Models.ViewModels.MinhaConta
{
    public class MinhaContaAdminViewModel
    {
        public int NumeroDeVisualizacoes { get; set; }
        public int NumeroDeAnunciosAtivos { get; set; }
        public int NumeroDeAnunciantesAtivos { get; set; }
        public int NumeroDePlanosPremiumAtivos { get; set; }

        public ICollection<ListaDeDenunciaViewModel> Denuncias { get; set; }
        public ICollection<ListaDeFeedBacks> Feedbacks { get; set; }
    }
}