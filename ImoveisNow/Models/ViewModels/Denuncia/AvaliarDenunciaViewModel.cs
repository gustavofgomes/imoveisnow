﻿using System;

namespace ImoveisNow.Models.ViewModels.Denuncia
{
    public class AvaliarDenunciaViewModel
    {
        public int Id { get; set; }
        public Guid AnuncioId { get; set; }
        public string Motivo { get; set; }
        public string Texto { get; set; }
        public DateTime DataEHoraDeCriacao { get; set; }
    }
}