﻿namespace ImoveisNow.Models.ViewModels.Denuncia
{
    public static class DenunciaFactory
    {
        public static Entities.Denuncia CriarDenuncia(DenunciarAnuncioViewModel denunciarAnuncioViewModel)
        {
            var avalida = false;

            return new Entities.Denuncia(
                denunciarAnuncioViewModel.AnuncioId,
                denunciarAnuncioViewModel.Texto,
                denunciarAnuncioViewModel.Motivo,
                avalida);
        }

        public static AvaliarDenunciaViewModel CriarAvaliarDenunciaViewModel(Entities.Denuncia denuncia)
        {
            return new AvaliarDenunciaViewModel
            {
                Id = denuncia.Id,
                AnuncioId = denuncia.AnuncioId,
                Motivo = denuncia.Motivo,
                Texto = denuncia.Texto,
                DataEHoraDeCriacao = denuncia.DataEHoraDeCriacao
            };
        }
    }
}