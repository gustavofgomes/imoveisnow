﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ImoveisNow.Models.ViewModels.Denuncia
{
    public class DenunciarAnuncioViewModel
    {
        public Guid AnuncioId { get; set; }
        [Required(ErrorMessage = "Informe o Problema Encontrado para continuar.")]
        [DisplayName("QUAL PROBLEMA FOI ENCONTRANDO?")]
        public string Texto { get; set; }
        [Required(ErrorMessage = "Informe o Tipo de Problema para continuar.")]
        [DisplayName("TIPO DE PROBLEMA")]
        public string Motivo { get; set; }

        public List<string> ListaDeMotivos { get; set; }
    }
}