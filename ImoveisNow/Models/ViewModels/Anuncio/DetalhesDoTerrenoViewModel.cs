﻿namespace ImoveisNow.Models.ViewModels.Anuncio
{
    public class DetalhesDoTerrenoViewModel
    {
        public string Planado { get; set; }
        public string Limpo { get; set; }
        public string AguaEncanada { get; set; }
        public string PocoArtesiano { get; set; }
        public string EnergiaEletrica { get; set; }
        public string AreaVerde { get; set; }
        public string CasaSede { get; set; }
    }
}