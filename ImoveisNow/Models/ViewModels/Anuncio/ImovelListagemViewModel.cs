﻿namespace ImoveisNow.Models.ViewModels.Anuncio
{
    public class ImovelListagemViewModel
    {
        public int Quartos { get; set; }
        public int Banheiros { get; set; }
        public int Vagas { get; set; }
        public string ValorDoCondominio { get; set; }
    }
}
