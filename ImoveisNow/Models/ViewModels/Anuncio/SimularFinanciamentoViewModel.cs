﻿namespace ImoveisNow.Models.ViewModels.Anuncio
{
    public class SimularFinanciamentoViewModel
    {
        public string ValorDoImovel { get; set; }
        public string Entrada { get; set; }
        public int Prazo { get; set; }
        public string ValorASerFinanciado { get; set; }
        public string PrimeiraParcela { get; set; }
        public string UltimaParcela { get; set; }
    }
}