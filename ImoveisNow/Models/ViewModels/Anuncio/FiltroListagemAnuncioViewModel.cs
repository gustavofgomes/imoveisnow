﻿namespace ImoveisNow.Models.ViewModels.Anuncio
{ 
    public class FiltroListagemAnuncioViewModel
    {
        public string Parametro { get; set; }
        public string PrecoOrdenadoPor { get; set; }
        public int NumeroDeQuartos { get; set; }
        public int PrecoMinimo { get; set; }
        public int PrecoMaximo { get; set; }
        public int AreaMinima { get; set; }
        public int AreaMaxima { get; set; }
    }
}