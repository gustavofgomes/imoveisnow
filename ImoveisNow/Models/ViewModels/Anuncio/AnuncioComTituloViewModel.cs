﻿using System;
using System.Collections.Generic;

namespace ImoveisNow.Models.ViewModels.Anuncio
{
    public class AnuncioComTituloViewModel
    {
        public Guid Id { get; set; }
        public ICollection<string> UrlImagem { get; set; }
        public string Titulo { get; set; }
        public decimal Valor { get; set; }
        public string MetrosQuadrados { get; set; }
        public string Descricao { get; set; }
        public string Cidade { get; set; }
        public string Bairro { get; set; }

        public ImovelListagemViewModel Imovel { get; set; }
    }
}
