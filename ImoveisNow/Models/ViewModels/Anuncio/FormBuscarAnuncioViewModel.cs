﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ImoveisNow.Models.ViewModels.Anuncio

{
    public class BuscarAnuncioViewModel
    {
        public string Parametro { get; set; }
        public int Categoria { get; set; }
        public int NumeroDeQuartos { get; set; }
        public int NumeroDeBanheiros { get; set; }
        public int VagasDeGaragem { get; set; }
        [Display(Name = "Tipo de Imóvel")]
        public int TipoDeImovelId { get; set; }
        public int PrecoMinimo { get; set; }
        public int PrecoMaximo { get; set; }
        public int AreaMinima { get; set; }
        public int AreaMaxima { get; set; }
        [Display(Name = "Financiável")]
        public bool Financiavel { get; set; }
        public bool Escriturado { get; set; }

        public Dictionary<int, string> ListaTipoDeImovel { get; set; }
    }
}