﻿using ImoveisNow.Models.ViewModels.Denuncia;
using System;
using System.Collections.Generic;

namespace ImoveisNow.Models.ViewModels.Anuncio
{
    public class DetalhesDoAnuncioViewModel
    {
        public Guid Id { get; set; }
        public ICollection<string> UrlImagem { get; set; }
        public string Valor { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public DateTime DataDeCriacao { get; set; }
        public string Cidade { get; set; }
        public bool OcultarTelefone { get; set; }
        public string TipoDeOferta { get; set; }
        public string TipoDeImovel { get; set; }
        public string AreaUtil { get; set; }
        public bool Condominio { get; set; }
        public bool Escriturado { get; set; }
        public bool Financiavel { get; set; }
        public string AreaMurada { get; set; }
        public string AcessoAsfaltado { get; set; }
        public string Piscina { get; set; }
        public string Churrasqueira { get; set; }
        public bool Favorito { get; set; }

        public InformacoesDoAnuncianteViewModel Anunciante { get; set; }
        public InformacoesDoEnderecoViewModel Endereco { get; set; }
        public SimularFinanciamentoViewModel SimularFinanciamento { get; set; }
        public DenunciarAnuncioViewModel DenunciarAnuncio { get; set; }
        public DetalhesDoImovelViewModel DetalhesDoImovel { get; set; }
        public InformacoesDoCondominioViewModel InformacoesDoCondominio { get; set; }
        public DetalhesDoTerrenoViewModel DetalhesDoTerreno { get; set; }
        public ContatarAnuncianteViewModel ContatarAnunciante { get; set; }
    }
}