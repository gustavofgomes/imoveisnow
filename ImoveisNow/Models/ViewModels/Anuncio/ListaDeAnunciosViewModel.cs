﻿using System.Collections.Generic;

namespace ImoveisNow.Models.ViewModels.Anuncio
{
    public class ListaDeAnunciosViewModel
    {
        public ICollection<AnuncioComTituloViewModel> Anuncios { get; set; }
        public FiltroListagemAnuncioViewModel Filtros { get; set; }
    }
}