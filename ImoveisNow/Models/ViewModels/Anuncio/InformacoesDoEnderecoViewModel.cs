﻿namespace ImoveisNow.Models.ViewModels.Anuncio
{
    public class InformacoesDoEnderecoViewModel
    {
        public string Cep { get; set; }
        public string Logradouro { get; set; }
        public string Bairro { get; set; }
        public string Numero { get; set; }
    }
}