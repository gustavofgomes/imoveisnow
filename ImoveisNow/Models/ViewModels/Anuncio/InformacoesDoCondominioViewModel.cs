﻿namespace ImoveisNow.Models.ViewModels.Anuncio
{
    public class InformacoesDoCondominioViewModel
    {
        public string CondominioNome { get; set; }
        public decimal CondominioValor { get; set; }
        public string Fechado { get; set; }
        public string SistemaDeVigilancia { get; set; }
        public string PortaoEletronico { get; set; }
        public string PermiteAnimais { get; set; }
        public string Elevador { get; set; }
    }
}