﻿using System;

namespace ImoveisNow.Models.ViewModels.Anuncio
{
    public class AnunciosFavoritosViewModel
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
    }
}