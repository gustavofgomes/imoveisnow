﻿using System;

namespace ImoveisNow.Models.ViewModels.Anuncio
{
    public class InformacoesDoAnuncianteViewModel
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
    }
}