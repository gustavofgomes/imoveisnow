﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ImoveisNow.Models.ViewModels.Anuncio
{
    public class NovoAnuncioResidencialComercialViewModel
    {
        [Display(Name = "Oferta")]
        [Required(ErrorMessage = "Informe a {0} do anúncio para continuar.")]
        public int TipoDeOfertaId { get; set; }

        [Display(Name = "Tipo de imóvel")]
        [Required(ErrorMessage = "Informe o {0} do anúncio para continuar.")]
        public int TipoDeImovelId { get; set; }

        [Display(Name = "Valor (R$)")]
        [Required(ErrorMessage = "Informe o Valor do anúncio para continuar.")]
        public string Valor { get; set; }

        [Display(Name = "Financiável")]
        public bool Financiavel { get; set; }

        public bool Escriturado { get; set; }

        [Display(Name = "Área útil (m²)")]
        [Required(ErrorMessage = "Informe a {0} do anúncio para continuar.")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Valor Inválido")]
        [MaxLength(12)]
        public string AreaUtil { get; set; }

        [Display(Name = "Terreno/Lote localizado em Condimínio?")]
        [Required(ErrorMessage = "Informe se o {0} para continuar.")]
        public bool Condominio { get; set; }

        [Display(Name = "Nome do Condomínio")]
        public string NomeDoCondominio { get; set; }

        [Display(Name = "Valor do Condomínio")]
        [Required(ErrorMessage = "Informe se o {0} para continuar.")]
        public string ValorDoCondominio { get; set; }

        public bool Fechado { get; set; }

        [Display(Name = "Sistema de Vigilância")]
        public bool SistemaDeVigilancia { get; set; }

        [Display(Name = "Portao Eletrônico")]
        public bool PortaoEletronico { get; set; }

        [Display(Name = "Permite Animais")]
        public bool PermiteAnimais { get; set; }

        public bool Elevador { get; set; }

        [Display(Name = "Área Murada")]
        public bool AreaMurada { get; set; }

        [Display(Name = "Acesso Asfaltado")]
        public bool AcessoAsfaltado { get; set; }

        public bool Piscina { get; set; }

        public bool Churrasqueira { get; set; }

        [Display(Name = "Número de quartos")]
        [Required(ErrorMessage = "Informe o {0} do anúncio para continuar.")]
        [MaxLength(3, ErrorMessage = "{0} inválido.")]
        public int NumeroDeQuartos { get; set; }

        [Display(Name = "Vagas de garagem")]
        [MaxLength(3, ErrorMessage = "{0} inválido.")]
        public int VagasDeGaragem { get; set; }

        [Display(Name = "Número de banheiros")]
        [MaxLength(3, ErrorMessage = "{0} inválido.")]
        public int NumeroDeBanheiros { get; set; }

        [Display(Name = "Número de suites")]
        [MaxLength(3, ErrorMessage = "{0} inválido.")]
        public int NumeroDeSuites { get; set; }

        public bool Mobiliado { get; set; }

        public bool Quintal { get; set; }

        public bool Jardim { get; set; }

        [Display(Name = "Garagem Coberta")]
        public bool GaragemCoberta { get; set; }

        [Display(Name = "CEP")]
        public string Cep { get; set; }

        [Display(Name = "Cidade")]
        [Required(ErrorMessage = "Informe a {0} do anúncio para continuar.")]
        public string Cidade { get; set; }

        public string Bairro { get; set; }

        public string Logradouro { get; set; }

        public string Numero { get; set; }

        [Display(Name = "Pre-visualização do Título")]
        [Required(ErrorMessage = "Informe a {0} do anúncio para continuar.")]
        [MaxLength(100, ErrorMessage = "Tamanho máximo estrapolado.")]
        public string Titulo { get; set; }

        [Display(Name = "Descrição")]
        [Required(ErrorMessage = "Informe a {0} do anúncio para continuar.")]
        [MaxLength(2000, ErrorMessage = "Tamanho máximo estrapolado.")]
        public string Descricao { get; set; }

        [Display(Name = "Fotos")]
        public List<IFormFile> Imagens { get; set; }

        [Display(Name = "Ocultar telefone neste anúncio")]
        public bool OcultarTelefone { get; set; }

        public string Telefone { get; set; }
        public Dictionary<int, string> ListaDeCidades { get; set; }
        public Dictionary<int, string> ListaTipoDeImovel { get; set; }
    }
}