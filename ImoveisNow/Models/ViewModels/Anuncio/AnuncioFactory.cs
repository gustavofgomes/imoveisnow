﻿using ImoveisNow.Models.DTO;
using ImoveisNow.Models.Entities;
using ImoveisNow.Models.Entities.Enum;
using ImoveisNow.Models.Helpers;
using ImoveisNow.Models.ViewModels.Denuncia;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ImoveisNow.Models.ViewModels.Anuncio
{
    public class AnuncioFactory
    {
        #region DetalhesDoAnuncio

        public static DetalhesDoAnuncioViewModel CriarDetalhesAnuncioViewModel(Entities.Anuncio anuncio, bool favoritado)
        {
            //Anuncio
            var detalhesDoAnuncioViewModel = new DetalhesDoAnuncioViewModel
            {
                Id = anuncio.Id,
                UrlImagem = CriarListaDeUrlImagem(anuncio.ListaDeImagens),
                Valor = anuncio.Valor.ToString("C"),
                Titulo = anuncio.Titulo,
                Descricao = anuncio.Descricao,
                DataDeCriacao = anuncio.DataDeCriacao,
                Cidade = anuncio.Cidade.Nome + "-" + anuncio.Cidade.Estado.Sigla,
                OcultarTelefone = anuncio.OcultarTelefone,
                TipoDeOferta = anuncio.Imovel.TipoDeOferta.Descricao.ToUpper(),
                TipoDeImovel = anuncio.Imovel.TipoDoImovel.Nome,
                AreaUtil = anuncio.Imovel.AreaUtil?.ToString("F0"),
                Condominio = anuncio.Imovel.Condominio,
                Escriturado = anuncio.Imovel.Escriturado,
                Financiavel = anuncio.Imovel.Financiavel,
                AreaMurada = anuncio.Imovel.AreaMurada ? "Sim" : null,
                AcessoAsfaltado = anuncio.Imovel.AreaMurada ? "Sim" : null,
                Piscina = anuncio.Imovel.Piscina ? "Sim" : null,
                Churrasqueira = anuncio.Imovel.Churrasqueira ? "Sim" : null,
                Favorito = favoritado
            };

            //Anunciante
            detalhesDoAnuncioViewModel.Anunciante = new InformacoesDoAnuncianteViewModel
            {
                Id = anuncio.Anunciante.Id,
                Nome = anuncio.Anunciante.Nome,
                Telefone = anuncio.Anunciante.Telefone,
                Email = anuncio.Anunciante.Email
            };

            //Endereco
            detalhesDoAnuncioViewModel.Endereco = new InformacoesDoEnderecoViewModel
            {
                Cep = anuncio.Imovel.Endereco.Cep,
                Logradouro = anuncio.Imovel.Endereco.Logradouro,
                Bairro = anuncio.Imovel.Endereco.Bairro,
                Numero = anuncio.Imovel.Endereco.Numero
            };

            //Simular Financiamento
            detalhesDoAnuncioViewModel.SimularFinanciamento = new SimularFinanciamentoViewModel
            {
                ValorDoImovel = anuncio.Valor.ToString("F0"),
                Entrada = (anuncio.Valor * new decimal(0.25)).ToString("F0"),
                Prazo = 30,
                ValorASerFinanciado = (anuncio.Valor - anuncio.Valor * new decimal(0.25)).ToString("C0"),
                PrimeiraParcela = Imovel.CalcularPrimeiraParcela(anuncio.Valor - anuncio.Valor * new decimal(0.25)).ToString("C"),
                UltimaParcela = Imovel.CalcularUltimaParcela(anuncio.Valor - anuncio.Valor * new decimal(0.25)).ToString("C")
            };

            //Denuncia
            detalhesDoAnuncioViewModel.DenunciarAnuncio = new DenunciarAnuncioViewModel
            {
                AnuncioId = anuncio.Id,
                ListaDeMotivos = EnumHelper.CriarListaDeStringEnum<MotivoDaDenuncia>()
            };

            //Contatar Anunciante
            detalhesDoAnuncioViewModel.ContatarAnunciante = new ContatarAnuncianteViewModel
            {
                AnuncioId = anuncio.Id,
                Mensagem = "Olá, tenho interesse neste imóvel: " + anuncio.Titulo + ", " + anuncio.Valor.ToString("C0") + ", Aguardo o contato. Obrigado."
            };

            //DetalhesDoImovel
            if (anuncio.Imovel.DetalhesDoImovel != null)
            {
                var imovel = new DetalhesDoImovelViewModel
                {
                    NumeroDeQuartos = anuncio.Imovel.DetalhesDoImovel.NumeroDeQuartos,
                    NumeroDeBanheiros = anuncio.Imovel.DetalhesDoImovel.NumeroDeBanheiros,
                    NumeroDeSuites = anuncio.Imovel.DetalhesDoImovel.NumeroDeSuites,
                    VagasDeGaragem = anuncio.Imovel.DetalhesDoImovel.VagasDeGaragem,
                    Mobiliado = anuncio.Imovel.DetalhesDoImovel.Mobiliado ? "Sim" : null,
                    GaragemCoberta = anuncio.Imovel.DetalhesDoImovel.GaragemCoberta ? "Sim" : null,
                    Quintal = anuncio.Imovel.DetalhesDoImovel.Quintal ? "Sim" : null,
                    Jardim = anuncio.Imovel.DetalhesDoImovel.Jardim ? "Sim" : null
                };

                //Informações do Condominio
                if (anuncio.Imovel.Condominio)
                {
                    var informacoesDoCondominio = new InformacoesDoCondominioViewModel
                    {
                        CondominioNome = anuncio.Imovel.InformacoesDoCondominio.Nome,
                        CondominioValor = anuncio.Imovel.InformacoesDoCondominio.ValorDoCondominio,
                        Fechado = anuncio.Imovel.InformacoesDoCondominio.Fechado ? "Sim" : null,
                        SistemaDeVigilancia = anuncio.Imovel.InformacoesDoCondominio.SistemaDeVigilancia ? "Sim" : null,
                        PortaoEletronico = anuncio.Imovel.InformacoesDoCondominio.PortaoEletronico ? "Sim" : null,
                        PermiteAnimais = anuncio.Imovel.InformacoesDoCondominio.PermiteAnimais ? "Sim" : null,
                        Elevador = anuncio.Imovel.InformacoesDoCondominio.Elevador ? "Sim" : null
                    };

                    detalhesDoAnuncioViewModel.Condominio = true;
                    detalhesDoAnuncioViewModel.InformacoesDoCondominio = informacoesDoCondominio;
                }

                detalhesDoAnuncioViewModel.DetalhesDoImovel = imovel;
                return detalhesDoAnuncioViewModel;
            }

            //DetalhesDoTerreno
            else
            {
                var terreno = new DetalhesDoTerrenoViewModel
                {
                    Planado = anuncio.Imovel.DetalhesDoTerreno.Planado ? "Sim" : null,
                    Limpo = anuncio.Imovel.DetalhesDoTerreno.Limpo ? "Sim" : null,
                    AguaEncanada = anuncio.Imovel.DetalhesDoTerreno.AguaEncanada ? "Sim" : null,
                    PocoArtesiano = anuncio.Imovel.DetalhesDoTerreno.PocoArtesiano ? "Sim" : null,
                    EnergiaEletrica = anuncio.Imovel.DetalhesDoTerreno.EnergiaEletrica ? "Sim" : null,
                    AreaVerde = anuncio.Imovel.DetalhesDoTerreno.AreaVerde ? "Sim" : null,
                    CasaSede = anuncio.Imovel.DetalhesDoTerreno.CasaSede ? "Sim" : null,
                };

                detalhesDoAnuncioViewModel.DetalhesDoTerreno = terreno;
                return detalhesDoAnuncioViewModel;
            }
        }

        public static AnunciosFavoritos CriarFavorito(Guid id, string usuario)
        {
            return new AnunciosFavoritos(
                id,
                new Guid(usuario));
        }

        #endregion

        #region CriarNovoAnuncioResidencialComercial
        public static NovoAnuncioResidencialComercialViewModel CriarNovoAnuncioResidencialComercialViewModel(ICollection<Cidade> cidades, Entities.Anunciante anunciante)
        {
            var cidadeDictionary = new Dictionary<int, string>();
            var tipoDeImovel = new Dictionary<int, string>();

            //ToDo:Chamar no Banco os Tipos
            tipoDeImovel.Add(1, "Casa");
            tipoDeImovel.Add(2, "Apartamento");
            tipoDeImovel.Add(3, "Cobertura");
            tipoDeImovel.Add(4, "Flat");
            tipoDeImovel.Add(5, "Kitnet/Conjugado");
            tipoDeImovel.Add(6, "Sobrado");
            tipoDeImovel.Add(7, "Ponto comercial");
            tipoDeImovel.Add(8, "Loja");
            tipoDeImovel.Add(9, "Box");
            tipoDeImovel.Add(10, "Imóvel comercial");

            foreach (var item in cidades)
                cidadeDictionary.Add(item.Id, item.Nome);

            return new NovoAnuncioResidencialComercialViewModel
            {
                ListaDeCidades = cidadeDictionary,
                ListaTipoDeImovel = tipoDeImovel,
                Telefone = anunciante.Telefone
            };
        }

        public static ContatosRecebidos CriarContato(ContatarAnuncianteViewModel contatarAnuncianteViewModel)
        {
            return new ContatosRecebidos(
                0,
                contatarAnuncianteViewModel.AnuncioId,
                contatarAnuncianteViewModel.Nome,
                contatarAnuncianteViewModel.Telefone,
                contatarAnuncianteViewModel.ContatoViaWhatsApp,
                contatarAnuncianteViewModel.Email,
                contatarAnuncianteViewModel.Mensagem,
                false);
        }

        public static Entities.Anuncio CriarAnuncioResidencialComercial(NovoAnuncioResidencialComercialViewModel novoAnuncioResidencialComercialViewModel, TipoDeOferta tipoDeOferta, TipoDoImovel tipoDoImovel, ICollection<FotosDTO> fotos, string usuarioId)
        {
            return new Entities.Anuncio(
                Guid.Empty,
                CriarImovelResidencial(novoAnuncioResidencialComercialViewModel, tipoDeOferta, tipoDoImovel),
                decimal.Parse(novoAnuncioResidencialComercialViewModel.Valor),
                novoAnuncioResidencialComercialViewModel.Descricao,
                new Guid(usuarioId),
                1, //ToDo: Ajustar a Cidade Futuramente
                novoAnuncioResidencialComercialViewModel.OcultarTelefone,
                StatusDoAnuncio.Ativo.GetDescription(),
                CriarImagens(fotos),
                tipoDeOferta,
                tipoDoImovel);
        }

        private static Imovel CriarImovelResidencial(NovoAnuncioResidencialComercialViewModel novoAnuncioResidencialComercialViewModel, TipoDeOferta tipoDeOferta, TipoDoImovel tipoDoImovel)
        {
            return new Imovel(
                Guid.Empty,
                tipoDeOferta.Id,
                tipoDoImovel.Id,
                decimal.Parse(novoAnuncioResidencialComercialViewModel.AreaUtil),
                novoAnuncioResidencialComercialViewModel.Condominio,
                novoAnuncioResidencialComercialViewModel.Escriturado,
                novoAnuncioResidencialComercialViewModel.Financiavel,
                novoAnuncioResidencialComercialViewModel.AreaMurada,
                novoAnuncioResidencialComercialViewModel.AcessoAsfaltado,
                novoAnuncioResidencialComercialViewModel.Piscina,
                novoAnuncioResidencialComercialViewModel.Churrasqueira,
                CriarEnderecoAnuncioResidencial(novoAnuncioResidencialComercialViewModel),
                CriarInformacoesDoCondominioResidencial(novoAnuncioResidencialComercialViewModel),
                CriarDetalhesDoImovel(novoAnuncioResidencialComercialViewModel));
        }

        public static Endereco CriarEnderecoAnuncioResidencial(NovoAnuncioResidencialComercialViewModel novoAnuncioResidencialComercialViewModel)
        {
            return new Endereco(
                novoAnuncioResidencialComercialViewModel.Cep,
                novoAnuncioResidencialComercialViewModel.Bairro,
                novoAnuncioResidencialComercialViewModel.Logradouro,
                novoAnuncioResidencialComercialViewModel.Numero);
        }

        public static InformacoesDoCondominio CriarInformacoesDoCondominioResidencial(NovoAnuncioResidencialComercialViewModel novoAnuncioResidencialComercialViewModel)
        {
            if (novoAnuncioResidencialComercialViewModel.Condominio == true)
            {
                return new InformacoesDoCondominio(
                    novoAnuncioResidencialComercialViewModel.NomeDoCondominio,
                    decimal.Parse(novoAnuncioResidencialComercialViewModel.ValorDoCondominio),
                    novoAnuncioResidencialComercialViewModel.Fechado,
                    novoAnuncioResidencialComercialViewModel.SistemaDeVigilancia,
                    novoAnuncioResidencialComercialViewModel.PortaoEletronico,
                    novoAnuncioResidencialComercialViewModel.PermiteAnimais,
                    novoAnuncioResidencialComercialViewModel.Elevador);
            }
            else
            {
                return null;
            }
        }

        public static DetalhesDoImovel CriarDetalhesDoImovel(NovoAnuncioResidencialComercialViewModel novoAnuncioResidencialComercialViewModel)
        {
            return new DetalhesDoImovel(
                Guid.Empty,
                novoAnuncioResidencialComercialViewModel.NumeroDeQuartos,
                novoAnuncioResidencialComercialViewModel.NumeroDeBanheiros,
                novoAnuncioResidencialComercialViewModel.NumeroDeSuites,
                novoAnuncioResidencialComercialViewModel.VagasDeGaragem,
                novoAnuncioResidencialComercialViewModel.GaragemCoberta,
                novoAnuncioResidencialComercialViewModel.Mobiliado,
                novoAnuncioResidencialComercialViewModel.Quintal,
                novoAnuncioResidencialComercialViewModel.Jardim);
        }

        #endregion

        #region CriarNovoAnuncioLoteTerreno
        public static NovoAnuncioLoteTerrenoViewModel CriarNovoLoteTerrenoAnuncioViewModel(Entities.Anunciante anunciante)
        {
            var tipoDeImovel = new Dictionary<int, string>();

            //ToDo:Chamar no Banco os Tipos
            tipoDeImovel.Add(11, "Lote/Terreno");
            tipoDeImovel.Add(12, "Fazenda/Sítio");
            tipoDeImovel.Add(13, "Chácara");

            return new NovoAnuncioLoteTerrenoViewModel
            {
                ListaTipoDeImovel = tipoDeImovel,
                Telefone = anunciante.Telefone
            };
        }

        public static Entities.Anuncio CriarAnuncioLoteTerreno(NovoAnuncioLoteTerrenoViewModel novoAnuncioTerrenoLoteViewModel, TipoDeOferta tipoDeOferta, TipoDoImovel tipoDoImovel, ICollection<FotosDTO> fotos, string usuarioId)
        {
            return new Entities.Anuncio(
                Guid.Empty,
                CriarImovelTerreno(novoAnuncioTerrenoLoteViewModel, tipoDeOferta, tipoDoImovel),
                decimal.Parse(novoAnuncioTerrenoLoteViewModel.Valor),
                novoAnuncioTerrenoLoteViewModel.Descricao,
                new Guid(usuarioId),
                1, //ToDo: Ajustar a Cidade Futuramente
                novoAnuncioTerrenoLoteViewModel.OcultarTelefone,
                StatusDoAnuncio.Ativo.GetDescription(),
                CriarImagens(fotos),
                tipoDeOferta,
                tipoDoImovel);
        }

        public static ICollection<Imagens> CriarImagens(ICollection<FotosDTO> fotos)
        {
            var listaDeImagens = new List<Imagens>();

            //ToDo: Trabalhar na Largura e Altura da Imagem
            foreach (var foto in fotos)
            {
                listaDeImagens.Add(new Imagens(foto.Nome, foto.URL, foto.Formato, foto.Tamanho));
            }

            return listaDeImagens;
        }

        private static Imovel CriarImovelTerreno(NovoAnuncioLoteTerrenoViewModel novoAnuncioTerrenoLoteViewModel, TipoDeOferta tipoDeOferta, TipoDoImovel tipoDoImovel)
        {
            return new Imovel(
                Guid.Empty,
                tipoDeOferta.Id,
                tipoDoImovel.Id,
                decimal.Parse(novoAnuncioTerrenoLoteViewModel.AreaUtil),
                novoAnuncioTerrenoLoteViewModel.Condominio,
                novoAnuncioTerrenoLoteViewModel.Escriturado,
                novoAnuncioTerrenoLoteViewModel.Financiavel,
                novoAnuncioTerrenoLoteViewModel.AreaMurada,
                novoAnuncioTerrenoLoteViewModel.AcessoAsfaltado,
                novoAnuncioTerrenoLoteViewModel.Piscina,
                novoAnuncioTerrenoLoteViewModel.Churrasqueira,
                CriarEnderecoAnuncioLoteTerreno(novoAnuncioTerrenoLoteViewModel),
                CriarInformacoesDoCondominioLoteTerreno(novoAnuncioTerrenoLoteViewModel),
                CriarDetalhesDoLoteTerreno(novoAnuncioTerrenoLoteViewModel));
        }

        public static Endereco CriarEnderecoAnuncioLoteTerreno(NovoAnuncioLoteTerrenoViewModel novoAnuncioTerrenoLoteViewModel)
        {
            return new Endereco(
                novoAnuncioTerrenoLoteViewModel.Cep,
                novoAnuncioTerrenoLoteViewModel.Bairro,
                novoAnuncioTerrenoLoteViewModel.Logradouro,
                novoAnuncioTerrenoLoteViewModel.Numero);
        }

        public static InformacoesDoCondominio CriarInformacoesDoCondominioLoteTerreno(NovoAnuncioLoteTerrenoViewModel novoAnuncioTerrenoLoteViewModel)
        {
            if (novoAnuncioTerrenoLoteViewModel.Condominio == true)
            {
                return new InformacoesDoCondominio(
                    novoAnuncioTerrenoLoteViewModel.NomeDoCondominio,
                    decimal.Parse(novoAnuncioTerrenoLoteViewModel.ValorDoCondominio),
                    novoAnuncioTerrenoLoteViewModel.Fechado,
                    novoAnuncioTerrenoLoteViewModel.SistemaDeVigilancia,
                    novoAnuncioTerrenoLoteViewModel.PortaoEletronico,
                    novoAnuncioTerrenoLoteViewModel.PermiteAnimais,
                    novoAnuncioTerrenoLoteViewModel.Elevador);
            }
            else
            {
                return null;
            }
        }

        public static DetalhesDoTerreno CriarDetalhesDoLoteTerreno(NovoAnuncioLoteTerrenoViewModel novoAnuncioTerrenoLoteViewModel)
        {
            return new DetalhesDoTerreno(
                Guid.Empty,
                novoAnuncioTerrenoLoteViewModel.Planado,
                novoAnuncioTerrenoLoteViewModel.Limpo,
                novoAnuncioTerrenoLoteViewModel.AguaEncanada,
                novoAnuncioTerrenoLoteViewModel.PocoArtesiano,
                novoAnuncioTerrenoLoteViewModel.EnergiaEletrica,
                novoAnuncioTerrenoLoteViewModel.AreaVerde,
                novoAnuncioTerrenoLoteViewModel.CasaSede
            );
        }

        #endregion

        #region Listagem

        public static ListaDeAnunciosViewModel CriarListaDeAnunciosViewModel(ICollection<Entities.Anuncio> anuncios, ICollection<TipoDeOferta> categoria, ICollection<TipoDoImovel> tipoDoImovel)
        {
            return new ListaDeAnunciosViewModel
            {
                Anuncios = CriarListaAnuncioComTituloViewModel(anuncios)
            };
        }

        private static ICollection<AnuncioComTituloViewModel> CriarListaAnuncioComTituloViewModel(ICollection<Entities.Anuncio> anuncios)
        {
            var anuncioComTituloViewModelList = new List<AnuncioComTituloViewModel>();

            foreach (var anuncio in anuncios)
            {
                if (anuncio.Imovel.DetalhesDoImovel != null)
                {
                    anuncioComTituloViewModelList.Add(new AnuncioComTituloViewModel
                    {
                        Id = anuncio.Id,
                        UrlImagem = CriarListaDeUrlImagem(anuncio.ListaDeImagens),
                        Titulo = anuncio.Titulo,
                        Valor = anuncio.Valor,
                        MetrosQuadrados = anuncio.Imovel.AreaUtil.Value.ToString("F0"),
                        Descricao = anuncio.Descricao.Substring(0, anuncio.Descricao.Length > 82 ? 84 : anuncio.Descricao.Length) + (anuncio.Descricao.Length > 82 ? "..." : ""),
                        Cidade = anuncio.Cidade.Nome + "-" + anuncio.Cidade.Estado.Sigla,
                        Bairro = anuncio.Imovel.Endereco.Bairro,
                        Imovel = CriarImovelViewModel(anuncio.Imovel)
                    });
                }
                else
                {
                    anuncioComTituloViewModelList.Add(new AnuncioComTituloViewModel
                    {
                        Id = anuncio.Id,
                        UrlImagem = CriarListaDeUrlImagem(anuncio.ListaDeImagens),
                        Titulo = anuncio.Titulo,
                        Valor = anuncio.Valor,
                        MetrosQuadrados = anuncio.Imovel.AreaUtil.Value.ToString("F0"),
                        Descricao = anuncio.Descricao.Substring(0, anuncio.Descricao.Length > 82 ? 84 : anuncio.Descricao.Length) + (anuncio.Descricao.Length > 82 ? "..." : ""),
                        Cidade = anuncio.Cidade.Nome + "-" + anuncio.Cidade.Estado.Sigla,
                        Bairro = anuncio.Imovel.Endereco.Bairro
                    });
                }
            }
            return anuncioComTituloViewModelList;
        }

        public static ImovelListagemViewModel CriarImovelViewModel(Imovel imovel)
        {
            if (imovel.Condominio)
            {
                return new ImovelListagemViewModel
                {
                    Quartos = imovel.DetalhesDoImovel.NumeroDeQuartos,
                    Banheiros = imovel.DetalhesDoImovel.NumeroDeBanheiros,
                    Vagas = imovel.DetalhesDoImovel.VagasDeGaragem,
                    ValorDoCondominio = imovel.InformacoesDoCondominio.ValorDoCondominio.ToString("C0")
                };
            }
            else
            {
                return new ImovelListagemViewModel
                {
                    Quartos = imovel.DetalhesDoImovel.NumeroDeQuartos,
                    Banheiros = imovel.DetalhesDoImovel.NumeroDeBanheiros,
                    Vagas = imovel.DetalhesDoImovel.VagasDeGaragem,
                    ValorDoCondominio = null
                };
            }
        }

        public static ICollection<MeusAnunciosViewModel> CriarMeusAnunciosViewModel(ICollection<Entities.Anuncio> anuncios)
        {
            var meusAnunciosList = new List<MeusAnunciosViewModel>();

            foreach (var item in anuncios)
            {
                meusAnunciosList.Add(new MeusAnunciosViewModel
                {
                    Id = item.Id,
                    Titulo = item.Titulo,
                    Descricao = item.Descricao.Substring(0, item.Descricao.Length > 82 ? 84 : item.Descricao.Length) + (item.Descricao.Length > 82 ? "..." : ""),
                    Valor = item.Valor.ToString("C0"),
                    Bairro = item.Imovel.Endereco.Bairro,
                    Cidade = item.Cidade.Nome + "-" + item.Cidade.Estado.Sigla,
                    UrlImagem = GerarUrlImagem(item.ListaDeImagens.FirstOrDefault()),
                    Visualizacoes = item.ListaDeVisualizacoes.Count()
                });
            }

            return meusAnunciosList;
        }

        private static string GerarUrlImagem(Imagens imagem)
        {
            if (imagem != null)
            {
                return imagem.Url + imagem.Nome + imagem.Formato;
            }
            else
            {
                return null;
            }
        }

        private static ICollection<string> CriarListaDeUrlImagem(ICollection<Imagens> listaDeImagens)
        {
            var urlImagemList = new List<string>();

            foreach (var imagem in listaDeImagens)
            {
                urlImagemList.Add(imagem.Url + imagem.Nome + imagem.Formato);
            }

            return urlImagemList;
        }

        #endregion

        public static BuscarAnuncioViewModel CriarFormBuscarAnuncioViewModel(ICollection<TipoDoImovel> tipoDoImovel)
        {
            var listaTipoDeImovel = new Dictionary<int, string>();

            foreach (var tipo in tipoDoImovel)
                listaTipoDeImovel.Add(tipo.Id, tipo.Nome);

            return new BuscarAnuncioViewModel
            {
                ListaTipoDeImovel = listaTipoDeImovel
            };
        }

        public static Visualizacao CriarVisualizacao(Guid id)
        {
            return new Visualizacao(id);
        }

        public static Buscas CriarBusca(string parametro)
        {
            return new Buscas(parametro);
        }
    }
}