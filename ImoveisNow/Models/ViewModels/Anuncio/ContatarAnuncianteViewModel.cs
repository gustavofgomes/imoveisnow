﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ImoveisNow.Models.ViewModels.Anuncio
{
    public class ContatarAnuncianteViewModel
    {
        public Guid AnuncioId { get; set; }
        [Display(Name = "INFORME SEU NOME")]
        [Required(ErrorMessage = "Informe o Seu Nome para continuar.")]
        public string Nome { get; set; }
        [Display(Name = "TELEFONE")]
        [Required(ErrorMessage = "Informe o Seu Telefone para continuar.")]
        public string Telefone { get; set; }
        [Display(Name = "DESEJA RECEBER CONTATO VIA WHATSAPP?")]
        public bool ContatoViaWhatsApp { get; set; }
        [Display(Name = "EMAIL")]
        public string Email { get; set; }
        [Display(Name = "MENSAGEM")]
        [Required(ErrorMessage = "Informe a Mensagem a ser enviada para continuar.")]
        public string Mensagem { get; set; }
    }
}