﻿using System;

namespace ImoveisNow.Models.ViewModels.Anuncio
{
    public class MeusAnunciosViewModel
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Valor { get; set; }
        public string Cidade { get; set; }
        public string Bairro { get; set; }
        public int Visualizacoes { get; set; }
        public string UrlImagem { get; set; }
    }
}