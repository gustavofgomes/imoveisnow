﻿namespace ImoveisNow.Models.ViewModels.Anuncio
{
    public class DetalhesDoImovelViewModel
    {
        public int NumeroDeQuartos { get; set; }
        public int NumeroDeBanheiros { get; set; }
        public int NumeroDeSuites { get; set; }
        public int VagasDeGaragem { get; set; }
        public string GaragemCoberta { get; set; }
        public string Mobiliado { get; set; }
        public string Quintal { get; set; }
        public string Jardim { get; set; }
    }
}