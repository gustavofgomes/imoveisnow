﻿using System.Collections.Generic;

namespace ImoveisNow.Models.ViewModels.Home
{
    public class InicioViewModel
    {
        public ICollection<AnuncioSemTituloViewModel> AnunciosEmDestaque { get; set; }
        public ICollection<AnuncioSemTituloViewModel> AnunciosNovos { get; set; }
    }
}