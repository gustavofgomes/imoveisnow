﻿using System;

namespace ImoveisNow.Models.ViewModels.Home
{
    public class AnuncioSemTituloViewModel
    {
        public Guid Id { get; set; }
        public string UrlImagem { get; set; }
        public string Valor { get; set; }
        public string MetrosQuadrados { get; set; }
        public string Descricao { get; set; }
        public string Cidade { get; set; }
        public string Bairro { get; set; }
        public DetalhesDoImovelViewModel Imovel {get; set;}
    }
}