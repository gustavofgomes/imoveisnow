﻿namespace ImoveisNow.Models.ViewModels.Home
{
    public class DetalhesDoImovelViewModel
    {
        public int Quartos { get; set; }
        public int Banheiros { get; set; }
        public int Vagas { get; set; }
        public string ValorDoCondominio { get; set; }
    }
}