﻿using ImoveisNow.Models.Entities;
using System.Collections.Generic;
using System.Linq;

namespace ImoveisNow.Models.ViewModels.Home
{
    public class HomeFactory
    {
        public static InicioViewModel CriarInicioViewModel(ICollection<Entities.Anuncio> tresAnunciosEmDestaque, ICollection<Entities.Anuncio> tresUltimosAnuncios)
        {
            return new InicioViewModel()
            {
                AnunciosEmDestaque = CriarListaAnuncioSemTituloViewModel(tresAnunciosEmDestaque),
                AnunciosNovos = CriarListaAnuncioSemTituloViewModel(tresUltimosAnuncios)
            };
        }

        public static List<AnuncioSemTituloViewModel> CriarListaAnuncioSemTituloViewModel(ICollection<Entities.Anuncio> anuncios)
        {
            var anuncioSemTituloViewModelList = new List<AnuncioSemTituloViewModel>();

            foreach (var anuncio in anuncios)
            {
                var imagem = anuncio.ListaDeImagens.FirstOrDefault();
                if (anuncio.Imovel.DetalhesDoImovel != null)
                {
                    anuncioSemTituloViewModelList.Add(new AnuncioSemTituloViewModel
                    {
                        Id = anuncio.Id,
                        UrlImagem = imagem.Url + imagem.Nome + imagem.Formato,
                        Valor = anuncio.Valor.ToString("C0"),
                        MetrosQuadrados = anuncio.Imovel.AreaUtil.Value.ToString("F0"),
                        Descricao = anuncio.Descricao.Substring(0, anuncio.Descricao.Length > 82 ? 84 : anuncio.Descricao.Length) + (anuncio.Descricao.Length > 82 ? "..." : ""),
                        Cidade = anuncio.Cidade.Nome + "-" + anuncio.Cidade.Estado.Sigla,
                        Bairro = anuncio.Imovel.Endereco.Bairro,
                        Imovel = CriarImovelViewModel(anuncio.Imovel)
                    });
                }
                else
                {
                    anuncioSemTituloViewModelList.Add(new AnuncioSemTituloViewModel
                    {
                        Id = anuncio.Id,
                        UrlImagem = GerarUrlImagem(imagem),
                        Valor = anuncio.Valor.ToString("C0"),
                        MetrosQuadrados = anuncio.Imovel.AreaUtil.Value.ToString("F0"),
                        Descricao = anuncio.Descricao.Substring(0, anuncio.Descricao.Length > 82 ? 84 : anuncio.Descricao.Length) + (anuncio.Descricao.Length > 82 ? "..." : ""),
                        Cidade = anuncio.Cidade.Nome + "-" + anuncio.Cidade.Estado.Sigla,
                        Bairro = anuncio.Imovel.Endereco.Bairro,
                        Imovel = null
                    });
                }
            }
            return anuncioSemTituloViewModelList;
        }

        private static string GerarUrlImagem(Imagens imagem)
        {
            if (imagem != null)
            {
                return imagem.Url + imagem.Nome + imagem.Formato;
            }
            else
            {
                return null;
            }
        }

        public static DetalhesDoImovelViewModel CriarImovelViewModel(Imovel imovel)
        {
            if (imovel.Condominio)
            {
                return new DetalhesDoImovelViewModel
                {
                    Quartos = imovel.DetalhesDoImovel.NumeroDeQuartos,
                    Banheiros = imovel.DetalhesDoImovel.NumeroDeBanheiros,
                    Vagas = imovel.DetalhesDoImovel.VagasDeGaragem,
                    ValorDoCondominio = imovel.InformacoesDoCondominio.ValorDoCondominio.ToString("C0")
                };
            }
            else
            {
                return new DetalhesDoImovelViewModel
                {
                    Quartos = imovel.DetalhesDoImovel.NumeroDeQuartos,
                    Banheiros = imovel.DetalhesDoImovel.NumeroDeBanheiros,
                    Vagas = imovel.DetalhesDoImovel.VagasDeGaragem,
                    ValorDoCondominio = null
                };
            }
        }
    }
}