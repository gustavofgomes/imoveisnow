﻿using ImoveisNow.Models.Entities;
using ImoveisNow.Models.Entities.Enum;
using ImoveisNow.Models.Helpers;
using System;
using System.Collections.Generic;

namespace ImoveisNow.Models.ViewModels.Anunciante
{
    public static class AnuncianteFactory
    {
        public static Entities.Anunciante CriarAnunciante(CadastroAnuncianteViewModel cadastroAnuncianteViewModel, Guid aspNetUserId)
        {
            var ativo = true;
            var tipoDeAnunciante = TipoDeAnunciante.Proprietario.GetDescription();

            return new Entities.Anunciante(
                aspNetUserId, cadastroAnuncianteViewModel.Nome, cadastroAnuncianteViewModel.Email,
                cadastroAnuncianteViewModel.Telefone, cadastroAnuncianteViewModel.CidadeId, cadastroAnuncianteViewModel.CRECI,
                DateTime.Now.Date, tipoDeAnunciante, ativo);
        }
        public static CadastroAnuncianteViewModel CriarCadastroAnuncianteViewModel(ICollection<Cidade> cidades)
        {
            var cidadeDictionary = new Dictionary<int, string>();

            foreach (var item in cidades)
                cidadeDictionary.Add(item.Id, item.Nome + " - " +item.Estado.Sigla);

            return new CadastroAnuncianteViewModel
            {
                ListaDeCidades = cidadeDictionary
            };
        }
    }
}