﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ImoveisNow.Models.ViewModels.Anunciante
{
    public class CadastroAnuncianteViewModel
    {
        [MaxLength(50, ErrorMessage = "Por favor, abrevie o seu nome.")]
        [Required(ErrorMessage = "Informe o seu {0} para continuar.")]
        public string Nome { get; set; }

        [MaxLength(80, ErrorMessage = "Tamanho máximo estrapolado.")]
        [Required(ErrorMessage = "Informe o seu {0} para continuar.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Informe o seu {0} para continuar.")]
        [StringLength(16, MinimumLength = 14, ErrorMessage = "Telefone inválido.")]
        public string Telefone { get; set; }

        [Display(Name = "Cidade")]
        [Required(ErrorMessage = "Informe a sua {0} para continuar.")]
        public int CidadeId { get; set; }

        [MaxLength(10, ErrorMessage = "CRECI Inválido.")]
        [Required(ErrorMessage = "Informe o seu {0} para continuar.")]
        public string CRECI { get; set; }

        [Required(ErrorMessage = "Informe a sua {0} para continuar.")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Mínimo 6 caracteres.")]
        public string Senha { get; set; }

        [Display(Name = "Confirmação de senha")]
        [Compare("Senha", ErrorMessage = "Confirmação de senha inválida.")]
        [Required(ErrorMessage = "Informe a sua {0} para continuar.")]
        public string ConfirmacaoSenha { get; set; }

        public Dictionary<int, string> ListaDeCidades { get; set; }
    }
}