﻿using ImoveisNow.Models.ViewModels.Anunciante;

namespace ImoveisNow.Models.ViewModels.Autenticacao
{
    public static class UsuarioIdentityFactory
    {
        public static Identity.Entities.UsuarioIdentity CriarUsuarioIdentityProprietario(CadastroAnuncianteViewModel cadastroAnuncianteViewModel)
        {
            return new Identity.Entities.UsuarioIdentity()
            {
                UserName = cadastroAnuncianteViewModel.Email,
                Email = cadastroAnuncianteViewModel.Email
            };
        }
    }
}