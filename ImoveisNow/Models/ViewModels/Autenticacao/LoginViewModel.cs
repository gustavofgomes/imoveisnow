﻿using System.ComponentModel.DataAnnotations;

namespace ImoveisNow.Models.ViewModels.Autenticacao
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Informe o seu {0} para continuar.")]
        [EmailAddress(ErrorMessage = "E-mail inválido.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Informe a sua {0} para continuar.")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Senha inválida.")]
        public string Senha { get; set; }
    }
}
