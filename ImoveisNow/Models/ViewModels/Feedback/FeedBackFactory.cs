﻿using System;

namespace ImoveisNow.Models.ViewModels.Feedback
{
    public static class FeedBackFactory
    {
        public static LerFeedBackViewModel CriarLerFeedBackViewModel(Entities.Feedback feedback)
        {
            return new LerFeedBackViewModel
            {
                Id = feedback.Id,
                Email = feedback.Email,
                Texto = feedback.Texto,
                ReceberReposta = feedback.ReceberResposta ? "Sim" : null,
                DataEHora = feedback.DataEHora
            };
        }

        public static Entities.Feedback CriarFeedBack(CriarFeedBackViewModel criarFeedBackViewModel)
        {
            return new Entities.Feedback(
                0,
                criarFeedBackViewModel.Email,
                criarFeedBackViewModel.Texto,
                criarFeedBackViewModel.ReceberReposta);
        }
    }
}