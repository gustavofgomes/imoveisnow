﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ImoveisNow.Models.ViewModels.Feedback
{
    public class CriarFeedBackViewModel
    {
        [Required(ErrorMessage = "Informe o Email para continuar.")]
        [DisplayName("EMAIL")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Informe o Texto para continuar.")]
        [DisplayName("TEXTO")]
        public string Texto { get; set; }
        [DisplayName("DESEJA RECEBER UM RETORNO?")]
        public bool ReceberReposta { get; set; }
    }
}