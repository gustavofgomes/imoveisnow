﻿using System;

namespace ImoveisNow.Models.ViewModels.Feedback
{
    public class LerFeedBackViewModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Texto { get; set; }
        public string ReceberReposta { get; set; }
        public DateTime DataEHora { get; set; }
    }
}