﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ImoveisNow.Models.Entities;
using ImoveisNow.Models.Mapping;
using ImoveisNow.Models.Repository.Seed;

namespace ImoveisNow.Models.Context
{
    public class INContext : DbContext
    {
        private readonly IConfiguration configuration;

        public INContext(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public DbSet<Anunciante> Anunciante { get; set; }
        public DbSet<Anuncio> Anuncio { get; set; }
        public DbSet<AnunciosFavoritos> AnunciosFavoritos { get; set; }
        public DbSet<Buscas> Busca { get; set; }
        public DbSet<Cidade> Cidade { get; set; }
        public DbSet<Compra> Compra { get; set; }
        public DbSet<ContatosRecebidos> ContatosRecebidos { get; set; }
        public DbSet<Denuncia> Denuncia { get; set; }
        public DbSet<DetalhesDoImovel> DetalhesDoImovel { get; set; }
        public DbSet<DetalhesDoTerreno> DetalhesDoTerreno { get; set; }
        public DbSet<Endereco> Endereco { get; set; }
        public DbSet<Estado> Estado { get; set; }
        public DbSet<Feedback> Feedback { get; set; }
        public DbSet<Imagens> Imagens { get; set; }
        public DbSet<Imovel> Imovel { get; set; }
        public DbSet<InformacoesDoCondominio> InformacoesDoCondominio { get; set; }
        public DbSet<PlanoDoAnunciante> PlanoDoAnunciante { get; set; }
        public DbSet<TipoDeOferta> TipoDeOferta { get; set; }
        public DbSet<TipoDePlano> TipoDePlano { get; set; }
        public DbSet<TipoDoImovel> TipoDoImovel { get; set; }
        public DbSet<Visualizacao> Visualizacao { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new AnuncianteMap());
            modelBuilder.ApplyConfiguration(new AnuncioMap());
            modelBuilder.ApplyConfiguration(new AnunciosFavoritosMap());
            modelBuilder.ApplyConfiguration(new BuscasMap());
            modelBuilder.ApplyConfiguration(new CidadeMap());
            modelBuilder.ApplyConfiguration(new CompraMap());
            modelBuilder.ApplyConfiguration(new ContatosRecebidosMap());
            modelBuilder.ApplyConfiguration(new DenunciaMap());
            modelBuilder.ApplyConfiguration(new DetalhesDoImovelMap());
            modelBuilder.ApplyConfiguration(new DetalhesDoTerrenoMap());
            modelBuilder.ApplyConfiguration(new EnderecoMap());
            modelBuilder.ApplyConfiguration(new EstadoMap());
            modelBuilder.ApplyConfiguration(new FeedbackMap());
            modelBuilder.ApplyConfiguration(new ImagensMap());
            modelBuilder.ApplyConfiguration(new ImovelMap());
            modelBuilder.ApplyConfiguration(new InformacoesDoCondominioMap());
            modelBuilder.ApplyConfiguration(new PlanoDoAnuncianteMap());
            modelBuilder.ApplyConfiguration(new TipoDeOfertaMap());
            modelBuilder.ApplyConfiguration(new TipoDePlanoMap());
            modelBuilder.ApplyConfiguration(new TipoDoImovelMap());
            modelBuilder.ApplyConfiguration(new VisualizacoesMap());

            modelBuilder.PopularEstados();
            modelBuilder.PopularCidades();
            modelBuilder.PopularTipoDePlanos();
            modelBuilder.PopularPlanoAtivos();
            modelBuilder.PopularAnunciantes();
            modelBuilder.PopularTipoDeOfertas();
            modelBuilder.PopularTipoDeImovel();
            modelBuilder.PopularAnuncios();
            modelBuilder.PopularContatosRecebidos();
            modelBuilder.PopularImagens();
            modelBuilder.PopularAnunciosFavoritos();
            modelBuilder.PopularDenuncias();
            modelBuilder.PopularImoveis();
            modelBuilder.PopularEndereco();
            modelBuilder.PopularInformacoesDosCondominios();
            modelBuilder.PopularDetalhesDosTerrenos();
            modelBuilder.PopularDetalhesDosImoveis();
            modelBuilder.PopularFeedbacks();
            modelBuilder.PopularBuscas();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("INContext"));
        }
    }
}