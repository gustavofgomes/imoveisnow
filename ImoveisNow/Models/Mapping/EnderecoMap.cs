﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImoveisNow.Models.Mapping
{
    public class EnderecoMap : IEntityTypeConfiguration<Endereco>
    {
        public void Configure(EntityTypeBuilder<Endereco> builder)
        {
            //Table Name
            builder.ToTable("Endereco");

            //Primary Key
            builder.HasKey(x => x.Id);

            //Properties
            builder.Property(x => x.Cep)
                .HasColumnType("varchar(10)")
                .HasMaxLength(10);

            builder.Property(x => x.Bairro)
                .HasColumnType("varchar(50)")
                .HasMaxLength(50);

            builder.Property(x => x.Logradouro)
                .HasColumnType("varchar(50)")
                .HasMaxLength(50);

            builder.Property(x => x.Numero)
                .HasColumnType("varchar(18)")
                .HasMaxLength(18);
        }
    }
}