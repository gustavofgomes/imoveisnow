﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImoveisNow.Models.Mapping
{
    public class TipoDePlanoMap : IEntityTypeConfiguration<TipoDePlano>
    {
        public void Configure(EntityTypeBuilder<TipoDePlano> builder)
        {
            //Table Name
            builder.ToTable("TipoDePlano");

            //Primary Key
            builder.HasKey(x => x.Id);

            //Properties
            builder.Property(x => x.Nome)
                .HasColumnType("varchar(50)")
                .HasMaxLength(50)
                .IsRequired();

            builder.Property(x => x.DiasDeDuracao)
                .IsRequired();

            builder.Property(x => x.LimiteDeAnuncios)
                .IsRequired();
        }
    }
}