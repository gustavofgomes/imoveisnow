﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImoveisNow.Models.Mapping
{
    public class ImovelMap : IEntityTypeConfiguration<Imovel>
    {
        public void Configure(EntityTypeBuilder<Imovel> builder)
        {
            //Table Name
            builder.ToTable("Imovel");

            //Primary Key
            builder.HasKey(x => x.Id);

            builder.Property(x => x.TipoDeOfertaId)
                .IsRequired();

            builder.Property(x => x.TipoDeImovelId)
                .IsRequired();

            builder.Property(x => x.AreaUtil)
                .IsRequired();

            //Relationships
            builder.HasOne(x => x.TipoDeOferta).WithMany(x => x.ListaDeImoveis)
                .HasForeignKey(y => y.TipoDeOfertaId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.TipoDoImovel).WithMany(x => x.ListaDeImoveis)
                .HasForeignKey(y => y.TipoDeImovelId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.Endereco).WithOne(x => x.Imovel)
                .HasPrincipalKey<Imovel>();

            builder.HasOne(x => x.InformacoesDoCondominio).WithOne(x => x.Imovel)
                .HasPrincipalKey<Imovel>();

            builder.HasOne(x => x.DetalhesDoImovel).WithOne( x => x.Imovel)
                .HasPrincipalKey<Imovel>();

            builder.HasOne(x => x.DetalhesDoTerreno).WithOne(x => x.Imovel)
                .HasPrincipalKey<Imovel>();
        }
    }
}