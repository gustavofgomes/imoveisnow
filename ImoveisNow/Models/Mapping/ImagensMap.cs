﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImoveisNow.Models.Mapping
{
    public class ImagensMap : IEntityTypeConfiguration<Imagens>
    {
        public void Configure(EntityTypeBuilder<Imagens> builder)
        {
            //Table Name
            builder.ToTable("Imagens");

            //Primary Key
            builder.HasKey(x => x.Id);

            //Properties
            builder.Property(x => x.AnuncioId)
                .IsRequired();

            builder.Property(x => x.Nome)
                .HasColumnType("varchar(36)")
                .HasMaxLength(36)
                .IsRequired();

            builder.Property(x => x.Url)
                .HasColumnType("varchar(200)")
                .HasMaxLength(24)
                .IsRequired();

            builder.Property(x => x.Formato)
                .HasColumnType("varchar(8)")
                .HasMaxLength(8)
                .IsRequired();

            builder.Property(x => x.Tamanho)
                .HasColumnType("varchar(24)")
                .HasMaxLength(24)
                .IsRequired();

            //Relationships
            builder.HasOne(x => x.Anuncio).WithMany(x => x.ListaDeImagens)
                .HasForeignKey(y => y.AnuncioId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}