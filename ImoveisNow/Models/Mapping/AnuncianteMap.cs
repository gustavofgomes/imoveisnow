﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImoveisNow.Models.Mapping
{
    public class AnuncianteMap : IEntityTypeConfiguration<Anunciante>
    {
        public void Configure(EntityTypeBuilder<Anunciante> builder)
        {
            //Table Name
            builder.ToTable("Anunciante");

            //Primary Key
            builder.HasKey(x => x.Id);

            //Properties
            builder.Property(x => x.Nome)
                .HasColumnType("varchar(50)")
                .HasMaxLength(50)
                .IsRequired();

            builder.Property(x => x.Email)
                .HasColumnType("varchar(80)")
                .HasMaxLength(80)
                .IsRequired();

            builder.Property(x => x.Telefone)
                .HasColumnType("varchar(20)")
                .HasMaxLength(20);

            builder.Property(x => x.CidadeId)
                .IsRequired();

            builder.Property(x => x.DataDeCadastro)
                .IsRequired();

            builder.Property(x => x.UltimoAcesso)
                .IsRequired();

            builder.Property(x => x.TipoDeAnunciante)
                .HasColumnType("varchar(20)")
                .HasMaxLength(20)
                .IsRequired();

            builder.Property(x => x.Ativo)
                .IsRequired();

            //Relationships
            builder.HasOne(x => x.Cidade).WithMany(x => x.ListaDeAnunciantes)
                .HasForeignKey(y => y.CidadeId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}