﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImoveisNow.Models.Mapping
{
    public class DetalhesDoImovelMap : IEntityTypeConfiguration<DetalhesDoImovel>
    {
        public void Configure(EntityTypeBuilder<DetalhesDoImovel> builder)
        {
            //Table Name
            builder.ToTable("DetalhesDoImovel");

            //Primary Key
            builder.HasKey(x => x.Id);
        }
    }
}