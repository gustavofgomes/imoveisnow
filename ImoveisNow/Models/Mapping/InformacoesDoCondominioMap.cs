﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImoveisNow.Models.Mapping
{
    public class InformacoesDoCondominioMap : IEntityTypeConfiguration<InformacoesDoCondominio>
    {
        public void Configure(EntityTypeBuilder<InformacoesDoCondominio> builder)
        {
            //Table Name
            builder.ToTable("InformacoesDoCondominio");

            //Primary Key
            builder.HasKey(x => x.Id);
        }
    }
}