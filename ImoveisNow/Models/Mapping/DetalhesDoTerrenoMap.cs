﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImoveisNow.Models.Mapping
{
    public class DetalhesDoTerrenoMap : IEntityTypeConfiguration<Entities.DetalhesDoTerreno>
    {
        public void Configure(EntityTypeBuilder<Entities.DetalhesDoTerreno> builder)
        {
            //Table Name
            builder.ToTable("DetalhesDoTerreno");

            //Primary Key
            builder.HasKey(x => x.Id);
        }
    }
}