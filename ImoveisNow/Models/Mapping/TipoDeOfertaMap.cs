﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImoveisNow.Models.Mapping
{
    public class TipoDeOfertaMap : IEntityTypeConfiguration<TipoDeOferta>
    {
        public void Configure(EntityTypeBuilder<TipoDeOferta> builder)
        {
            //Table Name
            builder.ToTable("TipoDeOferta");

            //Primary Key
            builder.HasKey(x => x.Id);

            //Properties
            builder.Property(x => x.Nome)
                .HasColumnType("varchar(50)")
                .HasMaxLength(36)
                .IsRequired();

            builder.Property(x => x.Descricao)
                .HasColumnType("varchar(50)")
                .HasMaxLength(36)
                .IsRequired();
        }
    }
}