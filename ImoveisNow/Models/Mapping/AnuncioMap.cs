﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ImoveisNow.Models.Entities;

namespace ImoveisNow.Models.Mapping
{
    public class AnuncioMap: IEntityTypeConfiguration<Anuncio>
    {
        public void Configure(EntityTypeBuilder<Anuncio> builder)
        {
            //Table Name
            builder.ToTable("Anuncio");

            //Primary Key
            builder.HasKey(x => x.Id);

            //Properties
            builder.Property(x => x.Valor)
                .IsRequired();

            builder.Property(x => x.Titulo)
                .HasColumnType("varchar(100)")
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(x => x.Descricao)
                .HasColumnType("varchar(2000)")
                .HasMaxLength(2000)
                .IsRequired();

            builder.Property(x => x.DataDeCriacao)
                .IsRequired();

            builder.Property(x => x.AnuncianteId)
                .IsRequired();

            builder.Property(x => x.CidadeId)
                .IsRequired();

            builder.Property(x => x.OcultarTelefone)
                .IsRequired();

            builder.Property(x => x.Status)
                .IsRequired();

            builder.Property(x => x.Peso)
                .IsRequired();

            //Relationships
            builder.HasOne(x => x.Anunciante).WithMany(x => x.ListaDeAnuncios)
                .HasForeignKey(y => y.AnuncianteId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.Cidade).WithMany(x => x.ListaDeAnuncios)
                .HasForeignKey(y => y.CidadeId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.Imovel).WithOne(x => x.Anuncio)
                .HasPrincipalKey<Anuncio>();
        }
    }
}