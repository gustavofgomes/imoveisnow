﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImoveisNow.Models.Mapping
{
    public class VisualizacoesMap : IEntityTypeConfiguration<Visualizacao>
    {
        public void Configure(EntityTypeBuilder<Visualizacao> builder)
        {
            //Table Name
            builder.ToTable("Visualizacoes");

            //Primary Key
            builder.HasKey(x => x.Id);

            //Properties
            builder.Property(x => x.AnuncioId)
                .IsRequired();

            builder.Property(x => x.DataEHora)
                .IsRequired();

            //Relationships
            builder.HasOne(x => x.Anuncio).WithMany(x => x.ListaDeVisualizacoes)
                .HasForeignKey(y => y.AnuncioId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}