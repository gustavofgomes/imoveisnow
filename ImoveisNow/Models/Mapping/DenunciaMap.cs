﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImoveisNow.Models.Mapping
{
    public class DenunciaMap: IEntityTypeConfiguration<Denuncia>
    {
        public void Configure(EntityTypeBuilder<Denuncia> builder)
        {
            //Table Name
            builder.ToTable("Denuncia");

            //Primary Key
            builder.HasKey(x => x.Id);

            //Properties
            builder.Property(x => x.AnuncioId)
                .IsRequired();

            builder.Property(x => x.Texto)
                .HasColumnType("varchar(1000)")
                .HasMaxLength(1000)
                .IsRequired();

            builder.Property(x => x.Motivo)
                .IsRequired();

            builder.Property(x => x.Avaliada)
                .IsRequired();

            builder.Property(x => x.Veredito)
                .IsRequired();

            //Relationships
            builder.HasOne(x => x.Anuncio).WithMany(x => x.ListaDeDenuncias)
                .HasForeignKey(y => y.AnuncioId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}