﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ImoveisNow.Models.Entities;

namespace ImoveisNow.Models.Mapping
{
    public class CidadeMap: IEntityTypeConfiguration<Cidade>
    {
        public void Configure(EntityTypeBuilder<Cidade> builder)
        {
            //Table Name
            builder.ToTable("Cidade");

            //Primary Key
            builder.HasKey(x => x.Id);

            //Properties
            builder.Property(x => x.Nome)
                .HasColumnType("varchar(50)")
                .HasMaxLength(50)
                .IsRequired();

            builder.Property(x => x.EstadoId)
                .IsRequired();

            //Relationships
            builder.HasOne(x => x.Estado).WithMany(x => x.ListaDeCidades)
                .HasForeignKey(y => y.EstadoId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
