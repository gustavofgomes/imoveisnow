﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImoveisNow.Models.Mapping
{
    public class AnunciosFavoritosMap : IEntityTypeConfiguration<AnunciosFavoritos>
    {
        public void Configure(EntityTypeBuilder<AnunciosFavoritos> builder)
        {
            //Table Name
            builder.ToTable("AnunciosFavoritos");

            //Primary Key
            builder.HasKey(x => x.Id);

            //Properties
            builder.Property(x => x.AnuncioId)
                .IsRequired();

            builder.Property(x => x.AnuncianteId)
                .IsRequired();

            //Relationships
            builder.HasOne(x => x.Anuncio).WithMany(x => x.ListaDeFavoritos)
                .HasForeignKey(y => y.AnuncioId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.Anunciante).WithMany(x => x.ListaDeFavoritos)
                .HasForeignKey(y => y.AnuncianteId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}