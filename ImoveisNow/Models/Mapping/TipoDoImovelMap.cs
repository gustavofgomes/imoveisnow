﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImoveisNow.Models.Mapping
{
    public class TipoDoImovelMap : IEntityTypeConfiguration<TipoDoImovel>
    {
        public void Configure(EntityTypeBuilder<TipoDoImovel> builder)
        {
            //Table Name
            builder.ToTable("TipoDoImovel");

            //Primary Key
            builder.HasKey(x => x.Id);

            //Properties
            builder.Property(x => x.Nome)
                .HasColumnType("varchar(36)")
                .HasMaxLength(36)
                .IsRequired();
        }
    }
}