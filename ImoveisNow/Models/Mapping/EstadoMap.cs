﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ImoveisNow.Models.Entities;

namespace ImoveisNow.Models.Mapping
{
    public class EstadoMap: IEntityTypeConfiguration<Estado>
    {
        public void Configure(EntityTypeBuilder<Estado> builder)
        {
            //Table Name
            builder.ToTable("Estado");

            //Primary Key
            builder.HasKey(x => x.Id);

            //Properties
            builder.Property(x => x.Nome)
                .HasColumnType("varchar(36)")
                .HasMaxLength(36)
                .IsRequired();

            builder.Property(x => x.Sigla)
                .HasColumnType("varchar(4)")
                .HasMaxLength(4)
                .IsRequired();
        }
    }
}