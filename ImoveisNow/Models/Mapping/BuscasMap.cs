﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImoveisNow.Models.Mapping
{
    public class BuscasMap : IEntityTypeConfiguration<Buscas>
    {
        public void Configure(EntityTypeBuilder<Buscas> builder)
        {
            //Table Name
            builder.ToTable("Buscas");

            //Primary Key
            builder.HasKey(x => x.Id);

            //Properties
            builder.Property(x => x.Parametro)
                .IsRequired();
        }
    }
}