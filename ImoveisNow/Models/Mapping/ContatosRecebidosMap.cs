﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImoveisNow.Models.Mapping
{
    public class ContatosRecebidosMap : IEntityTypeConfiguration<ContatosRecebidos>
    {
        public void Configure(EntityTypeBuilder<ContatosRecebidos> builder)
        {
            //Table Name
            builder.ToTable("ContatosRecebidos");

            //Primary Key
            builder.HasKey(x => x.Id);

            //Properties
            builder.Property(x => x.Nome)
                .HasColumnType("varchar(36)")
                .HasMaxLength(36)
                .IsRequired();

            builder.Property(x => x.AnuncioId)
                .IsRequired();

            builder.Property(x => x.DataEHora)
                .IsRequired();

            builder.Property(x => x.Mensagem)
                .IsRequired();

            builder.Property(x => x.Arquivado)
                .IsRequired();

            //Relationships
            builder.HasOne(x => x.Anuncio).WithMany(x => x.ListaDeContatosRecebidos)
                .HasForeignKey(y => y.AnuncioId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}