﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImoveisNow.Models.Mapping
{
    public class PlanoDoAnuncianteMap : IEntityTypeConfiguration<PlanoDoAnunciante>
    {
        public void Configure(EntityTypeBuilder<PlanoDoAnunciante> builder)
        {
            //Table Name
            builder.ToTable("PlanoDoAnunciante");

            //Primary Key
            builder.HasKey(x => x.Id);

            //Properties
            builder.Property(x => x.TipoDePlanoId)
                .IsRequired();

            builder.Property(x => x.DataDeInicio)
                .IsRequired();

            builder.Property(x => x.DataDeTermino)
                .IsRequired();

            //Relationships
            builder.HasOne(x => x.TipoDePlano).WithMany(x => x.ListaDePlanosDoAnunciante)
                .HasForeignKey(y => y.TipoDePlanoId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.Anunciante).WithMany(x => x.ListaDePlanoDoAnunciante)
                .HasForeignKey(y => y.AnuncianteId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}