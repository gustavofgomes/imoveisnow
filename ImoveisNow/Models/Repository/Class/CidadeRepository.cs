﻿using ImoveisNow.Models.Context;
using ImoveisNow.Models.Entities;
using ImoveisNow.Models.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ImoveisNow.Models.Repository.Class
{
    public class CidadeRepository: ICidadeRepository
    {
        private readonly INContext context;

        public CidadeRepository(INContext context)
        {
            this.context = context;
        }
        public ICollection<Cidade> ObterCidades(bool @readonly)
        {
            var dados = context.Cidade
                .Include(x => x.Estado);

            if (@readonly)
                dados.AsNoTracking();

            return dados.ToList();
        }

        public Cidade BuscarPorId(int cidadeId, bool @readonly)
        {
            var dados = context.Cidade;

            if (@readonly)
                dados.AsNoTracking();

            return dados
                .FirstOrDefault(x => x.Id == cidadeId);
        }
    }
}