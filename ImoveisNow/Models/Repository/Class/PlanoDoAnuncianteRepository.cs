﻿using ImoveisNow.Models.Context;
using ImoveisNow.Models.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace ImoveisNow.Models.Repository.Class
{
    public class PlanoDoAnuncianteRepository: IPlanoDoAnuncianteRepository
    {
        private readonly INContext context;

        public PlanoDoAnuncianteRepository(INContext context)
        {
            this.context = context;
        }

        public int ObterNumeroDePlanosPremiumsAtivo(bool @readonly)
        {
            var dados = context.PlanoDoAnunciante;

            if (@readonly)
                dados.AsNoTracking();

            return dados
                .Where(x => x.TipoDePlanoId != 1)
                .Count();
        }
    }
}