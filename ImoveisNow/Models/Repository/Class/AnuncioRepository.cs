﻿using System;
using System.Collections.Generic;
using System.Linq;
using ImoveisNow.Models.Context;
using ImoveisNow.Models.Entities;
using ImoveisNow.Models.Entities.Enum;
using ImoveisNow.Models.Helpers;
using ImoveisNow.Models.Repository.Interface;
using Microsoft.EntityFrameworkCore;

namespace ImoveisNow.Models.Repository.Class
{
    public class AnuncioRepository : IAnuncioRepository
    {
        private readonly INContext context;

        public AnuncioRepository(INContext context)
        {
            this.context = context;
        }

        public void CriarAnuncio(Anuncio anuncio)
        {
            context.Add(anuncio);
            context.SaveChangesAsync();
        }

        public ICollection<Anuncio> BuscarAnunciosDoUsuario(Guid usuarioId, bool @readonly)
        {
            var dados = context.Anuncio
                .Include(x => x.ListaDeImagens)
                .Include(x => x.Cidade)
                    .ThenInclude(x => x.Estado)
                .Include(x => x.Anunciante)
                .Include(x => x.Imovel)
                    .ThenInclude(x => x.Endereco)
                .Include(x => x.ListaDeVisualizacoes);

            if (@readonly)
                dados.AsNoTracking();

            return dados
                .Where(x => x.AnuncianteId == usuarioId)
                .ToList();
        }

        public Anuncio BuscarAnuncioPorId(Guid idAnuncio, bool @readonly)
        {
            var dados = context.Anuncio
                .Include(x => x.ListaDeImagens)
                .Include(x => x.Anunciante)
                .Include(x => x.Cidade)
                    .ThenInclude(x => x.Estado)
                .Include(x => x.Imovel)
                    .ThenInclude(x => x.Endereco)
                .Include(x => x.Imovel)
                    .ThenInclude(x => x.TipoDeOferta)
                .Include(x => x.Imovel)
                    .ThenInclude(x => x.TipoDeOferta)
                .Include(x => x.Imovel)
                    .ThenInclude(x => x.TipoDoImovel)
                .Include(x => x.Imovel)
                    .ThenInclude(x => x.DetalhesDoImovel)
                .Include(x => x.Imovel)
                    .ThenInclude(x => x.DetalhesDoTerreno)
                .Include(x => x.Imovel)
                    .ThenInclude(x => x.InformacoesDoCondominio);

            if (@readonly)
                dados.AsNoTracking();

            return dados
                .FirstOrDefault(x => x.Id == idAnuncio);
        }

        public ICollection<Anuncio> BuscarPorRuaBairroCidade(string parametro, bool @readonly)
        {
            var dados = context.Anuncio
                .Include(x => x.ListaDeImagens)
                .Include(x => x.Cidade)
                    .ThenInclude(x => x.Estado)
                .Include(x => x.Imovel)
                    .ThenInclude(x => x.Endereco)
                .Include(x => x.Imovel)
                    .ThenInclude(x => x.DetalhesDoImovel)
                .Include(x => x.Imovel)
                    .ThenInclude(x => x.InformacoesDoCondominio);

            if (@readonly)
                dados.AsNoTracking();

            return dados
                .Where(x => x.Imovel.Endereco.Bairro.Contains(parametro) || x.Cidade.Nome.Contains(parametro))
                .ToList();
        }

        public ICollection<Anuncio> ObterTodosOsAnunciosAgioEVendaAtivos(bool @readonly)
        {
            var dados = context.Anuncio
                .Include(x => x.ListaDeImagens)
                .Include(x => x.Cidade)
                    .ThenInclude(x => x.Estado)
                .Include(x => x.Imovel)
                    .ThenInclude(x => x.Endereco)
                .Include(x => x.Imovel)
                    .ThenInclude(x => x.DetalhesDoImovel)
                .Include(x => x.Imovel)
                    .ThenInclude(x => x.InformacoesDoCondominio);

            if (@readonly)
                dados.AsNoTracking();

            return dados
                .Where(x => x.Imovel.TipoDeOfertaId != 2 && (x.Status == StatusDoAnuncio.Ativo.GetDescription() || x.Status == StatusDoAnuncio.EmDestaque.GetDescription()))
                .OrderByDescending(x => x.Peso)
                .ToList();
        }

        public ICollection<Anuncio> ObterTodosOsAnunciosAlugarAtivos(bool @readonly)
        {
            var dados = context.Anuncio
                .Include(x => x.ListaDeImagens)
                .Include(x => x.Cidade)
                    .ThenInclude(x => x.Estado)
                .Include(x => x.Imovel)
                    .ThenInclude(x => x.Endereco)
                .Include(x => x.Imovel)
                    .ThenInclude(x => x.DetalhesDoImovel)
                .Include(x => x.Imovel)
                    .ThenInclude(x => x.InformacoesDoCondominio);

            if (@readonly)
                dados.AsNoTracking();

            return dados
                .Where(x => x.Imovel.TipoDeOfertaId == 2 && (x.Status == StatusDoAnuncio.Ativo.GetDescription() || x.Status == StatusDoAnuncio.EmDestaque.GetDescription()))
                .OrderByDescending(x => x.Peso)
                .ToList();
        }

        public ICollection<Anuncio> ObterTresUltimosAnunciosEmDestaque(bool @readonly)
        {
            var dados = context.Anuncio
                .Include(x => x.ListaDeImagens)
                .Include(x => x.Cidade)
                    .ThenInclude(x => x.Estado)
                .Include(x => x.Imovel)
                    .ThenInclude(x => x.Endereco)
                .Include(x => x.Imovel)
                    .ThenInclude(x => x.DetalhesDoImovel)
                .Include(x => x.Imovel)
                    .ThenInclude(x => x.InformacoesDoCondominio);

            if (@readonly)
                dados.AsNoTracking();

            return dados
                .Where(x => x.Status == StatusDoAnuncio.EmDestaque.GetDescription())
                .OrderByDescending(x => x.Id)
                .Take(3)
                .ToList();
        }

        public ICollection<Anuncio> ObterTresUltimosAnunciosAnuncios(bool @readonly)
        {
            var dados = context.Anuncio
                .Include(x => x.ListaDeImagens)
                .Include(x => x.Cidade)
                    .ThenInclude(x => x.Estado)
                .Include(x => x.Imovel)
                    .ThenInclude(x => x.Endereco)
                .Include(x => x.Imovel)
                    .ThenInclude(x => x.DetalhesDoImovel)
                .Include(x => x.Imovel)
                    .ThenInclude(x => x.InformacoesDoCondominio);


            if (@readonly)
                dados.AsNoTracking();

            return dados
                .Where(x => x.Status == StatusDoAnuncio.Ativo.GetDescription() || x.Status == StatusDoAnuncio.EmDestaque.GetDescription())
                .OrderByDescending(x => x.DataDeCriacao)
                .Take(3)
                .ToList();
        }

        public int ObterNumeroDeAnunciosAtivo(bool @readonly)
        {
            var dados = context.Anuncio;

            if (@readonly)
                dados.AsNoTracking();

            return dados
                .Where(x => x.Status == StatusDoAnuncio.Ativo.GetDescription() || x.Status == StatusDoAnuncio.EmDestaque.GetDescription())
                .Count();
        }
    }
}