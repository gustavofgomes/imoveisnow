﻿using Identity.Repository.Interface;
using ImoveisNow.Models.Repository.Interface;

namespace ImoveisNow.Models.Repository.Class
{
    public class AllRepository : IAllRepository
    {
        public AllRepository(
        IUsuarioIdentityRepository usuarioIdentityRepository,
        IAnuncianteRepository anuncianteRepository,
        IAnuncioRepository anuncioRepository,
        IAnunciosFavoritosRepository anunciosFavoritosRepository,
        IBuscaRepository buscasRepository,
        ITipoDeOfertaRepository categoriaRepository,
        ICidadeRepository cidadeRepository,
        IContatosRecebidosRepository contatosRecebidosRepository,
        IDenunciaRepository denunciaRepository,
        IFeedbackRepository feedbackRepository,
        IPlanoDoAnuncianteRepository planoPremiumRepository,
        ITipoDoImovelRepository tipoDoImovelRepository,
        IVisualizacaoRepository visualizacoesRepository)
        {
            UsuarioIdentityRepository = usuarioIdentityRepository;
            AnuncianteRepository = anuncianteRepository;
            AnuncioRepository = anuncioRepository;
            AnunciosFavoritosRepository = anunciosFavoritosRepository;
            BuscasRepository = buscasRepository;
            CategoriaRepository = categoriaRepository;
            CidadeRepository = cidadeRepository;
            ContatosRecebidosRepository = contatosRecebidosRepository;
            DenunciaRepository = denunciaRepository;
            FeedbackRepository = feedbackRepository;
            PlanoPremiumRepository = planoPremiumRepository;
            TipoDoImovelRepository = tipoDoImovelRepository;
            VisualizacoesRepository = visualizacoesRepository;
        }
        public IUsuarioIdentityRepository UsuarioIdentityRepository { get; set; }
        public IAnuncianteRepository AnuncianteRepository { get; set; }
        public IAnuncioRepository AnuncioRepository { get; set; }
        public IAnunciosFavoritosRepository AnunciosFavoritosRepository { get; set; }
        public IBuscaRepository BuscasRepository { get; set; }
        public ITipoDeOfertaRepository CategoriaRepository { get; set; }
        public ICidadeRepository CidadeRepository { get; set; }
        public IContatosRecebidosRepository ContatosRecebidosRepository { get; set; }
        public IDenunciaRepository DenunciaRepository { get; set; }
        public IFeedbackRepository FeedbackRepository { get; set; }
        public IPlanoDoAnuncianteRepository PlanoPremiumRepository { get; set; }
        public ITipoDoImovelRepository TipoDoImovelRepository { get; set; }
        public IVisualizacaoRepository VisualizacoesRepository { get; set; }
    }
}