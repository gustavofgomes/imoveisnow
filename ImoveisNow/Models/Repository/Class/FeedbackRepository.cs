﻿using System.Collections.Generic;
using System.Linq;
using ImoveisNow.Models.Context;
using ImoveisNow.Models.Entities;
using ImoveisNow.Models.Repository.Interface;
using Microsoft.EntityFrameworkCore;

namespace ImoveisNow.Models.Repository.Class
{
    public class FeedbackRepository: IFeedbackRepository
    {
        private readonly INContext context;

        public FeedbackRepository(INContext context)
        {
            this.context = context;
        }

        public void Atualizar(Feedback feedback)
        {
            context.Update(feedback);
            context.SaveChanges();
        }

        public void Criar(Feedback feedback)
        {
            context.Add(feedback);
            context.SaveChanges();
        }

        public Feedback BuscarPorId(int id, bool @readonly)
        {
            var dados = context.Feedback;

            if (@readonly)
                dados.AsNoTracking();

            return dados
                .Where(x => x.Id == id)
                .FirstOrDefault();
        }

        public ICollection<Feedback> ObterCincoFeedBacksNaoArquivados(bool @readonly)
        {
            var dados = context.Feedback;

            if (@readonly)
                dados.AsNoTracking();

            return dados
                .Where(x => x.Arquivado == false)
                .Take(5)
                .ToList();
        }
    }
}