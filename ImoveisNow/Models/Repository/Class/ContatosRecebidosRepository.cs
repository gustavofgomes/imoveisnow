﻿using ImoveisNow.Models.Context;
using ImoveisNow.Models.Entities;
using ImoveisNow.Models.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ImoveisNow.Models.Repository.Class
{
    public class ContatosRecebidosRepository : IContatosRecebidosRepository
    {
        private readonly INContext context;

        public ContatosRecebidosRepository(INContext context)
        {
            this.context = context;
        }

        public void Criar(ContatosRecebidos contato)
        {
            context.Add(contato);
            context.SaveChanges();
        }
        public ICollection<ContatosRecebidos> BuscarTodosDoUsuario(Guid usuarioId, bool @readonly)
        {
            var dados = context.ContatosRecebidos
                .Include(x => x.Anuncio);

            if (@readonly)
                dados.AsNoTracking();

            return dados
                .Where(x => x.Anuncio.AnuncianteId == usuarioId)
                .ToList();
        }

        public ContatosRecebidos BuscasPorId(int id, bool @readonly)
        {
            var dados = context.ContatosRecebidos
                .Include(x => x.Anuncio);

            if (@readonly)
                dados.AsNoTracking();

            return dados
                .Where(x => x.Id == id)
                .FirstOrDefault();
        }

        public void Arquivar(ContatosRecebidos contato)
        {
            context.Update(contato);
            context.SaveChanges();
        }
    }
}