﻿using System;
using System.Collections.Generic;
using System.Linq;
using ImoveisNow.Models.Context;
using ImoveisNow.Models.Entities;
using ImoveisNow.Models.Repository.Interface;
using Microsoft.EntityFrameworkCore;

namespace ImoveisNow.Models.Repository.Class
{
    public class AnunciosFavoritosRepository: IAnunciosFavoritosRepository
    {
        private readonly INContext context;

        public AnunciosFavoritosRepository(INContext context)
        {
            this.context = context;
        }

        public void CriarFavorito(AnunciosFavoritos favorito)
        {
            context.Add(favorito);
            context.SaveChanges();
        }

        public AnunciosFavoritos BuscarPorId(Guid anuncioId, Guid usuarioId, bool @readonly)
        {
            return context.AnunciosFavoritos.FirstOrDefault(x => x.AnuncioId == anuncioId && (x.AnuncianteId == usuarioId));
        }

        public void RemoverFavorito(AnunciosFavoritos favoritos)
        {
            context.Remove(favoritos);
            context.SaveChanges();
        }

        public ICollection<AnunciosFavoritos> BuscarTodosDoUsuario(Guid usuarioId, bool @readonly)
        {
            var dados = context.AnunciosFavoritos
                .Include(x => x.Anuncio);

            if (@readonly)
                dados.AsNoTracking();

            return dados
                .Where(x => x.AnuncianteId == usuarioId)
                .ToList();
        }
    }
}