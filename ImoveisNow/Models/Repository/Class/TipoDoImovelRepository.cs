﻿using ImoveisNow.Models.Context;
using ImoveisNow.Models.Entities;
using ImoveisNow.Models.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ImoveisNow.Models.Repository.Class
{
    public class TipoDoImovelRepository: ITipoDoImovelRepository
    {
        private readonly INContext context;

        public TipoDoImovelRepository(INContext context)
        {
            this.context = context;
        }

        public TipoDoImovel BuscarTipoDoImovelPorId(int id, bool @readonly)
        {
            var dados = context.TipoDoImovel;

            if (@readonly)
                dados.AsNoTracking();

            return dados.FirstOrDefault(x => x.Id == id);
        }

        public ICollection<TipoDoImovel> ObterTodosOsTiposDeImoveis(bool @readonly)
        {
            var dados = context.TipoDoImovel;

            if (@readonly)
                dados.AsNoTracking();

            return dados.ToList();
        }
    }
}