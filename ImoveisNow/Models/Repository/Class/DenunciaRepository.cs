﻿using System.Collections.Generic;
using System.Linq;
using ImoveisNow.Models.Context;
using ImoveisNow.Models.Entities;
using ImoveisNow.Models.Repository.Interface;
using Microsoft.EntityFrameworkCore;

namespace ImoveisNow.Models.Repository.Class
{
    public class DenunciaRepository : IDenunciaRepository
    {
        private readonly INContext context;

        public DenunciaRepository(INContext context)
        {
            this.context = context;
        }

        public void Atualizar(Denuncia denuncia)
        {
            context.Update(denuncia);
            context.SaveChanges();
        }

        public Denuncia BuscarPorId(int id, bool @readonly)
        {
            var dados = context.Denuncia;

            if (@readonly)
                dados.AsNoTracking();

            return dados
                .Where(x => x.Id == id)
                .FirstOrDefault();
        }

        public void CriarDenuncia(Denuncia denuncia)
        {
            context.Add(denuncia);
            context.SaveChanges();
        }

        public ICollection<Denuncia> ObterCincoDenunciasNaoAvaliadas(bool @readonly)
        {
            var dados = context.Denuncia;

            if (@readonly)
                dados.AsNoTracking();

            return dados
                .Where(x => x.Avaliada == false)
                .Take(5)
                .ToList();
        }

        public ICollection<Denuncia> ObterTodasDenunciasNaoAvaliadas(bool @readonly)
        {
            var dados = context.Denuncia;

            if (@readonly)
                dados.AsNoTracking();

            return dados
                .Where(x => x.Avaliada == false)
                .ToList();
        }
    }
}