﻿using ImoveisNow.Models.Context;
using ImoveisNow.Models.Entities;
using ImoveisNow.Models.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ImoveisNow.Models.Repository.Class
{
    public class TipoDeOfertaRepository: ITipoDeOfertaRepository
    {
        private readonly INContext context;

        public TipoDeOfertaRepository(INContext context)
        {
            this.context = context;
        }

        public TipoDeOferta BuscarCategoriaPorId(int id, bool @readonly)
        {
            var dados = context.TipoDeOferta;

            if (@readonly)
                dados.AsNoTracking();

            return dados.FirstOrDefault(x => x.Id == id);
        }

        public ICollection<TipoDeOferta> ObterTodasCategorias(bool @readonly)
        {
            var dados = context.TipoDeOferta;

            if (@readonly)
                dados.AsNoTracking();

            return dados.ToList();
        }
    }
}