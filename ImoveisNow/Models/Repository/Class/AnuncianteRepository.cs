﻿using System;
using System.Linq;
using System.Threading.Tasks;
using ImoveisNow.Models.Context;
using ImoveisNow.Models.Entities;
using ImoveisNow.Models.Repository.Interface;
using Microsoft.EntityFrameworkCore;

namespace ImoveisNow.Models.Repository.Class
{
    public class AnuncianteRepository : IAnuncianteRepository
    {
        private readonly INContext context;

        public AnuncianteRepository(INContext context)
        {
            this.context = context;
        }

        public async Task CriarAnunciante(Anunciante anunciante)
        {
            await context.Anunciante.AddAsync(anunciante);
            await context.SaveChangesAsync();
        }
        public Anunciante BuscarAnunciantePorId(Guid id, bool @readonly)
        {
            var dados = context.Anunciante
                .Include(x => x.ListaDePlanoDoAnunciante)
                    .ThenInclude(x => x.TipoDePlano)
                .Include(x => x.ListaDeAnuncios)
                .Include(x => x.ListaDeFavoritos);

            if (@readonly)
                dados.AsNoTracking();

            return dados
                .FirstOrDefault(x => x.Id == id);
        }

        public int ObterNumeroDeAnunciantesAtivo(bool @readonly)
        {
            var dados = context.Anunciante;

            if (@readonly)
                dados.AsNoTracking();

            return dados
                .Where(x => x.Ativo == true)
                .Count();
        }
    }
}