﻿using ImoveisNow.Models.Context;
using ImoveisNow.Models.Entities;
using ImoveisNow.Models.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace ImoveisNow.Models.Repository.Class
{
    public class VisualizacaoRepository: IVisualizacaoRepository
    {
        private readonly INContext context;

        public VisualizacaoRepository(INContext context)
        {
            this.context = context;
        }

        public void CriarVisualizacao(Visualizacao visualizacao)
        {
            context.Add(visualizacao);
            context.SaveChanges();
        }

        public int ObterNumeroDeVisualizacoes(bool @readonly)
        {
            var dados = context.Visualizacao;

            if (@readonly)
                dados.AsNoTracking();

            return dados.Count();
        }
    }
}