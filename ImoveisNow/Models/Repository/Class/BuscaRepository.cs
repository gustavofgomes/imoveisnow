﻿using ImoveisNow.Models.Context;
using ImoveisNow.Models.Entities;
using ImoveisNow.Models.Repository.Interface;

namespace ImoveisNow.Models.Repository.Class
{
    public class BuscaRepository: IBuscaRepository
    {
        private readonly INContext context;

        public BuscaRepository(INContext context)
        {
            this.context = context;
        }

        public void CriarBusca(Buscas busca)
        {
            context.Add(busca);
            context.SaveChanges();
        }
    }
}