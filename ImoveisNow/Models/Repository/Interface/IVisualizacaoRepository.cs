﻿using ImoveisNow.Models.Entities;

namespace ImoveisNow.Models.Repository.Interface
{
    public interface IVisualizacaoRepository
    {
        void CriarVisualizacao(Visualizacao visualizacoes);
        int ObterNumeroDeVisualizacoes(bool @readonly);
    }
}