﻿using ImoveisNow.Models.Entities;
using System;
using System.Collections.Generic;

namespace ImoveisNow.Models.Repository.Interface
{
    public interface IContatosRecebidosRepository
    {
        void Criar(ContatosRecebidos contato);
        ICollection<ContatosRecebidos> BuscarTodosDoUsuario(Guid usuarioId, bool @readonly);
        ContatosRecebidos BuscasPorId(int id, bool v);
        void Arquivar(ContatosRecebidos contato);
    }
}