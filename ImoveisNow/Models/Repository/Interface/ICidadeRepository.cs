﻿using ImoveisNow.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImoveisNow.Models.Repository.Interface
{
    public interface ICidadeRepository
    {
        ICollection<Cidade> ObterCidades(bool @readonly);
        Cidade BuscarPorId(int cidadeId, bool @readonly);
    }
}