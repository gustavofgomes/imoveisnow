﻿using ImoveisNow.Models.Entities;
using System.Collections.Generic;

namespace ImoveisNow.Models.Repository.Interface
{
    public interface ITipoDoImovelRepository
    {
        TipoDoImovel BuscarTipoDoImovelPorId(int id, bool @readonly);
        ICollection<TipoDoImovel> ObterTodosOsTiposDeImoveis(bool @readonly);
    }
}