﻿namespace ImoveisNow.Models.Repository.Interface
{
    public interface IPlanoDoAnuncianteRepository
    {
        int ObterNumeroDePlanosPremiumsAtivo(bool @readonly);
    }
}