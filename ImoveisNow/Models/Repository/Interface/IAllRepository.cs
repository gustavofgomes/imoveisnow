﻿using Identity.Repository.Interface;

namespace ImoveisNow.Models.Repository.Interface
{
    public interface IAllRepository
    {
        IUsuarioIdentityRepository UsuarioIdentityRepository { get; set; }
        IAnuncianteRepository AnuncianteRepository { get; set; }
        IAnuncioRepository AnuncioRepository { get; set; }
        IAnunciosFavoritosRepository AnunciosFavoritosRepository { get; set; }
        IBuscaRepository BuscasRepository { get; set; }
        ITipoDeOfertaRepository CategoriaRepository { get; set; }
        ICidadeRepository CidadeRepository { get; set; }
        IContatosRecebidosRepository ContatosRecebidosRepository { get; set; }
        IDenunciaRepository DenunciaRepository { get; set; }
        IFeedbackRepository FeedbackRepository { get; set; }
        IPlanoDoAnuncianteRepository PlanoPremiumRepository { get; set; }
        ITipoDoImovelRepository TipoDoImovelRepository { get; set; }
        IVisualizacaoRepository VisualizacoesRepository { get; set; }
    }
}