﻿using ImoveisNow.Models.Entities;
using System.Collections.Generic;

namespace ImoveisNow.Models.Repository.Interface
{
    public interface IDenunciaRepository
    {
        void CriarDenuncia(Denuncia denuncia);
        ICollection<Denuncia> ObterCincoDenunciasNaoAvaliadas(bool @readonly);
        ICollection<Denuncia> ObterTodasDenunciasNaoAvaliadas(bool @readonly);
        Denuncia BuscarPorId(int id, bool @readonly);
        void Atualizar(Denuncia denuncia);
    }
}