﻿using ImoveisNow.Models.Entities;
using System;
using System.Collections.Generic;

namespace ImoveisNow.Models.Repository.Interface
{
    public interface IAnuncioRepository
    {
        void CriarAnuncio(Anuncio anuncio);
        ICollection<Anuncio> BuscarAnunciosDoUsuario(Guid usuarioId, bool @readonly);
        Anuncio BuscarAnuncioPorId(Guid idAnuncio, bool @readonly);
        ICollection<Anuncio> BuscarPorRuaBairroCidade(string parametro, bool @readonly);
        ICollection<Anuncio> ObterTodosOsAnunciosAgioEVendaAtivos(bool @readonly);
        ICollection<Anuncio> ObterTodosOsAnunciosAlugarAtivos(bool @readonly);
        ICollection<Anuncio> ObterTresUltimosAnunciosEmDestaque(bool @readonly);
        ICollection<Anuncio> ObterTresUltimosAnunciosAnuncios(bool @readonly);
        int ObterNumeroDeAnunciosAtivo(bool @readonly);
    }
}