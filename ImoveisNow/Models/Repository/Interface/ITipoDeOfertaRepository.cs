﻿using ImoveisNow.Models.Entities;
using System.Collections.Generic;

namespace ImoveisNow.Models.Repository.Interface
{
    public interface ITipoDeOfertaRepository
    {
        TipoDeOferta BuscarCategoriaPorId(int id, bool @readonly);
        ICollection<TipoDeOferta> ObterTodasCategorias(bool @readonly);
    }
}