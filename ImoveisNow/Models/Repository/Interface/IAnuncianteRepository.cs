﻿using ImoveisNow.Models.Entities;
using System;
using System.Threading.Tasks;

namespace ImoveisNow.Models.Repository.Interface
{
    public interface IAnuncianteRepository
    {
        Task CriarAnunciante(Anunciante anunciante);
        Anunciante BuscarAnunciantePorId(Guid id, bool @readonly);
        int ObterNumeroDeAnunciantesAtivo(bool @readonly);
    }
}