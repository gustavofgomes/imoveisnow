﻿using ImoveisNow.Models.Entities;

namespace ImoveisNow.Models.Repository.Interface
{
    public interface IBuscaRepository
    {
        void CriarBusca(Buscas busca);
    }
}