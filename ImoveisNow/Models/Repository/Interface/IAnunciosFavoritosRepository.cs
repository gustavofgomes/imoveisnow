﻿using System;
using System.Collections.Generic;
using ImoveisNow.Models.Entities;

namespace ImoveisNow.Models.Repository.Interface
{
    public interface IAnunciosFavoritosRepository
    {
        void CriarFavorito(AnunciosFavoritos favorito);
        AnunciosFavoritos BuscarPorId(Guid anuncioId, Guid usuarioId, bool @readonly);
        void RemoverFavorito(AnunciosFavoritos favoritos);
        ICollection<AnunciosFavoritos> BuscarTodosDoUsuario(Guid usuarioId, bool @readonly);
    }
}