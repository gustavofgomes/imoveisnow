﻿using ImoveisNow.Models.Entities;
using System.Collections.Generic;

namespace ImoveisNow.Models.Repository.Interface
{
    public interface IFeedbackRepository
    {
        ICollection<Feedback> ObterCincoFeedBacksNaoArquivados(bool @readonly);
        Feedback BuscarPorId(int id, bool @readonly);
        void Atualizar(Feedback feedback);
        void Criar(Feedback feedback);
    }
}