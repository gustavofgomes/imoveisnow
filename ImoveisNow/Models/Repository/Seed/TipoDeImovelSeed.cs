﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace ImoveisNow.Models.Repository.Seed
{
    public static class TipoDeImovelSeed
    {
        public static void PopularTipoDeImovel(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TipoDoImovel>(m => { m.HasData(new { Id = 1, Nome = "Casa"}); });
            modelBuilder.Entity<TipoDoImovel>(m => { m.HasData(new { Id = 2, Nome = "Apartamento"}); });
            modelBuilder.Entity<TipoDoImovel>(m => { m.HasData(new { Id = 3, Nome = "Cobertuda"}); });
            modelBuilder.Entity<TipoDoImovel>(m => { m.HasData(new { Id = 4, Nome = "Flat"}); });
            modelBuilder.Entity<TipoDoImovel>(m => { m.HasData(new { Id = 5, Nome = "Kitnet/Conjugado" }); });
            modelBuilder.Entity<TipoDoImovel>(m => { m.HasData(new { Id = 6, Nome = "Sobrado"}); });
            modelBuilder.Entity<TipoDoImovel>(m => { m.HasData(new { Id = 7, Nome = "Ponto comercial"}); });
            modelBuilder.Entity<TipoDoImovel>(m => { m.HasData(new { Id = 8, Nome = "Loja"}); });
            modelBuilder.Entity<TipoDoImovel>(m => { m.HasData(new { Id = 9, Nome = "Box"}); });
            modelBuilder.Entity<TipoDoImovel>(m => { m.HasData(new { Id = 10, Nome = "Imóvel comercial"}); });
            modelBuilder.Entity<TipoDoImovel>(m => { m.HasData(new { Id = 11, Nome = "Lote/Terreno" }); });
            modelBuilder.Entity<TipoDoImovel>(m => { m.HasData(new { Id = 12, Nome = "Fazenda/Sítio" }); });
            modelBuilder.Entity<TipoDoImovel>(m => { m.HasData(new { Id = 13, Nome = "Chácara" }); });
        }
    }
}