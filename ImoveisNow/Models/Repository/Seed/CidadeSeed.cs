﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace ImoveisNow.Models.Repository.Seed
{
    public static class CidadeSeed
    {
        public static void PopularCidades(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cidade>(m => { m.HasData(new { Id = 1, Nome = "Porto Velho", EstadoId = 1 }); });
        }
    }
}