﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace ImoveisNow.Models.Repository.Seed
{
    public static class EnderecoSeed
    {
        public static void PopularEndereco(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Endereco>(m =>
            {
                m.HasData(new
                {
                    Id = new Guid("37f0b25d-a6b3-4337-bf8b-8a6a289c3ebc"),
                    Cep = "76811-200",
                    Bairro = "Castanheira",
                    Logradouro = "Rua São Borja",
                    Numero = "6169"
                });
            });

            modelBuilder.Entity<Endereco>(m =>
            {
                m.HasData(new
                {
                    Id = new Guid("987e10f8-5042-4b54-aca6-f7bfb01e5301"),
                    Cep = "76811-200",
                    Bairro = "Areal",
                    Logradouro = "Rua Três e meio",
                    Numero = "1341"
                });
            });

            modelBuilder.Entity<Endereco>(m =>
            {
                m.HasData(new
                {
                    Id = new Guid("796e2890-519a-40e4-b8d5-631e67cbc37a"),
                    Cep = "76811-200",
                    Bairro = "Cohab",
                    Logradouro = "Rua Aroeira",
                    Numero = "5796"
                });
            });

            modelBuilder.Entity<Endereco>(m =>
            {
                m.HasData(new
                {
                    Id = new Guid("7dac6fff-eeff-47e9-92df-a8341f0c44fc"),
                    Cep = "76811-200",
                    Bairro = "Aeroclube",
                    Logradouro = "Estrada do Japônes",
                    Numero = "438"
                });
            });

            modelBuilder.Entity<Endereco>(m =>
            {
                m.HasData(new
                {
                    Id = new Guid("3a6b2dca-a817-4fc5-be1d-37af2d62ea6f"),
                    Cep = "76811-200",
                    Bairro = "Eldorado",
                    Logradouro = "Rua Açai",
                    Numero = "7243"
                });
            });

            modelBuilder.Entity<Endereco>(m =>
            {
                m.HasData(new
                {
                    Id = new Guid("5015e685-eda4-4591-b63b-44764c8c41c1"),
                    Cep = "76811-200",
                    Bairro = "Caladinho",
                    Logradouro = "Rua Tancredo Neves",
                    Numero = "3567"
                });
            });
        }
    }
}