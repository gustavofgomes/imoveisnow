﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace ImoveisNow.Models.Repository.Seed
{
    public static class ImagensSeed
    {
        public static void PopularImagens(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Imagens>(m =>
            {
                m.HasData(new
                {
                    Id = 1,
                    AnuncioId = new Guid("37f0b25d-a6b3-4337-bf8b-8a6a289c3ebc"),
                    Nome = "7683571e-d99b-4a9a-9059-6b1a2c7675a8",
                    Url = "/upload/",
                    Formato = ".jpg",
                    Tamanho = "875",
                    Largura = 360,
                    Altura = 240
                });
            });

            modelBuilder.Entity<Imagens>(m =>
            {
                m.HasData(new
                {
                    Id = 2,
                    AnuncioId = new Guid("37f0b25d-a6b3-4337-bf8b-8a6a289c3ebc"),
                    Nome = "f88fa5dd-d329-4a73-99d1-21efa9cffc79",
                    Url = "/upload/",
                    Formato = ".jpg",
                    Tamanho = "798",
                    Largura = 360,
                    Altura = 240
                });
            });

            modelBuilder.Entity<Imagens>(m =>
            {
                m.HasData(new
                {
                    Id = 3,
                    AnuncioId = new Guid("37f0b25d-a6b3-4337-bf8b-8a6a289c3ebc"),
                    Nome = "9d16061d-779c-42b3-ad34-9508244898eb",
                    Url = "/upload/",
                    Formato = ".jpg",
                    Tamanho = "1040",
                    Largura = 360,
                    Altura = 240
                });
            });

            modelBuilder.Entity<Imagens>(m =>
            {
                m.HasData(new
                {
                    Id = 4,
                    AnuncioId = new Guid("987e10f8-5042-4b54-aca6-f7bfb01e5301"),
                    Nome = "455a1961-41b8-4ff5-9ab2-d6ff53f5770f",
                    Url = "/upload/",
                    Formato = ".jpg",
                    Tamanho = "342",
                    Largura = 870,
                    Altura = 489
                });
            });

            modelBuilder.Entity<Imagens>(m =>
            {
                m.HasData(new
                {
                    Id = 5,
                    AnuncioId = new Guid("987e10f8-5042-4b54-aca6-f7bfb01e5301"),
                    Nome = "85870e80-36f3-422d-9afa-bfa2e7bb1fd5",
                    Url = "/upload/",
                    Formato = ".jpg",
                    Tamanho = "495",
                    Largura = 812,
                    Altura = 397
                });
            });

            modelBuilder.Entity<Imagens>(m =>
            {
                m.HasData(new
                {
                    Id = 6,
                    AnuncioId = new Guid("796e2890-519a-40e4-b8d5-631e67cbc37a"),
                    Nome = "81c2bbee-fcb9-49ae-ae16-8aab1c8cf948",
                    Url = "/upload/",
                    Formato = ".jpg",
                    Tamanho = "875",
                    Largura = 849,
                    Altura = 539
                });
            });

            modelBuilder.Entity<Imagens>(m =>
            {
                m.HasData(new
                {
                    Id = 7,
                    AnuncioId = new Guid("7dac6fff-eeff-47e9-92df-a8341f0c44fc"),
                    Nome = "fd79d498-793b-46f5-a813-7c8bfd5f710f",
                    Url = "/upload/",
                    Formato = ".jpg",
                    Tamanho = "875",
                    Largura = 849,
                    Altura = 539
                });
            });

            modelBuilder.Entity<Imagens>(m =>
            {
                m.HasData(new
                {
                    Id = 8,
                    AnuncioId = new Guid("3a6b2dca-a817-4fc5-be1d-37af2d62ea6f"),
                    Nome = "3577cb3a-35d6-43a4-b6a8-03523b246736",
                    Url = "/upload/",
                    Formato = ".jpg",
                    Tamanho = "645",
                    Largura = 870,
                    Altura = 522
                });
            });

            modelBuilder.Entity<Imagens>(m =>
            {
                m.HasData(new
                {
                    Id = 9,
                    AnuncioId = new Guid("5015e685-eda4-4591-b63b-44764c8c41c1"),
                    Nome = "f5b2efce-dd8d-4c6f-8a64-76db0b502206",
                    Url = "/upload/",
                    Formato = ".jpg",
                    Tamanho = "232",
                    Largura = 870,
                    Altura = 580
                });
            });
        }
    }
}