﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace ImoveisNow.Models.Repository.Seed
{
    public static class DenunciaSeed
    {
        public static void PopularDenuncias(this ModelBuilder modelBuilder)
        {
            var data = DateTime.Now;
            DateTime? dataAvaliada = null;
            var imovelJavendido = "Imóvel já foi vendido";
            var veredito = "Não Avaliada";

            modelBuilder.Entity<Denuncia>(m =>
            {
                m.HasData(new
                {
                    Id = 1,
                    AnuncioId = new Guid("37f0b25d-a6b3-4337-bf8b-8a6a289c3ebc"),
                    Texto = "Entrei em contato com o Anunciante e o mesmo informou que o Imóvel já se encontra vendido mas o anúncio estar aberto",
                    Motivo = imovelJavendido,
                    DataEHoraDeCriacao = data,
                    AvaliadaEm = dataAvaliada,
                    Avaliada = false,
                    Veredito = veredito
                });
            });
        }
    }
}