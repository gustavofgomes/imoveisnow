﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace ImoveisNow.Models.Repository.Seed
{
    public static class DetalhesDoTerrenoSeed
    {
        public static void PopularDetalhesDosTerrenos(this ModelBuilder modelBuilder)
        {
            var anuncioTerreno4 = new Guid("7dac6fff-eeff-47e9-92df-a8341f0c44fc");
            var anuncioTerreno5 = new Guid("3a6b2dca-a817-4fc5-be1d-37af2d62ea6f");

            modelBuilder.Entity<DetalhesDoTerreno>(m =>
            {
                m.HasData(new
                {
                    Id = anuncioTerreno4,
                    Planado = false,
                    Limpo = false,
                    AguaEncanada = false,
                    PocoArtesiano = true,
                    EnergiaEletrica = true,
                    AreaVerde = true,
                    CasaSede = false
                });
            });

            modelBuilder.Entity<DetalhesDoTerreno>(m =>
            {
                m.HasData(new
                {
                    Id = anuncioTerreno5,
                    Planado = false,
                    Limpo = false,
                    AguaEncanada = true,
                    PocoArtesiano = true,
                    EnergiaEletrica = true,
                    AreaVerde = false,
                    CasaSede = true
                });
            });
        }
    }
}