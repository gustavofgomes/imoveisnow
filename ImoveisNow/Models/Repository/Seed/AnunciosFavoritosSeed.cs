﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace ImoveisNow.Models.Repository.Seed
{
    public static class AnunciosFavoritosSeed
    {
        public static void PopularAnunciosFavoritos(this ModelBuilder modelBuilder)
        {
            var anunciante1 = new Guid("84ad2c82-23f4-4b52-b379-ebd4e07bc7ed");
            var anuncio1 = new Guid("987e10f8-5042-4b54-aca6-f7bfb01e5301");

            modelBuilder.Entity<AnunciosFavoritos>(m => { m.HasData(new { Id = 1, AnuncioId = anuncio1, AnuncianteId = anunciante1 }); });
        }
    }
}