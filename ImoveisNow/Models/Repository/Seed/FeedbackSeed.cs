﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace ImoveisNow.Models.Repository.Seed
{
    public static class FeedbackSeed
    {
        public static void PopularFeedbacks(this ModelBuilder modelBuilder)
        {
            var data = DateTime.Now;

            modelBuilder.Entity<Feedback>(m => { m.HasData(new {
                Id = 1,
                Email = "feedback@feedback.com",
                Texto = "Mensagem enviada pelo Feedback",
                ReceberResposta = true,
                Arquivado = false,
                DataEHora = data});
            });
        }
    }
}