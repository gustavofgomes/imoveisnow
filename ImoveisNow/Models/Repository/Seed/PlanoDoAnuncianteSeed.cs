﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace ImoveisNow.Models.Repository.Seed
{
    public static class PlanoDoAnuncianteSeed
    {
        public static void PopularPlanoAtivos(this ModelBuilder modelBuilder)
        {
            var dataInicio = DateTime.Now.Date;
            var dataTerminoPremium = DateTime.Now.AddDays(30);
            var dataTerminoGratuito = DateTime.Now.AddYears(1);
            var proprietarioId = "84ad2c82-23f4-4b52-b379-ebd4e07bc7ed";
            var corretorId = "874a2abd-4d56-4af5-b361-c2b24234d519";
            var planoGratuito = 1;
            var planoPremium1 = 2;

            modelBuilder.Entity<PlanoDoAnunciante>(m =>
            {
                m.HasData(new
                {
                    Id = 1,
                    TipoDePlanoId = planoGratuito,
                    AnuncianteId = new Guid(corretorId),
                    DataDeInicio = dataInicio,
                    DataDeTermino = dataTerminoGratuito,
                    Ativo = true
                });
            });
            modelBuilder.Entity<PlanoDoAnunciante>(m =>
            {
                m.HasData(new
                {
                    Id = 2,
                    TipoDePlanoId = planoPremium1,
                    AnuncianteId = new Guid(proprietarioId),
                    DataDeInicio = dataInicio,
                    DataDeTermino = dataTerminoPremium,
                    Ativo = true
                });
            });
        }
    }
}