﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace ImoveisNow.Models.Repository.Seed
{
    public static class EstadoSeed
    {
        public static void PopularEstados(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Estado>(m => { m.HasData(new { Id = 1, Nome = "Rondônia", Sigla = "RO" }); });
        }
    }
}