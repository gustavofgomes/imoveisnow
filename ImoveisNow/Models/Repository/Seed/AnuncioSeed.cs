﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace ImoveisNow.Models.Repository.Seed
{
    public static class AnuncioSeed
    {
        public static void PopularAnuncios(this ModelBuilder modelBuilder)
        {
            var data = DateTime.Now;
            var statusAtivo = "Ativo";
            var statusDestaque = "Em Destaque";

            modelBuilder.Entity<Anuncio>(m =>
            {
                m.HasData(new
                {
                    Id = new Guid("37f0b25d-a6b3-4337-bf8b-8a6a289c3ebc"),
                    Valor = new decimal(110000.00),
                    Titulo = "Casa á venda, 96m² com 2 Quartos",
                    Descricao = "Casa de 96 metros quadrados está localizado no bairro Castanheira com 2 Quartos e 1 Banheiro, Sala de estar e jantar, cozinha, churrasqueira, área de serviço e um amplo terreno.",
                    DataDeCriacao = data,
                    AnuncianteId = new Guid("84ad2c82-23f4-4b52-b379-ebd4e07bc7ed"),
                    CidadeId = 1,
                    OcultarTelefone = false,
                    Peso = 100,
                    Status = statusDestaque
                });
            });

            modelBuilder.Entity<Anuncio>(m =>
            {
                m.HasData(new
                {
                    Id = new Guid("987e10f8-5042-4b54-aca6-f7bfb01e5301"),
                    Valor = new decimal(160000.00),
                    Titulo = "Casa á venda, 148m² com 3 Quartos",
                    Descricao = "Casa de 148 metros quadrados está localizado no bairro Areal com 3 Quartos, 2 Banheiro e 1 Suite, Sala de estar e jantar, cozinha, churrasqueira, área de serviço e um amplo terreno.",
                    DataDeCriacao = data,
                    AnuncianteId = new Guid("84ad2c82-23f4-4b52-b379-ebd4e07bc7ed"),
                    CidadeId = 1,
                    OcultarTelefone = false,
                    Peso = 90,
                    Status = statusAtivo
                });
            });

            modelBuilder.Entity<Anuncio>(m =>
            {
                m.HasData(new
                {
                    Id = new Guid("796e2890-519a-40e4-b8d5-631e67cbc37a"),
                    Valor = new decimal(80000.00),
                    Titulo = "Casa á venda, 76m², com 2 Quartos",
                    Descricao = "Casa de 76 metros quadrados está localizado no bairro Cohab com 2 Quartos e 1 Banheiro, cozinha, área de serviço e um amplo terreno.",
                    DataDeCriacao = data,
                    AnuncianteId = new Guid("84ad2c82-23f4-4b52-b379-ebd4e07bc7ed"),
                    CidadeId = 1,
                    OcultarTelefone = true,
                    Peso = 80,
                    Status = statusAtivo
                });
            });

            modelBuilder.Entity<Anuncio>(m =>
            {
                m.HasData(new
                {
                    Id = new Guid("7dac6fff-eeff-47e9-92df-a8341f0c44fc"),
                    Valor = new decimal(2000000.00),
                    Titulo = "Terreno/Lote á venda, 1000m²",
                    Descricao = "Terreno de 1000 metros quadrados localizado Estrada do Japonês, Bairro Aeroclube.",
                    DataDeCriacao = data,
                    AnuncianteId = new Guid("84ad2c82-23f4-4b52-b379-ebd4e07bc7ed"),
                    CidadeId = 1,
                    OcultarTelefone = true,
                    Peso = 60,
                    Status = statusDestaque
                });
            });

            modelBuilder.Entity<Anuncio>(m =>
            {
                m.HasData(new
                {
                    Id = new Guid("3a6b2dca-a817-4fc5-be1d-37af2d62ea6f"),
                    Valor = new decimal(1000000.00),
                    Titulo = "Terreno/Lote á venda, 500m²",
                    Descricao = "Terreno de 500 metros quadrados localizado no Bairro Eldorado.",
                    DataDeCriacao = data,
                    AnuncianteId = new Guid("84ad2c82-23f4-4b52-b379-ebd4e07bc7ed"),
                    CidadeId = 1,
                    OcultarTelefone = false,
                    Peso = 50,
                    Status = statusDestaque
                });
            });

            modelBuilder.Entity<Anuncio>(m =>
            {
                m.HasData(new
                {
                    Id = new Guid("5015e685-eda4-4591-b63b-44764c8c41c1"),
                    Valor = new decimal(90000.00),
                    Titulo = "Apartamento para alugar, 58m²",
                    Descricao = "Apartamento de 58 metros quadrados localizado no Bairro Embratel.",
                    DataDeCriacao = data,
                    AnuncianteId = new Guid("84ad2c82-23f4-4b52-b379-ebd4e07bc7ed"),
                    CidadeId = 1,
                    OcultarTelefone = false,
                    Peso = 70,
                    Status = statusDestaque
                });
            });
        }
    }
}