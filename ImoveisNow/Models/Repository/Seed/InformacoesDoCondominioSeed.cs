﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace ImoveisNow.Models.Repository.Seed
{
    public static class InformacoesDoCondominioSeed
    {
        public static void PopularInformacoesDosCondominios(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<InformacoesDoCondominio>(m =>
            {
                m.HasData(new
                {
                    Id = new Guid("37f0b25d-a6b3-4337-bf8b-8a6a289c3ebc"),
                    Nome = "Condominio San Marcos",
                    ValorDoCondominio = new decimal(300.00),
                    Fechado = true,
                    SistemaDeVigilancia = true,
                    PortaoEletronico = true,
                    PermiteAnimais = true,
                    Elevador = true
                });
            });
        }
    }
}