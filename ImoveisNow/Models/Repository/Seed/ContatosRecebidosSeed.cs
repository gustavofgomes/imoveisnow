﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace ImoveisNow.Models.Repository.Seed
{
    public static class ContatosRecebidosSeed
    {
        public static void PopularContatosRecebidos(this ModelBuilder modelBuilder)
        {
            var data = DateTime.Now;
            var anuncio1 = new Guid("37f0b25d-a6b3-4337-bf8b-8a6a289c3ebc");

            modelBuilder.Entity<ContatosRecebidos>(m =>
            {
                m.HasData(new
                {
                    Id = 1,
                    AnuncioId = anuncio1,
                    DataEHora = data,
                    Nome = "João Lucas",
                    Telefone = "(69) 99325-0874",
                    ContatoViaWhatsApp = true,
                    Email = "joao@gmail.com",
                    Mensagem = "Tenho interesse em comprar este imóvel.",
                    Arquivado = false
                });
            });
        }
    }
}