﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace ImoveisNow.Models.Repository.Seed
{
    public static class BuscasSeed
    {
        public static void PopularBuscas(this ModelBuilder modelBuilder)
        {
            var data = DateTime.Now;

            modelBuilder.Entity<Buscas>(m => { m.HasData(new { Id = 1, Parametro = "Cohab", DataEHora = data }); });
            modelBuilder.Entity<Buscas>(m => { m.HasData(new { Id = 2, Parametro = "Castanheira", DataEHora = data }); });
            modelBuilder.Entity<Buscas>(m => { m.HasData(new { Id = 3, Parametro = "Porto Velho", DataEHora = data }); });
        }
    }
}