﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace ImoveisNow.Models.Repository.Seed
{
    public static class ImovelSeed
    {
        public static void PopularImoveis(this ModelBuilder modelBuilder)
        {
            var anuncioImovel1 = new Guid("37f0b25d-a6b3-4337-bf8b-8a6a289c3ebc");
            var anuncioImovel2 = new Guid("987e10f8-5042-4b54-aca6-f7bfb01e5301");
            var anuncioImovel3 = new Guid("796e2890-519a-40e4-b8d5-631e67cbc37a");
            var anuncioImovel4 = new Guid("7dac6fff-eeff-47e9-92df-a8341f0c44fc");
            var anuncioImovel5 = new Guid("3a6b2dca-a817-4fc5-be1d-37af2d62ea6f");
            var anuncioImovel6 = new Guid("5015e685-eda4-4591-b63b-44764c8c41c1");
            var tipoDeOfertaVenda = 1;
            var tipoDeOfertaAluguel = 2;
            var tipoDeImovelCasa = 1;
            var tipoDeImovelApartamento = 2;
            var tipoDeImovelTerreno = 11;

            modelBuilder.Entity<Imovel>(m =>
            {
                m.HasData(new
                {
                    Id = anuncioImovel1,
                    TipoDeOfertaId = tipoDeOfertaVenda,
                    TipoDeImovelId = tipoDeImovelCasa,
                    AreaUtil = new decimal(96.00),
                    Condominio = true,
                    Escriturado = true,
                    Financiavel = true,
                    AreaMurada = true,
                    AcessoAsfaltado = true,
                    Piscina = false,
                    Churrasqueira = true
                });
            });

            modelBuilder.Entity<Imovel>(m =>
            {
                m.HasData(new
                {
                    Id = anuncioImovel2,
                    TipoDeOfertaId = tipoDeOfertaVenda,
                    TipoDeImovelId = tipoDeImovelCasa,
                    AreaUtil = new decimal(148.00),
                    Condominio = false,
                    Escriturado = true,
                    Financiavel = true,
                    AreaMurada = true,
                    AcessoAsfaltado = true,
                    Piscina = true,
                    Churrasqueira = true
                });
            });

            modelBuilder.Entity<Imovel>(m =>
            {
                m.HasData(new
                {
                    Id = anuncioImovel3,
                    TipoDeOfertaId = tipoDeOfertaVenda,
                    TipoDeImovelId = tipoDeImovelCasa,
                    AreaUtil = new decimal(76.00),
                    Condominio = false,
                    Escriturado = true,
                    Financiavel = true,
                    AreaMurada = true,
                    AcessoAsfaltado = true,
                    Piscina = false,
                    Churrasqueira = false
                });
            });

            modelBuilder.Entity<Imovel>(m =>
            {
                m.HasData(new
                {
                    Id = anuncioImovel4,
                    TipoDeOfertaId = tipoDeOfertaVenda,
                    TipoDeImovelId = tipoDeImovelTerreno,
                    AreaUtil = new decimal(1000.00),
                    Condominio = false,
                    Escriturado = true,
                    Financiavel = true,
                    AreaMurada = true,
                    AcessoAsfaltado = false,
                    Piscina = false,
                    Churrasqueira = false
                });
            });

            modelBuilder.Entity<Imovel>(m =>
            {
                m.HasData(new
                {
                    Id = anuncioImovel5,
                    TipoDeOfertaId = tipoDeOfertaVenda,
                    TipoDeImovelId = tipoDeImovelTerreno,
                    AreaUtil = new decimal(500.00),
                    Condominio = false,
                    Escriturado = true,
                    Financiavel = true,
                    AreaMurada = true,
                    AcessoAsfaltado = true,
                    Piscina = false,
                    Churrasqueira = false
                });
            });

            modelBuilder.Entity<Imovel>(m =>
            {
                m.HasData(new
                {
                    Id = anuncioImovel6,
                    TipoDeOfertaId = tipoDeOfertaAluguel,
                    TipoDeImovelId = tipoDeImovelApartamento,
                    AreaUtil = new decimal(58.00),
                    Condominio = false,
                    Escriturado = true,
                    Financiavel = true,
                    AreaMurada = true,
                    AcessoAsfaltado = true,
                    Piscina = false,
                    Churrasqueira = false
                });
            });
        }
    }
}