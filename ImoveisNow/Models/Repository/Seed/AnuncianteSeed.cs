﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace ImoveisNow.Models.Repository.Seed
{
    public static class AnuncianteSeed
    {
        public static void PopularAnunciantes(this ModelBuilder modelBuilder)
        {
            Guid adminId = new Guid("a18be9c0-aa65-4af8-bd17-00bd9344e575");
            Guid proprietarioId = new Guid("84ad2c82-23f4-4b52-b379-ebd4e07bc7ed");
            Guid imobiliariaId = new Guid("472f72a7-2e9f-41d1-b756-a662ac2a1d98");
            Guid corretorId = new Guid("874a2abd-4d56-4af5-b361-c2b24234d519");

            var tipoDeAnuncianteProprietario = "Proprietário";
            var tipoDeAnuncianteImobiliaria = "Imobiliária";
            var tipoDeAnuncianteCorretor = "Corretor";
            var data = DateTime.Now.Date;

            modelBuilder.Entity<Anunciante>(m => { m.HasData(new {
                Id = adminId,
                Nome = "Admin",
                Email = "admin@admin",
                Telefone = "(69) 91234-5678",
                CidadeId = 1,
                PlanoAtivoId = 1,
                DataDeCadastro = data,
                UltimoAcesso = data,
                TipoDeAnunciante = tipoDeAnuncianteProprietario,
                InformacoesCertificadas = true,
                Ativo = true});
            });

            modelBuilder.Entity<Anunciante>(m =>
            {
                m.HasData(new
                {
                    Id = proprietarioId,
                    Nome = "Proprietário",
                    Email = "proprietario@proprietario",
                    Telefone = "(69) 91234-5678",
                    CidadeId = 1,
                    PlanoAtivoId = 2,
                    DataDeCadastro = data,
                    UltimoAcesso = data,
                    TipoDeAnunciante = tipoDeAnuncianteProprietario,
                    InformacoesCertificadas = false,
                    Ativo = true
                });
            });

            modelBuilder.Entity<Anunciante>(m =>
            {
                m.HasData(new
                {
                    Id = imobiliariaId,
                    Nome = "Imobiliária",
                    Email = "imobiliaria@imobiliaria",
                    Telefone = "(69) 91234-5678",
                    CidadeId = 1,
                    PlanoAtivoId = 1,
                    DataDeCadastro = data,
                    UltimoAcesso = data,
                    TipoDeAnunciante = tipoDeAnuncianteImobiliaria,
                    InformacoesCertificadas = true,
                    Ativo = true
                });
            });

            modelBuilder.Entity<Anunciante>(m =>
            {
                m.HasData(new
                {
                    Id = corretorId,
                    Nome = "Corretor",
                    Email = "corretor@corretor",
                    Telefone = "(69) 91234-5678",
                    CidadeId = 1,
                    PlanoAtivoId = 1,
                    DataDeCadastro = data,
                    UltimoAcesso = data,
                    TipoDeAnunciante = tipoDeAnuncianteCorretor,
                    InformacoesCertificadas = true,
                    Ativo = true
                });
            });
        }
    }
}