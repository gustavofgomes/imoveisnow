﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace ImoveisNow.Models.Repository.Seed
{
    public static class TipoDeOfertaSeed
    {
        public static void PopularTipoDeOfertas(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TipoDeOferta>(m => { m.HasData(new { Id = 1, Nome = "Venda", Descricao = "à Venda" }); });
            modelBuilder.Entity<TipoDeOferta>(m => { m.HasData(new { Id = 2, Nome = "Aluguel", Descricao = "para Alugar" }); });
            modelBuilder.Entity<TipoDeOferta>(m => { m.HasData(new { Id = 3, Nome = "Agio", Descricao = "vendo agio" }); });
        }
    }
}