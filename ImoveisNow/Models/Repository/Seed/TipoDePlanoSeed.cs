﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace ImoveisNow.Models.Repository.Seed
{
    public static class TipoDePlanoSeed
    {
        public static void PopularTipoDePlanos(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TipoDePlano>(m => { m.HasData(new { Id = 1, Nome = "Gratuito", DiasDeDuracao = 365, LimiteDeAnuncios = 3, LimiteDeDestaques = 0});});
            modelBuilder.Entity<TipoDePlano>(m => { m.HasData(new { Id = 2, Nome = "Premium1", DiasDeDuracao = 30, LimiteDeAnuncios = 8, LimiteDeDestaques = 2});});
            modelBuilder.Entity<TipoDePlano>(m => { m.HasData(new { Id = 3, Nome = "Premium2", DiasDeDuracao = 30, LimiteDeAnuncios = 20, LimiteDeDestaques = 4});});
            modelBuilder.Entity<TipoDePlano>(m => { m.HasData(new { Id = 4, Nome = "Premium3", DiasDeDuracao = 30, LimiteDeAnuncios = 50, LimiteDeDestaques = 8});});
        }
    }
}