﻿using ImoveisNow.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace ImoveisNow.Models.Repository.Seed
{
    public static class DetalhesDoImovelSeed
    {
        public static void PopularDetalhesDosImoveis(this ModelBuilder modelBuilder)
        {
            var anuncioImovel1 = new Guid("37f0b25d-a6b3-4337-bf8b-8a6a289c3ebc");
            var anuncioImovel2 = new Guid("987e10f8-5042-4b54-aca6-f7bfb01e5301");
            var anuncioImovel3 = new Guid("796e2890-519a-40e4-b8d5-631e67cbc37a");
            var anuncioImovel6 = new Guid("5015e685-eda4-4591-b63b-44764c8c41c1");

            modelBuilder.Entity<DetalhesDoImovel>(m =>
            {
                m.HasData(new
                {
                    Id = anuncioImovel1,
                    NumeroDeQuartos = 2,
                    NumeroDeBanheiros = 1,
                    NumeroDeSuites = 0,
                    VagasDeGaragem = 1,
                    GaragemCoberta = false,
                    Mobiliado = false,
                    Quintal = false,
                    Jardim = false
                });
            });

            modelBuilder.Entity<DetalhesDoImovel>(m =>
            {
                m.HasData(new
                {
                    Id = anuncioImovel2,
                    NumeroDeQuartos = 3,
                    NumeroDeBanheiros = 2,
                    NumeroDeSuites = 1,
                    VagasDeGaragem = 1,
                    GaragemCoberta = false,
                    Mobiliado = false,
                    Quintal = false,
                    Jardim = false
                });
            });

            modelBuilder.Entity<DetalhesDoImovel>(m =>
            {
                m.HasData(new
                {
                    Id = anuncioImovel3,
                    NumeroDeQuartos = 2,
                    NumeroDeBanheiros = 1,
                    NumeroDeSuites = 0,
                    VagasDeGaragem = 2,
                    GaragemCoberta = false,
                    Mobiliado = false,
                    Quintal = false,
                    Jardim = false
                });
            });

            modelBuilder.Entity<DetalhesDoImovel>(m =>
            {
                m.HasData(new
                {
                    Id = anuncioImovel6,
                    NumeroDeQuartos = 1,
                    NumeroDeBanheiros = 1,
                    NumeroDeSuites = 0,
                    VagasDeGaragem = 2,
                    GaragemCoberta = true,
                    Mobiliado = true,
                    Quintal = false,
                    Jardim = false
                });
            });
        }
    }
}