﻿using Microsoft.AspNetCore.Mvc;

namespace ImoveisNow.Controllers
{
    [Route("error")]
    public class ErrorController : Controller
    {
        [Route("404")]
        public IActionResult PageNotFound()
        {
            return View();
        }
        [Route("500")]
        public IActionResult InternalServerError()
        {
            return View();
        }
    }
}