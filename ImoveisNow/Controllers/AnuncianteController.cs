﻿using ImoveisNow.Models.Repository.Interface;
using ImoveisNow.Models.ViewModels.Anunciante;
using ImoveisNow.Models.ViewModels.Autenticacao;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ImoveisNow.Controllers
{
    [AllowAnonymous]
    public class AnuncianteController : Controller
    {
        private readonly IAllRepository allRepository;
        private readonly IToastNotification toastNotification;

        public AnuncianteController(IAllRepository allRepository, IToastNotification toastNotification)
        {
            this.allRepository = allRepository;
            this.toastNotification = toastNotification;
        }

        public IActionResult CadastroProprietario()
        {
            var cidades = allRepository.CidadeRepository.ObterCidades(true);
            var cadastroProprietarioViewModel = AnuncianteFactory.CriarCadastroAnuncianteViewModel(cidades);
            return View(cadastroProprietarioViewModel);
        }
        public IActionResult CadastroCorretor()
        {
            var cidades = allRepository.CidadeRepository.ObterCidades(true);
            var cadastroProprietarioViewModel = AnuncianteFactory.CriarCadastroAnuncianteViewModel(cidades);
            return View(cadastroProprietarioViewModel);
        }

        public IActionResult CadastroImobiliaria()
        {
            var cidades = allRepository.CidadeRepository.ObterCidades(true);
            var cadastroProprietarioViewModel = AnuncianteFactory.CriarCadastroAnuncianteViewModel(cidades);
            return View(cadastroProprietarioViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> CadastroProprietario(CadastroAnuncianteViewModel cadastroAnuncianteViewModel)
        {
            var checarEmail = await allRepository.UsuarioIdentityRepository.BuscarPorEmail(cadastroAnuncianteViewModel.Email);
            if (checarEmail != null)
            {
                toastNotification.AddErrorToastMessage("Falha ao realizar cadastro.");
                return View(cadastroAnuncianteViewModel);
            }

            var usuarioIdentity = UsuarioIdentityFactory.CriarUsuarioIdentityProprietario(cadastroAnuncianteViewModel);
            var resultado = await allRepository.UsuarioIdentityRepository.CriarUsuarioIdentity(usuarioIdentity, cadastroAnuncianteViewModel.Senha);
            if (resultado.Succeeded)
            {
                var usuarioId = await allRepository.UsuarioIdentityRepository.BuscarPorEmail(cadastroAnuncianteViewModel.Email);
                var anunciante = AnuncianteFactory.CriarAnunciante(cadastroAnuncianteViewModel, new Guid(usuarioId.Id));
                await allRepository.AnuncianteRepository.CriarAnunciante(anunciante);
                await allRepository.UsuarioIdentityRepository.CriarClaimsDoUsuario(usuarioIdentity, new Claim("Anunciante", "True"));
                await allRepository.UsuarioIdentityRepository.CriarClaimsDoUsuario(usuarioIdentity, new Claim("Proprietario", "True"));
                toastNotification.AddInfoToastMessage("Cadastro realizado com sucesso!");

                return RedirectToAction("Entrar", "Autenticacao");
            }
            toastNotification.AddErrorToastMessage("Falha ao realizar cadastro.");
            return View(cadastroAnuncianteViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> CadastroCorretor(CadastroAnuncianteViewModel cadastroAnuncianteViewModel)
        {
            var checarEmail = await allRepository.UsuarioIdentityRepository.BuscarPorEmail(cadastroAnuncianteViewModel.Email);
            if (checarEmail != null)
            {
                toastNotification.AddErrorToastMessage("Falha ao realizar cadastro.");
                return View(cadastroAnuncianteViewModel);
            }

            var usuarioIdentity = UsuarioIdentityFactory.CriarUsuarioIdentityProprietario(cadastroAnuncianteViewModel);
            var resultado = await allRepository.UsuarioIdentityRepository.CriarUsuarioIdentity(usuarioIdentity, cadastroAnuncianteViewModel.Senha);
            if (resultado.Succeeded)
            {
                var usuarioId = await allRepository.UsuarioIdentityRepository.BuscarPorEmail(cadastroAnuncianteViewModel.Email);
                var anunciante = AnuncianteFactory.CriarAnunciante(cadastroAnuncianteViewModel, new Guid(usuarioId.Id));
                await allRepository.AnuncianteRepository.CriarAnunciante(anunciante);
                await allRepository.UsuarioIdentityRepository.CriarClaimsDoUsuario(usuarioIdentity, new Claim("Anunciante", "True"));
                await allRepository.UsuarioIdentityRepository.CriarClaimsDoUsuario(usuarioIdentity, new Claim("Corretor", "True"));
                toastNotification.AddInfoToastMessage("Cadastro realizado com sucesso!");

                return RedirectToAction("Entrar", "Autenticacao");
            }
            toastNotification.AddErrorToastMessage("Falha ao realizar cadastro.");
            return View(cadastroAnuncianteViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> CadastroImobiliaria(CadastroAnuncianteViewModel cadastroAnuncianteViewModel)
        {
            var checarEmail = await allRepository.UsuarioIdentityRepository.BuscarPorEmail(cadastroAnuncianteViewModel.Email);
            if (checarEmail != null)
            {
                toastNotification.AddErrorToastMessage("Falha ao realizar cadastro.");
                return View(cadastroAnuncianteViewModel);
            }

            var usuarioIdentity = UsuarioIdentityFactory.CriarUsuarioIdentityProprietario(cadastroAnuncianteViewModel);
            var resultado = await allRepository.UsuarioIdentityRepository.CriarUsuarioIdentity(usuarioIdentity, cadastroAnuncianteViewModel.Senha);
            if (resultado.Succeeded)
            {
                var usuarioId = await allRepository.UsuarioIdentityRepository.BuscarPorEmail(cadastroAnuncianteViewModel.Email);
                var anunciante = AnuncianteFactory.CriarAnunciante(cadastroAnuncianteViewModel, new Guid(usuarioId.Id));
                await allRepository.AnuncianteRepository.CriarAnunciante(anunciante);
                await allRepository.UsuarioIdentityRepository.CriarClaimsDoUsuario(usuarioIdentity, new Claim("Anunciante", "True"));
                await allRepository.UsuarioIdentityRepository.CriarClaimsDoUsuario(usuarioIdentity, new Claim("Imobiliaria", "True"));
                toastNotification.AddInfoToastMessage("Cadastro realizado com sucesso!");

                return RedirectToAction("Entrar", "Autenticacao");
            }
            toastNotification.AddErrorToastMessage("Falha ao realizar cadastro.");
            return View(cadastroAnuncianteViewModel);
        }
    }
}