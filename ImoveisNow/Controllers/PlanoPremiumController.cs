﻿using ImoveisNow.Models.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ImoveisNow.Controllers
{
    [Authorize(Policy = "Anunciante")]
    public class PlanoPremiumController : Controller
    {
        private readonly IAllRepository allRepository;

        public PlanoPremiumController(IAllRepository allRepository)
        {
            this.allRepository = allRepository;
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult FormaDePagamento(int id)
        {
            var usuario = allRepository.UsuarioIdentityRepository.UsuarioLogadoId();
            return View();
        }
    }
}