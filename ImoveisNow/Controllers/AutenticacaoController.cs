﻿using ImoveisNow.Models.Helpers;
using ImoveisNow.Models.Repository.Interface;
using ImoveisNow.Models.ViewModels.Autenticacao;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using System;
using System.Threading.Tasks;

namespace ImoveisNow.Controllers
{
    [AllowAnonymous]
    public class AutenticacaoController : Controller
    {
        private readonly IAllRepository allRepository;
        private readonly IToastNotification toastNotification;

        public AutenticacaoController(IAllRepository allRepository, IToastNotification toastNotification)
        {
            this.allRepository = allRepository;
            this.toastNotification = toastNotification;
        }

        public IActionResult Entrar()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Entrar(LoginViewModel loginViewModel)
        {
            var usuarioIdentity = await allRepository.UsuarioIdentityRepository.BuscarPorEmail(loginViewModel.Email);
            var usuarioIdentityResult = await allRepository.UsuarioIdentityRepository.RealizarLogin(usuarioIdentity, loginViewModel.Senha);
            var anunciante = allRepository.AnuncianteRepository.BuscarAnunciantePorId(new Guid(usuarioIdentity.Id), true);
            if (usuarioIdentityResult.Succeeded)
            {
                Response.Cookies.Append("EmailUsuarioIdentity", usuarioIdentity.Email, new CookieOptions() { IsEssential = true });
                Response.Cookies.Append("NomeUsuarioIdentity", StringHelper.GetFirstName(anunciante.Nome), new CookieOptions() { IsEssential = true });
                Response.Cookies.Append("IdUsuarioIdentity", usuarioIdentity.Id, new CookieOptions() { IsEssential = true });
                toastNotification.AddInfoToastMessage("Login Realizado com Sucesso!");
                return RedirectToAction("Index", "Home");
            }
            else
            {
                toastNotification.AddErrorToastMessage("Login ou Senha Incorreto!");
                return View(loginViewModel);
            }
        }

        public IActionResult Cadastro()
        {
            var usuario = allRepository.UsuarioIdentityRepository.UsuarioLogadoId();

            if (usuario != null)
            {
                toastNotification.AddInfoToastMessage("Ops!");
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View();
            }
        }

        public async Task<IActionResult> Sair()
        {
            await allRepository.UsuarioIdentityRepository.Sair();
            toastNotification.AddSuccessToastMessage("LogOff Realizado!");
            return RedirectToAction("Entrar");
        }

        public IActionResult AcessoNegado()
        {
            return View();
        }
    }
}