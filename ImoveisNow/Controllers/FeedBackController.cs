﻿using ImoveisNow.Models.Repository.Interface;
using ImoveisNow.Models.ViewModels.Feedback;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;

namespace ImoveisNow.Controllers
{
    [Authorize(Policy = "Administrador")]
    public class FeedBackController : Controller
    {
        private readonly IAllRepository allRepository;
        private readonly IToastNotification toastNotification;

        public FeedBackController(IAllRepository allRepository, IToastNotification toastNotification)
        {
            this.allRepository = allRepository;
            this.toastNotification = toastNotification;
        }

        public IActionResult LerFeedBack(int id)
        {
            var feedback = allRepository.FeedbackRepository.BuscarPorId(id, true);
            var lerFeedbackViewModel = FeedBackFactory.CriarLerFeedBackViewModel(feedback);
            return View(lerFeedbackViewModel);
        }

        [AllowAnonymous]
        public IActionResult Criar(CriarFeedBackViewModel criarFeedBackViewModel)
        {
            var feedback = FeedBackFactory.CriarFeedBack(criarFeedBackViewModel);
            allRepository.FeedbackRepository.Criar(feedback);
            toastNotification.AddInfoToastMessage("Reporte de problemas enviado!");
            return RedirectToAction("Anunciante", "MinhaConta");
        }

        public IActionResult Arquivar(int id)
        {
            var feedback = allRepository.FeedbackRepository.BuscarPorId(id, true);
            feedback.Arquivar();
            allRepository.FeedbackRepository.Atualizar(feedback);

            return RedirectToAction("Admin", "MinhaConta");
        }
    }
}