﻿using ImoveisNow.Models.Repository.Interface;
using ImoveisNow.Models.ViewModels.MinhaConta;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace ImoveisNow.Controllers
{
    [Authorize]
    public class MinhaContaController : Controller
    {
        private readonly IAllRepository allRepository;

        public MinhaContaController(IAllRepository allRepository)
        {
            this.allRepository = allRepository;
        }

        public async Task<IActionResult> Index()
        {
            var usuarioLogadoId = allRepository.UsuarioIdentityRepository.UsuarioLogadoId();
            var claims = await allRepository.UsuarioIdentityRepository.BuscarClaims(usuarioLogadoId);
            //ToDo: Melhorar o Redirecionamento
            if (claims != null)
            {
                foreach (var claim in claims)
                {
                    if (claim.Type == "Administrador")
                    {
                        return RedirectToAction("Admin");
                    }
                    if (claim.Type == "Anunciante")
                    {
                        return RedirectToAction("Anunciante");
                    }
                }
            }
            return NotFound();
        }

        [Authorize(Policy = "Anunciante")]
        public IActionResult Anunciante()
        {
            var usuarioId = allRepository.UsuarioIdentityRepository.UsuarioLogadoId();
            //var anunciante = allRepository.AnuncianteRepository.BuscarAnunciantePorId(new Guid(usuarioId), true);
            var favoritos = allRepository.AnunciosFavoritosRepository.BuscarTodosDoUsuario(new Guid(usuarioId), true);
            var contato = allRepository.ContatosRecebidosRepository.BuscarTodosDoUsuario(new Guid(usuarioId), true);
            //var planoAtivo = allRepository.AnuncianteRepository.BuscarPlanoAtivoPorAnuncianteId(new Guid(usuarioId, true);
            var minhaContaAnuncianteViewModel = MinhaContaFactory.CriarMinhaContaAnuncianteViewModel(favoritos, contato);
            return View(minhaContaAnuncianteViewModel);
        }

        [Authorize(Policy = "Administrador")]
        public IActionResult Admin()
        {
            var numeroDeVisualizacoes = allRepository.VisualizacoesRepository.ObterNumeroDeVisualizacoes(true);
            var numeroDeAnuncios = allRepository.AnuncioRepository.ObterNumeroDeAnunciosAtivo(true);
            var numeroDeAnunciantes = allRepository.AnuncianteRepository.ObterNumeroDeAnunciantesAtivo(true);
            var numeroDePlanosPremiums = allRepository.PlanoPremiumRepository.ObterNumeroDePlanosPremiumsAtivo(true);
            var denuncias = allRepository.DenunciaRepository.ObterCincoDenunciasNaoAvaliadas(true);
            var feedback = allRepository.FeedbackRepository.ObterCincoFeedBacksNaoArquivados(true);
            var minhaContaAdminViewModel = MinhaContaFactory.CriarMinhaContaAdminViewModel(numeroDeVisualizacoes, numeroDeAnuncios, numeroDeAnunciantes, numeroDePlanosPremiums, denuncias, feedback);
            return View(minhaContaAdminViewModel);
        }

        public IActionResult VisualizarContato(int id)
        {
            var contato = allRepository.ContatosRecebidosRepository.BuscasPorId(id, true);

            var xpto = MinhaContaFactory.CriarContato(contato);

            return View(xpto);
        }

        public IActionResult Arquivar(int id)
        {
            var contato = allRepository.ContatosRecebidosRepository.BuscasPorId(id, true);
            contato.Arquivar();
            allRepository.ContatosRecebidosRepository.Arquivar(contato);

            return RedirectToAction("Anunciante");
        }
    }
}