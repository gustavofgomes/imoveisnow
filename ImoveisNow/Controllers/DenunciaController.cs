﻿using ImoveisNow.Models.Repository.Interface;
using ImoveisNow.Models.ViewModels.Denuncia;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;

namespace ImoveisNow.Controllers
{
    [Authorize(Policy = "Administrador")]
    public class DenunciaController : Controller
    {
        private readonly IAllRepository allRepository;
        private readonly IToastNotification toastNotification;

        public DenunciaController(IAllRepository allRepository, IToastNotification toastNotification)
        {
            this.allRepository = allRepository;
            this.toastNotification = toastNotification;
        }

        public IActionResult AvaliarDenuncia(int id)
        {
            var denuncia = allRepository.DenunciaRepository.BuscarPorId(id, true);
            var avaliarDenunciaViewModel = DenunciaFactory.CriarAvaliarDenunciaViewModel(denuncia);
            return View(avaliarDenunciaViewModel);
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult NovaDenuncia(DenunciarAnuncioViewModel denunciarAnuncioViewModel)
        {
            var denuncia = DenunciaFactory.CriarDenuncia(denunciarAnuncioViewModel);
            allRepository.DenunciaRepository.CriarDenuncia(denuncia);
            toastNotification.AddInfoToastMessage("Reporte de problemas enviado!");
            return RedirectToAction("Detalhes", "Anuncio", new { Id = denunciarAnuncioViewModel.AnuncioId });
        }

        public IActionResult VereditoAcatar(int id)
        {
            var denuncia = allRepository.DenunciaRepository.BuscarPorId(id, true);
            denuncia.DenunciaAprovada();
            allRepository.DenunciaRepository.Atualizar(denuncia);
            toastNotification.AddInfoToastMessage("Veredito gravado!");
            return RedirectToAction("Admin", "MinhaConta");
        }

        public IActionResult VereditoIgnorar(int id)
        {
            var denuncia = allRepository.DenunciaRepository.BuscarPorId(id, true);
            denuncia.DenunciaNegada();
            allRepository.DenunciaRepository.Atualizar(denuncia);
            toastNotification.AddInfoToastMessage("Veredito gravado!");
            return RedirectToAction("Admin", "MinhaConta");
        }
    }
}