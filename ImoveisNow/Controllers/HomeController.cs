﻿using ImoveisNow.Models.Repository.Interface;
using ImoveisNow.Models.ViewModels.Home;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ImoveisNow.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        private readonly IAllRepository allRepository;

        public HomeController(IAllRepository allRepository)
        {
            this.allRepository = allRepository;
        }

        public IActionResult Index()
        {
            var tresAnunciosEmDestaque = allRepository.AnuncioRepository.ObterTresUltimosAnunciosEmDestaque(true);
            var tresUltimosAnuncios = allRepository.AnuncioRepository.ObterTresUltimosAnunciosAnuncios(true);
            var inicioViewModel = HomeFactory.CriarInicioViewModel(tresAnunciosEmDestaque, tresUltimosAnuncios);
            return View(inicioViewModel);
        }

        public IActionResult Sobre()
        {
            return View();
        }

        public IActionResult Contato()
        {
            return View();
        }

        public IActionResult DuvidasFrequentes()
        {
            return View();
        }

        public IActionResult Legal()
        {
            return View();
        }
    }
}