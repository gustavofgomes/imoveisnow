﻿using ImoveisNow.Models.DTO;
using ImoveisNow.Models.Repository.Interface;
using ImoveisNow.Models.ViewModels.Anuncio;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using System;
using System.Collections.Generic;
using System.IO;

namespace ImoveisNow.Controllers
{
    [Authorize(Policy = "Anunciante")]
    public class AnuncioController : Controller
    {
        private readonly IAllRepository allRepository;
        private readonly IHostingEnvironment hostingEnvironment;
        private readonly IToastNotification toastNotification;

        public AnuncioController(IAllRepository allRepository, IToastNotification toastNotification, IHostingEnvironment hostingEnvironment)
        {
            this.allRepository = allRepository;
            this.hostingEnvironment = hostingEnvironment;
            this.toastNotification = toastNotification;
        }

        #region BuscasEListagem

        [AllowAnonymous]
        public IActionResult Index()
        {
            var anuncios = allRepository.AnuncioRepository.ObterTodosOsAnunciosAgioEVendaAtivos(true);
            var categorias = allRepository.CategoriaRepository.ObterTodasCategorias(true);
            var tipoDoImovel = allRepository.TipoDoImovelRepository.ObterTodosOsTiposDeImoveis(true);
            var listaDeAnunciosViewModel = AnuncioFactory.CriarListaDeAnunciosViewModel(anuncios, categorias, tipoDoImovel);
            return View(listaDeAnunciosViewModel);
        }

        [AllowAnonymous]
        public IActionResult Alugar()
        {
            var anuncios = allRepository.AnuncioRepository.ObterTodosOsAnunciosAlugarAtivos(true);
            var categorias = allRepository.CategoriaRepository.ObterTodasCategorias(true);
            var tipoDoImovel = allRepository.TipoDoImovelRepository.ObterTodosOsTiposDeImoveis(true);
            var listaDeAnunciosViewModel = AnuncioFactory.CriarListaDeAnunciosViewModel(anuncios, categorias, tipoDoImovel);
            return View("Index", listaDeAnunciosViewModel);
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult BuscaGeral(string parametro)
        {
            //ToDo: Exibir o Parametro De Busca e Melhorar a URL
            //ToDo: Gravar Id do Usuário se logado para pegar a ultima consulta dele é buscar denovo caso feche a tela.
            var busca = AnuncioFactory.CriarBusca(parametro);
            allRepository.BuscasRepository.CriarBusca(busca);

            var anuncios = allRepository.AnuncioRepository.BuscarPorRuaBairroCidade(parametro, true);
            var categorias = allRepository.CategoriaRepository.ObterTodasCategorias(true);
            var tipoDoImovel = allRepository.TipoDoImovelRepository.ObterTodosOsTiposDeImoveis(true);
            var listaDeAnunciosViewModel = AnuncioFactory.CriarListaDeAnunciosViewModel(anuncios, categorias, tipoDoImovel);
            return View("Index", listaDeAnunciosViewModel);
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult FiltroListagem(FiltroListagemAnuncioViewModel filtro)
        {
            return View("Index");
        }

        #endregion

        [AllowAnonymous]
        [Route("Anuncio/{id}", Name = "Anuncio")]
        public IActionResult Detalhes(Guid Id)
        {
            var favoritado = false;
            var anuncio = allRepository.AnuncioRepository.BuscarAnuncioPorId(Id, true);
            if (anuncio == null)
            {
                return NotFound();
            }
            var visualizacao = AnuncioFactory.CriarVisualizacao(Id);
            allRepository.VisualizacoesRepository.CriarVisualizacao(visualizacao);
            var usuario = allRepository.UsuarioIdentityRepository.UsuarioLogadoId();
            if (usuario != null)
            {
                var favorito = allRepository.AnunciosFavoritosRepository.BuscarPorId(Id, new Guid(usuario), true);
                if (favorito != null)
                {
                    favoritado = true;
                }
            }
            var detalhesAnuncioViewModel = AnuncioFactory.CriarDetalhesAnuncioViewModel(anuncio, favoritado);
            return View(detalhesAnuncioViewModel);
        }

        [HttpPost]
        public IActionResult Favorito(Guid Id)
        {
            var usuario = allRepository.UsuarioIdentityRepository.UsuarioLogadoId();
            var favorito = AnuncioFactory.CriarFavorito(Id, usuario);
            allRepository.AnunciosFavoritosRepository.CriarFavorito(favorito);
            toastNotification.AddInfoToastMessage("Anuncio adicionado aos seus favoritos!");
            return RedirectToAction("Detalhes", new { Id });
        }

        [HttpPost]
        public IActionResult RemoverFavorito(Guid Id)
        {
            var usuario = allRepository.UsuarioIdentityRepository.UsuarioLogadoId();
            var favorito = allRepository.AnunciosFavoritosRepository.BuscarPorId(Id, new Guid(usuario), true);
            if (favorito != null)
            {
                allRepository.AnunciosFavoritosRepository.RemoverFavorito(favorito);
            }
            toastNotification.AddInfoToastMessage("Anuncio removido dos seus favoritos!");
            return RedirectToAction("Detalhes", new { Id });
        }

        [AllowAnonymous]
        public IActionResult CategoriaDoNovoAnuncio()
        {
            return View();
        }

        #region NovoAnuncioResidencialComercial

        public IActionResult NovoResidencialComercial()
        {
            //ToDo: Checar se o Anunciante Pode Criar Anúncio
            var usuarioId = allRepository.UsuarioIdentityRepository.UsuarioLogadoId();
            var anunciante = allRepository.AnuncianteRepository.BuscarAnunciantePorId(new Guid(usuarioId), true);
            var cidades = allRepository.CidadeRepository.ObterCidades(true);
            var novoAnuncioViewModel = AnuncioFactory.CriarNovoAnuncioResidencialComercialViewModel(cidades, anunciante);
            return View(novoAnuncioViewModel);
        }

        [HttpPost]
        public IActionResult NovoResidencialComercial(NovoAnuncioResidencialComercialViewModel novoAnuncioResidencialComercialViewModel)
        {
            var listaDeFotos = TransferirFotoParaArquivoEhRealizarConversao(novoAnuncioResidencialComercialViewModel.Imagens);
            var usuarioId = allRepository.UsuarioIdentityRepository.UsuarioLogadoId();
            var tipoDeOferta = allRepository.CategoriaRepository.BuscarCategoriaPorId(novoAnuncioResidencialComercialViewModel.TipoDeOfertaId, true);
            var tipoDoImovel = allRepository.TipoDoImovelRepository.BuscarTipoDoImovelPorId(novoAnuncioResidencialComercialViewModel.TipoDeImovelId, true);
            var anuncio = AnuncioFactory.CriarAnuncioResidencialComercial(novoAnuncioResidencialComercialViewModel, tipoDeOferta, tipoDoImovel, listaDeFotos, usuarioId);
            allRepository.AnuncioRepository.CriarAnuncio(anuncio);
            toastNotification.AddSuccessToastMessage("Anúncio criado!");
            return RedirectToAction("MeusAnuncios");
        }

        #endregion

        #region NovoAnuncioLoteTerreno

        public IActionResult NovoLoteTerreno()
        {
            //ToDo: Checar se o Anunciante Pode Criar Anúncio
            var usuarioId = allRepository.UsuarioIdentityRepository.UsuarioLogadoId();
            var anunciante = allRepository.AnuncianteRepository.BuscarAnunciantePorId(new Guid(usuarioId), true);
            var novoAnuncioViewModel = AnuncioFactory.CriarNovoLoteTerrenoAnuncioViewModel(anunciante);
            return View(novoAnuncioViewModel);
        }

        [HttpPost]
        public IActionResult NovoLoteTerreno(NovoAnuncioLoteTerrenoViewModel novoAnuncioTerrenoLoteViewModel)
        {
            var listaDeFotos = TransferirFotoParaArquivoEhRealizarConversao(novoAnuncioTerrenoLoteViewModel.Imagens);
            var usuarioId = allRepository.UsuarioIdentityRepository.UsuarioLogadoId();
            var tipoDeOferta = allRepository.CategoriaRepository.BuscarCategoriaPorId(novoAnuncioTerrenoLoteViewModel.TipoDeOfertaId, true);
            var tipoDoImovel = allRepository.TipoDoImovelRepository.BuscarTipoDoImovelPorId(novoAnuncioTerrenoLoteViewModel.TipoDeImovelId, true);
            var anuncio = AnuncioFactory.CriarAnuncioLoteTerreno(novoAnuncioTerrenoLoteViewModel, tipoDeOferta, tipoDoImovel, listaDeFotos, usuarioId);
            allRepository.AnuncioRepository.CriarAnuncio(anuncio);
            toastNotification.AddSuccessToastMessage("Anúncio criado!");
            return RedirectToAction("MeusAnuncios");
        }

        #endregion

        public IActionResult DetalhesDoMeuAnuncio()
        {
            return View();
        }

        public IActionResult MeusAnuncios()
        {
            var usuarioId = allRepository.UsuarioIdentityRepository.UsuarioLogadoId();
            var anuncios = allRepository.AnuncioRepository.BuscarAnunciosDoUsuario(new Guid(usuarioId), true);
            var meusAnunciosViewModel = AnuncioFactory.CriarMeusAnunciosViewModel(anuncios);
            return View(meusAnunciosViewModel);
        }

        public IActionResult EditarAnuncio()
        {
            return View();
        }

        public IActionResult EncerrarAnuncio()
        {
            return RedirectToAction("MeusAnuncios");
        }

        [HttpPost]
        public IActionResult ContatarAnunciante(ContatarAnuncianteViewModel contatarAnuncianteViewModel)
        {
            var contato = AnuncioFactory.CriarContato(contatarAnuncianteViewModel);
            allRepository.ContatosRecebidosRepository.Criar(contato);
            toastNotification.AddInfoToastMessage("Sua Mensagem foi Enviada!");
            return RedirectToAction("Detalhes", new { id = contatarAnuncianteViewModel.AnuncioId });
        }

        private ICollection<FotosDTO> TransferirFotoParaArquivoEhRealizarConversao(ICollection<IFormFile> listaDeFotos)
        {
            var listaDeFotosConvertidas = new List<FotosDTO>();
            if (listaDeFotos != null)
            {
                foreach (var foto in listaDeFotos)
                {
                    var nomeGuid = Guid.NewGuid().ToString();
                    var extensaoDoArquivo = Path.GetExtension(foto.FileName);
                    var path = Path.Combine(hostingEnvironment.ContentRootPath, "wwwroot/upload", nomeGuid + extensaoDoArquivo);
                    var tamanho = foto.Length.ToString();
                    var url = "/upload/";

                    foto.CopyTo(new FileStream(path, FileMode.Create));

                    bool fotoFoiTransferidaProServidor = new FileInfo(path).Exists;

                    if (fotoFoiTransferidaProServidor)
                    {
                        listaDeFotosConvertidas.Add(new FotosDTO { Nome = nomeGuid, URL = url, Formato = extensaoDoArquivo, Tamanho = tamanho });
                    }
                }
            }
            return listaDeFotosConvertidas;
        }
    }
}