﻿using Identity.Repository.Class;
using Identity.Repository.Interface;
using ImoveisNow.Models.Repository.Class;
using ImoveisNow.Models.Repository.Interface;
using Microsoft.Extensions.DependencyInjection;

namespace ImoveisNow
{
    public class BootStrapper
    {
        public static void RegisterServices(IServiceCollection container)
        {
            container.AddScoped<IAllRepository, AllRepository>();

            container.AddScoped<IUsuarioIdentityRepository, UsuarioIdentityRepository>();

            container.AddScoped<IAnuncianteRepository, AnuncianteRepository>();

            container.AddScoped<IAnuncioRepository, AnuncioRepository>();

            container.AddScoped<IAnunciosFavoritosRepository, AnunciosFavoritosRepository>();

            container.AddScoped<IBuscaRepository, BuscaRepository>();

            container.AddScoped<ITipoDeOfertaRepository, TipoDeOfertaRepository>();

            container.AddScoped<ICidadeRepository, CidadeRepository>();

            container.AddScoped<IContatosRecebidosRepository, ContatosRecebidosRepository>();

            container.AddScoped<IDenunciaRepository, DenunciaRepository>();

            container.AddScoped<IFeedbackRepository, FeedbackRepository>();

            container.AddScoped<IPlanoDoAnuncianteRepository, PlanoDoAnuncianteRepository>();

            container.AddScoped<ITipoDoImovelRepository, TipoDoImovelRepository>();

            container.AddScoped<IVisualizacaoRepository, VisualizacaoRepository>();
        }
    }
}